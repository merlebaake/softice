package org.ornet.softice.test;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ornet.cdm.AlertSignalState;
import org.ornet.cdm.InstanceIdentifier;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.LocationContextState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.AlertSignalPresence;
import org.ornet.softice.SoftICE;
import org.ornet.softice.consumer.FutureInvocationState;
import org.ornet.softice.consumer.SDCConsumer;
import org.ornet.softice.consumer.SDCConsumerEventHandler;
import org.ornet.softice.consumer.SDCConsumerOperationInvokedHandler;
import org.ornet.softice.consumer.SDCConsumerStateChangedHandler;
import org.ornet.softice.provider.SDCProvider;
import org.ornet.softice.consumer.SDCServiceManager;
import org.ornet.softice.provider.OperationInvocationContext;
import org.ornet.softice.test.classes.DemoProviderFactory;

/**
 *
 * @author besting
 */
public class SDCTest {
    
    static SDCProvider provider = null;
    static SDCConsumer consumer = null;
    
    public SDCTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        SoftICE.getInstance().startup();
        SoftICE.getInstance().setSchemaValidationEnabled(false);
        provider = DemoProviderFactory.getDemoProvider("UDI-1234567890");
        provider.startup();      
        consumer = SDCServiceManager.getInstance().discoverEPR("UDI-1234567890", 10000);     
    }
    
    @AfterClass
    public static void tearDownClass() {         
        if (consumer != null)
            consumer.close();
        provider.shutdown();       
        SoftICE.getInstance().shutdown();
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMDIB() {
        assertEquals(provider.isRunning(), true);
        try {
            Thread.sleep(0);
        } catch (InterruptedException ex) {
            Logger.getLogger(SDCTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertNotNull(consumer);
        if (consumer == null)
            return;       
        long startTime = System.currentTimeMillis();
        assertEquals(true, consumer.getMDIB() != null);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Time Get MDIB: " + elapsedTime);
    }
    
    @Test
    public void testGetSetMetric() {
        assertNotNull(consumer);
        if (consumer == null)
            return;         
        final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        consumer.registerEventHandler(new SDCConsumerEventHandler<NumericMetricState>("handle_metric") {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                changeEventReceived.set(true);
            }
        });
        // Get metric
        NumericMetricState nms = consumer.requestState("handle_metric", NumericMetricState.class);
        assertNotNull(nms);
        assertNotNull(nms.getMetricValue());
        assertNotNull(nms.getMetricValue().getValue());
        // Set metric
        FutureInvocationState fis = new FutureInvocationState();
        nms.getMetricValue().setValue(BigDecimal.valueOf(2));
        long startTime = System.currentTimeMillis();
        assertEquals(InvocationState.WAIT, consumer.commitState(nms, fis));
        long stopTime = System.currentTimeMillis();
        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));
        long elapsedTime = stopTime - startTime;
        System.out.println("Time Set Value: " + elapsedTime);
        // Get again and compare
        startTime = System.currentTimeMillis();
        nms = consumer.requestState("handle_metric", NumericMetricState.class);
        stopTime = System.currentTimeMillis();
        elapsedTime = stopTime - startTime;
        System.out.println("Time Get Value: " + elapsedTime);
        assertNotNull(nms);
        assertNotNull(nms.getMetricValue());
        assertNotNull(nms.getMetricValue().getValue());
        assertEquals(BigDecimal.valueOf(2), nms.getMetricValue().getValue());
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(SDCTest.class.getName()).log(Level.SEVERE, null, ex);
        }        
        assertEquals(true, changeEventReceived.get());
        consumer.unregisterEventHandler("handle_metric");
    }
    
    @Test
    public void testGetSetContext() {
        assertNotNull(consumer);
        if (consumer == null)
            return;         
        final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        final AtomicBoolean asyncFISreceived = new AtomicBoolean(false);
        consumer.registerEventHandler(new SDCConsumerEventHandler<LocationContextState>("handle_context") {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());
            }
            
            @Override
            public void onStateChanged(LocationContextState state) {
                changeEventReceived.set(true);
            }
            
        });
        // Get context
        LocationContextState lcs = consumer.requestState("handle_context", LocationContextState.class);
        assertNotNull(lcs);
        assertEquals(true, lcs.getIdentification().size() == 1);
        // Set context
        InstanceIdentifier instId = new InstanceIdentifier();
        instId.setRoot("root");
        instId.setExtension("TestSet");
        lcs.getIdentification().set(0, instId);
        // Test async FIS receiver
        consumer.commitState(lcs, new FutureInvocationState().receiveAsync((InvocationState state) -> {
            if (state == InvocationState.FIN) {
                asyncFISreceived.set(true);
            }
        }));        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SDCTest.class.getName()).log(Level.SEVERE, null, ex);
        }                
        // Get again and compare
        lcs = consumer.requestState("handle_context", LocationContextState.class);
        assertNotNull(lcs);
        assertEquals(true, lcs.getIdentification().get(0).getExtension().equals("TestSet"));
        assertEquals(true, changeEventReceived.get());
        assertEquals(true, asyncFISreceived.get());
        consumer.unregisterEventHandler("handle_context");
    }    
    
    @Test
    public void testActivate() {
        assertNotNull(consumer);
        if (consumer == null)
            return;
        final AtomicBoolean invokeEventReceived = new AtomicBoolean(false);
        consumer.registerEventHandler(new SDCConsumerOperationInvokedHandler("handle_cmd") {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name()); 
                invokeEventReceived.set(true);
            }

        });
        FutureInvocationState fis = new FutureInvocationState();
        assertEquals(InvocationState.WAIT, consumer.activate("handle_cmd", fis));
        assertEquals(true, fis.waitReceived(new InvocationState[] { InvocationState.START, InvocationState.FIN }, 2000));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SDCTest.class.getName()).log(Level.SEVERE, null, ex);
        }                
        assertEquals(true, invokeEventReceived.get());        
        consumer.unregisterEventHandler("handle_cmd");
    }
    
    @Test
    public void testAlerts() {
        // register for alarm signal events
        final AtomicBoolean onReceived = new AtomicBoolean(false);
        final AtomicBoolean latchReceived = new AtomicBoolean(false);
        consumer.registerEventHandler(new SDCConsumerStateChangedHandler<AlertSignalState>("handle_alert_signal_latching") {

            @Override
            public void onStateChanged(AlertSignalState state) {
                if (state.getPresence() == AlertSignalPresence.ON)
                    onReceived.set(true);
                if (state.getPresence() == AlertSignalPresence.LATCH)
                    latchReceived.set(true);
            }
            
        });        
        // Get metric
        NumericMetricState nms = consumer.requestState("handle_metric",NumericMetricState.class);
        assertNotNull(nms);
        assertNotNull(nms.getMetricValue());
        // Set metric to value above upper limit (0, 2), alert condition triggers alert signal ON
        FutureInvocationState fis = new FutureInvocationState();
        nms.getMetricValue().setValue(BigDecimal.valueOf(3));
        assertEquals(InvocationState.WAIT, consumer.commitState(nms, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));        
        // Set metric to value within bounds (0, 2), alert condition triggers alert signal LATCHED
        fis = new FutureInvocationState();
        nms.getMetricValue().setValue(BigDecimal.valueOf(1));
        assertEquals(InvocationState.WAIT, consumer.commitState(nms, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));
        // Check if events have been received
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SDCTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(true, onReceived.get());
        assertEquals(true, latchReceived.get());
        // Test get/set alert signal
        AlertSignalState cas = consumer.requestState("handle_alert_signal_latching", AlertSignalState.class);
        assertNotNull(cas);
        assertNotSame(cas.getPresence(), AlertSignalPresence.OFF);
        cas.setPresence(AlertSignalPresence.OFF);
        fis = new FutureInvocationState();
        assertEquals(InvocationState.WAIT, consumer.commitState(cas, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FIN, 2000));
        // Compare
        cas = consumer.requestState("handle_alert_signal_latching", AlertSignalState.class);
        assertEquals(cas.getPresence(), AlertSignalPresence.OFF);
    }
    
    
}
