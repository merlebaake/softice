package org.ornet.softice.test;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.ssl.SSLContexts;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.softice.SoftICE;
import org.ornet.softice.consumer.FutureInvocationState;
import org.ornet.softice.consumer.SDCConsumer;
import org.ornet.softice.consumer.SDCConsumerEventHandler;
import org.ornet.softice.consumer.SDCConsumerStateChangedHandler;
import org.ornet.softice.consumer.SDCServiceManager;
import org.ornet.softice.provider.OperationInvocationContext;

/**
 *
 * @author besting
 */
public class ExampleConsumer {
    
    static SDCConsumer consumer = null;
    
    public ExampleConsumer() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        SoftICE.getInstance().startup();
        SoftICE.getInstance().setSchemaValidationEnabled(true);
        SoftICE.getInstance().setPortStart(30000);
//        try {
//            SoftICE.getInstance().setClientSSLContext(SSLContexts.custom().loadTrustMaterial(new File("c:\\truststore.jks"), "5t6z7u8i".toCharArray()).build());
//            SoftICE.getInstance().setServerSSLContext(SSLContexts.custom().loadKeyMaterial(new File("c:\\keystore.jks"), "5t6z7u8i".toCharArray(), "1q2w3e4r".toCharArray()).build());            
//        } catch (Exception ex) {
//            Logger.getLogger(ExampleProvider.class.getName()).log(Level.SEVERE, null, ex);
//        }                                           
        consumer = SDCServiceManager.getInstance().discoverEPR("UDI-1234567890");
        consumer.registerEventHandler(new SDCConsumerEventHandler<NumericMetricState>("handle_metric") {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received operation invoked (handle CUR): " + is.name());
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                System.out.println("Received state changed, value (handle CUR): " + ((NumericMetricState)state).getMetricValue().getValue().doubleValue());
            }
        });    
        consumer.registerEventHandler(new SDCConsumerStateChangedHandler<RealTimeSampleArrayMetricState>("handle_stream") {
            
            @Override
            public void onStateChanged(RealTimeSampleArrayMetricState state) {
                assertNotNull(state.getMetricValue());
                assertNotNull(state.getMetricValue().getSamples());                
                List<BigDecimal> values = state.getMetricValue().getSamples();
                StringBuilder sb = new StringBuilder();
                sb.append("Received: ");
                for (BigDecimal next : values) {
                    sb.append(next.doubleValue()).append(" ");
                }
                System.out.println(sb.toString()); 
            }
        });        
    }
    
    @AfterClass
    public static void tearDownClass() {
        if (consumer != null)
            consumer.close();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ExampleConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }        
        SoftICE.getInstance().shutdown();
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
        try {
            Thread.sleep(2000);
        } catch (Exception ex) {
            Logger.getLogger(ExampleProvider.class.getName()).log(Level.SEVERE, null, ex);
        }          
    }

    @Test
    public void testGetSet() {
        assertNotNull(consumer);
        if (consumer == null)
            return;                
        // Get
        NumericMetricState nms = consumer.requestState("handle_metric", NumericMetricState.class);
        assertNotNull(nms);
        assertNotNull(nms.getMetricValue());
        assertNotNull(nms.getMetricValue().getValue());
        // Set
        nms.getMetricValue().setValue(BigDecimal.TEN);
        for (int i=0; i < 100; i++) {
            FutureInvocationState fis = new FutureInvocationState();
            long startTime = System.currentTimeMillis();
            assertEquals(InvocationState.WAIT, consumer.commitState(nms, fis));
            long stopTime = System.currentTimeMillis();
            assertEquals(true, fis.waitReceived(InvocationState.FIN, 5000));
            long elapsedTime = stopTime - startTime;
            System.out.println("Time Set Value: " + elapsedTime);
        }        
        // Get
        long startTime = System.currentTimeMillis();
        nms = consumer.requestState("handle_metric", NumericMetricState.class);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Time Get Value: " + elapsedTime);
        assertNotNull(nms.getMetricValue());
        assertEquals(BigDecimal.TEN, nms.getMetricValue().getValue());        
    }
    
}
