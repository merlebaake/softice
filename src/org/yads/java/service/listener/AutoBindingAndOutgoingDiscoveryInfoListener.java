/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service.listener;

import org.yads.java.communication.structures.Binding;
import org.yads.java.communication.structures.CommunicationBinding;
import org.yads.java.communication.structures.DiscoveryAutoBinding;
import org.yads.java.communication.structures.DiscoveryBinding;
import org.yads.java.communication.structures.OutgoingDiscoveryInfo;

public interface AutoBindingAndOutgoingDiscoveryInfoListener extends CommunicationStructureListener {

	/**
	 * Will be called if a new {@link CommunicationBinding} is available.
	 * 
	 * @param binding
	 */
	public void announceNewCommunicationBindingAvailable(Binding binding, boolean isDiscovery);

	/**
	 * Will be called if a new {@link DiscoveryBinding} is available.
	 * 
	 * @param binding
	 */
	public void announceNewDiscoveryBindingAvailable(DiscoveryBinding binding, DiscoveryAutoBinding dab);

	/**
	 * Will be called if a new {@link OutgoingDiscoveryInfo} is available.
	 * 
	 * @param binding
	 */
	public void announceNewOutgoingDiscoveryInfoAvailable(OutgoingDiscoveryInfo outgoingDiscoveryInfo);

	/**
	 * Will be called if a new {@link CommunicationBinding} is destroyed.
	 * 
	 * @param binding
	 */
	public void announceCommunicationBindingDestroyed(Binding binding, boolean isDiscovery);

	/**
	 * Will be called if a new {@link DiscoveryBinding} is destroyed.
	 * 
	 * @param binding
	 */
	public void announceDiscoveryBindingDestroyed(DiscoveryBinding binding, DiscoveryAutoBinding dab);

	/**
	 * Will be called if a new {@link OutgoingDiscoveryInfo} is destroyed.
	 * 
	 * @param binding
	 */
	public void announceOutgoingDiscoveryInfoDestroyed(OutgoingDiscoveryInfo outgoingDiscoveryInfo);

	public String getPath();
}
