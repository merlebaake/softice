/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service;

import org.yads.java.message.metadata.GetResponseMessage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.yads.java.types.LocalizedString;
import org.yads.java.types.ThisDeviceMData;
import org.yads.java.types.ThisModelMData;
import org.yads.java.types.URI;
import org.yads.java.types.UnknownDataContainer;
import org.yads.java.types.XAddressInfo;
import org.yads.java.util.Log;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

/**
 * Class represents the common part of a proxy/local device also known as <code>Hosting Service</code>.
 */
public abstract class DeviceCommons implements Device {

	/** Model data locally set, or created with wsd:Get. */
	public ThisModelMData	modelMetadata;

	/** Device data locally set, or created with wsd:Get. */
	public ThisDeviceMData	deviceMetadata;

	/**
	 * Additional custom metadata of this device -> Elements =
	 * UnknownDataContainer
	 */
	public HashMap			customMData	= null;

	// ArrayList customMData = null;

	private Integer			hashCode	= null;

	/**
	 * Default constructor. Creates empty model metadata {@link ThisModelMData} and device metadata {@link ThisDeviceMData}. The metadata is part of the
	 * get response message {@link GetResponseMessage}.
	 */
	protected DeviceCommons() {
		this(new ThisModelMData(), new ThisDeviceMData());
	}

	/**
	 * Constructor. The specified metadata will be set to fields of the device.
	 * The metadata is part of the get response message {@link GetResponseMessage}.
	 * 
	 * @param modelMetadata This model metadata is part of the get response
	 *            message {@link GetResponseMessage}.
	 * @param deviceMetadata This device metadata is part of the get response
	 *            message {@link GetResponseMessage}.
	 */
	protected DeviceCommons(ThisModelMData modelMetadata, ThisDeviceMData deviceMetadata) {
		super();

		this.modelMetadata = modelMetadata;
		this.deviceMetadata = deviceMetadata;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder(getClass().getName());
		sb.append(" [ remote=").append(isRemote());
		sb.append(", endpointReference=").append(getEndpointReference());

		Iterator it = getPortTypes();
		if (it.hasNext()) {
			sb.append(", types={ ");
			while (it.hasNext()) {
				sb.append(it.next()).append(' ');
			}
			sb.append('}');
		}
		it = getScopes();
		if (it.hasNext()) {
			sb.append(", scopes={ ");
			while (it.hasNext()) {
				sb.append(it.next()).append(' ');
			}
			sb.append('}');
		}
		it = getTransportXAddressInfos();
		if (it.hasNext()) {
			sb.append(", xAddresses={ ");
			while (it.hasNext()) {
				sb.append(((XAddressInfo) it.next()).getXAddress()).append(' ');
			}
			sb.append('}');
		}
		sb.append(", metadataVersion=").append(getMetadataVersion());
		sb.append(", thisModel=").append(modelMetadata);
		sb.append(", thisDevice=").append(deviceMetadata);
		sb.append(" ]");
		return sb.toString();
	}

	// ----------------------- Model Metadata ------------------

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getManufacturer(java.lang.String)
	 */
	@Override
	public String getManufacturer(String lang) {
		if (modelMetadata == null) {
			return null;
		}
		LocalizedString manufacturer = modelMetadata.getManufacturerName(lang);
		if (manufacturer != null) {
			return manufacturer.getValue();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getManufacturers()
	 */
	@Override
	public Iterator getManufacturers() {
		if (modelMetadata == null) {
			new ArrayList().iterator();
		}
		List manufacturers = modelMetadata.getManufacturerNames();
		return manufacturers == null ? new ArrayList().iterator() : manufacturers.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getManufacturerUrl()
	 */
	@Override
	public String getManufacturerUrl() {
		if (modelMetadata == null) {
			return null;
		}
		URI manufacturerUrl = modelMetadata.getManufacturerUrl();
		if (manufacturerUrl != null) {
			return manufacturerUrl.toString();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getModelName(java.lang.String)
	 */
	@Override
	public String getModelName(String lang) {
		if (modelMetadata == null) {
			return null;
		}
		LocalizedString modelName = modelMetadata.getModelName(lang);
		if (modelName != null) {
			return modelName.getValue();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getModelNames()
	 */
	@Override
	public Iterator getModelNames() {
		if (modelMetadata == null) {
			new ArrayList().iterator();
		}
		List modelNames = modelMetadata.getModelNames();
		return modelNames == null ? new ArrayList().iterator() : modelNames.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getModelNumber()
	 */
	@Override
	public String getModelNumber() {
		if (modelMetadata == null) {
			return null;
		}
		return modelMetadata.getModelNumber();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getModelUrl()
	 */
	@Override
	public String getModelUrl() {
		if (modelMetadata == null) {
			return null;
		}
		URI modelUrl = modelMetadata.getModelUrl();
		if (modelUrl != null) {
			return modelUrl.toString();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getPresentationUrl()
	 */
	@Override
	public String getPresentationUrl() {
		if (modelMetadata == null) {
			return null;
		}
		URI presentationUrl = modelMetadata.getPresentationUrl();
		if (presentationUrl != null) {
			return presentationUrl.toString();
		}

		return null;
	}

	// ----------------------- Device Metadata ------------------------

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getFriendlyName(java.lang.String)
	 */
	@Override
	public String getFriendlyName(String lang) {
		if (deviceMetadata == null) {
			return null;
		}
		LocalizedString friendlyName = deviceMetadata.getFriendlyName(lang);
		if (friendlyName != null) {
			return friendlyName.getValue();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getFriendlyNames()
	 */
	@Override
	public Iterator getFriendlyNames() {
		if (deviceMetadata == null) {
			new ArrayList().iterator();
		}
		List friendlyNames = deviceMetadata.getFriendlyNames();
		return friendlyNames == null ? new ArrayList().iterator() : friendlyNames.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getFirmwareVersion()
	 */
	@Override
	public String getFirmwareVersion() {
		if (deviceMetadata == null) {
			return null;
		}
		return deviceMetadata.getFirmwareVersion();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getSerialNumber()
	 */
	@Override
	public String getSerialNumber() {
		if (deviceMetadata == null) {
			return null;
		}
		return deviceMetadata.getSerialNumber();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.service.Device#getCustomMData()
	 */
	@Override
	public UnknownDataContainer[] getCustomMData(String communicationManagerId) {
		if (customMData == null) {
			return null;
		}
		ArrayList metaDataList = (ArrayList) customMData.get(communicationManagerId);
		if (metaDataList == null) {
			return null;
		}
		UnknownDataContainer[] result = new UnknownDataContainer[metaDataList.size()];
		metaDataList.toArray(result);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DeviceCommons)) {
			return false;
		}

		DeviceCommons device = (DeviceCommons) obj;
		return this.getEndpointReference().equals(device.getEndpointReference());
	}

	@Override
	public int hashCode() {
		int currentHashCode = 31 + getEndpointReference().hashCode();

		if (hashCode == null) {
			hashCode = currentHashCode;
			return currentHashCode;
		}

		if (hashCode != currentHashCode) {
			Log.error("DeviceCommons.hashCode(): endpoint reference has been changed " + toString());
		}

		return currentHashCode;
	}

	/**
	 * Disconnects all ServiceReferences from device. If <code> resetServiceRefs <code>
	 * is true serviceReferences will be reseted too.
	 * 
	 * @param resetServiceRefs
	 */
	public abstract void disconnectAllServiceReferences(boolean resetServiceRefs);

}
