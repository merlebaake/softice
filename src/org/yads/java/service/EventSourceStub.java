/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service;

import org.yads.java.description.wsdl.WSDLOperation;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.util.Log;

public class EventSourceStub extends DefaultEventSource {

	private final Object	delegateSync	= new Object();

	EventDelegate			defaultDelegate	= new EventDelegate() {

												@Override
												public void solicitResponseReceived(DefaultEventSource event, ParameterValue paramValue, int eventNumber, ServiceSubscription subscription) {
													Log.error("DefaultEventSource.receivedSolicitResponse: Overwrite this method to receive solicit responses.");
												}
											};

	EventDelegate			delegate		= null;

	public EventSourceStub(WSDLOperation operation) {
		super(operation);
		delegate = defaultDelegate;

	}

	public EventDelegate getDelegate() {
		synchronized (delegateSync) {
			return delegate;
		}
	}

	public void setDelegate(EventDelegate delegate) {
		synchronized (delegateSync) {
			this.delegate = delegate;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.service.DefaultEventSource#solicitResponseReceived(org.
	 * yads.java.service.parameter.ParameterValue, int,
	 * org.yads.java.service.ServiceSubscription)
	 */
	@Override
	public void solicitResponseReceived(ParameterValue paramValue, int eventNumber, ServiceSubscription subscription) {
		synchronized (delegateSync) {
			delegate.solicitResponseReceived(this, paramValue, eventNumber, subscription);
		}
	}
}
