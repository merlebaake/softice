/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service;

import java.util.concurrent.atomic.AtomicLong;
import org.yads.java.types.AppSequence;

/**
 * Manages the application sequence of device.
 */
public class AppSequenceManager {

	/** Seconds till era when device started */
	private final AtomicLong	instanceId		= new AtomicLong(0);

	// private URI sequenceId; // optional

	/** last send message number */
	private final AtomicLong	messageNumber	= new AtomicLong(0);

	/**
	 * Resets application sequence
	 */
	public void reset() {
		instanceId.set(System.currentTimeMillis() / 1000);
		messageNumber.set(0);
	}

	/**
	 * Increments message number by one and returns AppSequence with this;
	 * 
	 * @return appSequence
	 */
	public AppSequence getNext() {
		return new AppSequence(instanceId.get(), messageNumber.incrementAndGet());
	}

}