/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service;

import java.util.Collection;
import org.yads.java.communication.structures.CommunicationBinding;

/**
 * This class represents a combination of the three needed communication informations to communicate with other endpoints.
 * In case of a device, a CommunicationBinding, Discoverybinding and a OutgoingdiscoveryInfo is need, to make sure, that all functions of the stack can be used.
 */
public class BindingContainer {

	Collection			discoverBindings		= null;

	Collection			outgoingdiscoveryInfos	= null;

	CommunicationBinding	communicationbinding	= null;

	public BindingContainer(Collection discoverBindings, Collection outgoingdiscoveryInfos, CommunicationBinding communicationbinding) {
		super();
		this.discoverBindings = discoverBindings;
		this.outgoingdiscoveryInfos = outgoingdiscoveryInfos;
		this.communicationbinding = communicationbinding;
	}

	public Collection getDiscoveryBindings() {
		return discoverBindings;
	}

	public void setDiscoverBindings(Collection discoverBindings) {
		this.discoverBindings = discoverBindings;
	}

	public Collection getOutgoingdiscoveryInfos() {
		return outgoingdiscoveryInfos;
	}

	public void setOutgoingdiscoveryInfos(Collection outgoingdiscoveryInfos) {
		this.outgoingdiscoveryInfos = outgoingdiscoveryInfos;
	}

	public CommunicationBinding getCommunicationBinding() {
		return communicationbinding;
	}

	public void setCommunicationbinding(CommunicationBinding communicationbinding) {
		this.communicationbinding = communicationbinding;
	}

}