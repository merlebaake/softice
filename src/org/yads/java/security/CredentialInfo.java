/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.security;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.yads.java.communication.structures.DiscoveryDomain;
import org.yads.java.service.Device;
import org.yads.java.service.listener.DeviceListener;
import org.yads.java.service.reference.DeviceReference;
import org.yads.java.types.Memento;
import org.yads.java.types.MementoSupport;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;
import org.yads.java.util.WS4DIllegalStateException;

/**
 * Abstract implementation of security credentials. If no credential info is
 * available use always {@link #EMPTY_CREDENTIAL_INFO} instead of <code>null</code>.
 */
public final class CredentialInfo implements DeviceListener, MementoSupport {

	public static final CredentialInfo	EMPTY_CREDENTIAL_INFO	= new CredentialInfo();

	private static final String			M_SECURE_MESSAGES_IN	= "sec_msg_in";

	private static final String			M_SECURE_MESSAGES_OUT	= "sec_msg_out";

	private static final String			M_HASH_CODE				= "hash_code";

	private static final String			M_LOCKED				= "locked";

	// key: class , value: object
	protected HashMap					credentials				= new HashMap();

	// key: domain , value: set of device references
	protected HashMap					discoveryProxies		= new HashMap();

	protected boolean					secureMessagesIn		= true;

	protected boolean					secureMessagesOut		= true;

	protected int						hashCode;

	protected boolean					locked					= false;

	static {
		EMPTY_CREDENTIAL_INFO.secureMessagesOut = false;
		EMPTY_CREDENTIAL_INFO.secureMessagesIn = false;
		EMPTY_CREDENTIAL_INFO.hashCode();
	}

	protected CredentialInfo() {}

	public CredentialInfo(Object credential) {
		addCredential(credential);
	}

	public CredentialInfo(Class key, Object credential) {
		addCredential(key, credential);
	}

	public void addCredential(Object credential) {
		if (locked) {
			throw new WS4DIllegalStateException("CredentialInfo must not be changed after first use of hashCode()!");
		}
		credentials.put(credential.getClass(), credential);
	}

	public void addCredential(Class key, Object credential) {
		if (locked) {
			throw new WS4DIllegalStateException("CredentialInfo must not be changed after first use of hashCode()!");
		}
		credentials.put(key, credential);
	}

	public Object getCredential(Class key) {
		return credentials.get(key);
	}

	public boolean isSecureMessagesIn() {
		return secureMessagesIn;
	}

	public void setSecureMessagesIn(boolean secureMessagesIn) {
		if (locked && secureMessagesIn != this.secureMessagesIn) {
			throw new WS4DIllegalStateException("CredentialInfo must not be changed after first use of hashCode()!");
		}
		this.secureMessagesIn = secureMessagesIn;
	}

	public boolean isSecureMessagesOut() {
		return secureMessagesOut;
	}

	public void setSecureMessagesOut(boolean secureMessagesOut) {
		if (locked && secureMessagesOut != this.secureMessagesOut) {
			throw new WS4DIllegalStateException("CredentialInfo must not be changed after first use of hashCode()!");
		}
		this.secureMessagesOut = secureMessagesOut;
	}

	public HashSet getDiscoveryProxiesForDomain(DiscoveryDomain domain) {
		return (HashSet) discoveryProxies.get(domain);
	}

	/**
	 * Add a DeviceReference to the list of DiscoveryProxies
	 * 
	 * @param devRef
	 */
	public synchronized void addDiscoveryProxyForDomain(DiscoveryDomain domain, DeviceReference devRef) {
		HashSet set = (HashSet) discoveryProxies.get(domain);
		if (set == null) {
			set = new HashSet();
			discoveryProxies.put(domain, set);
		}
		set.add(devRef);
	}

	/**
	 * Removes a DeviceReference from the List of Proxies
	 * 
	 * @param devRef
	 */
	public synchronized void removeDiscoveryProxyForDomain(DiscoveryDomain domain, DeviceReference devRef) {
		HashSet set = (HashSet) discoveryProxies.get(domain);
		if (set != null) {
			set.remove(devRef);
			if (set.isEmpty()) {
				discoveryProxies.remove(domain);
			}
		}
	}

	@Override
	public void deviceRunning(DeviceReference deviceRef) {}

	@Override
	public void deviceCompletelyDiscovered(DeviceReference deviceRef) {}

	@Override
	public synchronized void deviceBye(DeviceReference deviceRef) {
		Iterator it = discoveryProxies.values().iterator();
		while (it.hasNext()) {
			HashSet set = (HashSet) it.next();
			set.remove(deviceRef);
			if (set.isEmpty()) {
				it.remove();
			}
		}
	}

	@Override
	public void deviceChanged(DeviceReference deviceRef) {}

	@Override
	public void deviceBuiltUp(DeviceReference deviceRef, Device device) {}

	@Override
	public void deviceCommunicationErrorOrReset(DeviceReference deviceRef) {}

	@Override
	public int hashCode() {
		if (locked) {
			return hashCode;
		}

		final int prime = 31;
		int result = 1;
		result = prime * result + ((credentials == null) ? 0 : credentials.hashCode());
		result = prime * result + (secureMessagesOut ? 1231 : 1237);
		result = prime * result + (secureMessagesIn ? 1231 : 1237);
		hashCode = result;
		locked = true;

		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		CredentialInfo other = (CredentialInfo) obj;
		if (credentials == null) {
			if (other.credentials != null) return false;
		} else if (!credentials.equals(other.credentials)) return false;
		if (secureMessagesOut != other.secureMessagesOut) return false;
		if (secureMessagesIn != other.secureMessagesIn) return false;
		return true;
	}

	@Override
	public String toString() {
		SimpleStringBuilder buf = Toolkit.getInstance().createSimpleStringBuilder();
		buf.append("CredentialInfo[ Credentials:[ ");
		for (Iterator iterator = credentials.values().iterator(); iterator.hasNext();) {
			Object cri = iterator.next();
			buf.append(cri.toString()).append(';');
		}
		buf.append("], SecureMessagesIn[ ");
		buf.append(secureMessagesIn);
		buf.append("],SecureMessagesOut[ ");
		buf.append(secureMessagesOut);
		buf.append("]]");

		return buf.toString();
	}

	@Override
	public void saveToMemento(Memento m) {
		// TODO: (WIP) Kroeger
		m.putValue(M_SECURE_MESSAGES_IN, secureMessagesIn);
		m.putValue(M_SECURE_MESSAGES_OUT, secureMessagesOut);
		m.putValue(M_HASH_CODE, hashCode);
		m.putValue(M_LOCKED, locked);
	}

	@Override
	public void readFromMemento(Memento m) throws IOException {
		// TODO: (WIP) Kroeger
		secureMessagesIn = m.getBooleanValue(M_SECURE_MESSAGES_IN, false);
		secureMessagesOut = m.getBooleanValue(M_SECURE_MESSAGES_OUT, false);
		hashCode = m.getIntValue(M_HASH_CODE, 0);
		locked = m.getBooleanValue(M_LOCKED, false);
	}
}
