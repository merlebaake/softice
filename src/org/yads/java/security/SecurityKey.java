/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.security;

import java.util.Collection;
import java.util.Iterator;
import org.yads.java.communication.structures.OutgoingDiscoveryInfo;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

public class SecurityKey {

	public static final SecurityKey	EMPTY_KEY				= new SecurityKey(null, CredentialInfo.EMPTY_CREDENTIAL_INFO);

	private Collection  			outgoingDiscoveryInfos	= null;

	private CredentialInfo			localCredentialInfo		= CredentialInfo.EMPTY_CREDENTIAL_INFO;

	public SecurityKey(Collection outgoingDiscoveryInfos, CredentialInfo localCredentialInfo) {
		this.outgoingDiscoveryInfos = outgoingDiscoveryInfos;
		if (localCredentialInfo != null && localCredentialInfo != CredentialInfo.EMPTY_CREDENTIAL_INFO) {
			this.localCredentialInfo = localCredentialInfo;
		}
	}

	public Collection getOutgoingDiscoveryInfos() {
		return outgoingDiscoveryInfos;
	}

	public CredentialInfo getLocalCredentialInfo() {
		return localCredentialInfo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localCredentialInfo == null) ? 0 : localCredentialInfo.hashCode());
		result = prime * result + ((outgoingDiscoveryInfos == null) ? 0 : outgoingDiscoveryInfos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		SecurityKey other = (SecurityKey) obj;
		if (localCredentialInfo == null) {
			if (other.localCredentialInfo != null) return false;
		} else if (!localCredentialInfo.equals(other.localCredentialInfo)) return false;
		if (outgoingDiscoveryInfos == null) {
			if (other.outgoingDiscoveryInfos != null) return false;
		} else if (!outgoingDiscoveryInfos.equals(other.outgoingDiscoveryInfos)) return false;
		return true;
	}

	@Override
	public String toString() {
		SimpleStringBuilder buf = Toolkit.getInstance().createSimpleStringBuilder();
		buf.append("SecurityKey[ OutgoingDiscoveryInfos[ ");
		if (outgoingDiscoveryInfos != null) {
			for (Iterator iterator = outgoingDiscoveryInfos.iterator(); iterator.hasNext();) {
				OutgoingDiscoveryInfo odi = (OutgoingDiscoveryInfo) iterator.next();
				buf.append(odi.toString());
			}
		} else {
			buf.append("null ");
		}

		buf.append("], CredentialInfo [");
		buf.append(localCredentialInfo);
		buf.append("]]");

		return buf.toString();
	}
}
