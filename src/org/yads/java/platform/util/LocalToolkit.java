/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.platform.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.yads.java.YADSFramework;
import org.yads.java.configuration.FrameworkProperties;
import org.yads.java.util.Log;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

public final class LocalToolkit extends Toolkit {

	private volatile boolean	shutdownAdded	= false;

	public LocalToolkit() {
		addShutdownHook();
	}

	private synchronized void addShutdownHook() {
		if (shutdownAdded) {
			return;
		}
		Thread t = new Thread() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				if (FrameworkProperties.getInstance().getKillOnShutdownHook()) {
					YADSFramework.kill();

					/*
					 * Allow the framework to do its job for one second. After
					 * that time the framework and the JavaVM is killed.
					 */
					if (YADSFramework.isRunning()) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					if (Log.isDebug() && YADSFramework.isRunning()) {
						Log.debug("Killing JMEDS Framework and JavaVM");
					}
					Runtime.getRuntime().halt(0);
				} else {
					YADSFramework.stop();
				}
			}

		};
		Runtime.getRuntime().addShutdownHook(t);
		shutdownAdded = true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.util.Toolkit#printStackTrace(java.io.PrintStream,
	 * java.lang.Throwable)
	 */
	@Override
	public void printStackTrace(PrintStream err, Throwable t) {
		t.printStackTrace(err);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.util.Toolkit#getStackTrace(java.lang.Throwable)
	 */
	@Override
	public String[] getStackTrace(Throwable t) {
		StackTraceElement[] elements = t.getStackTrace();
		String[] result = new String[elements.length];
		for (int a = 0; a < elements.length; a++) {
			result[a] = elements[a].getClassName() + "." + elements[a].getMethodName() + " at " + elements[a].getLineNumber();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.util.Toolkit#writeBufferToStream(java.io.ByteArrayOutputStream
	 * , java.io.OutputStream)
	 */
	@Override
	public void writeBufferToStream(ByteArrayOutputStream source, OutputStream target) throws IOException {
		if (source != null) {
			source.writeTo(target);
		}
	}

	@Override
	public InputStream buffer(InputStream stream) {
		if (stream instanceof java.io.BufferedInputStream) {
			return stream;
		} else {
			return new BufferedInputStream(stream);
		}
	}

	@Override
	public SimpleStringBuilder createSimpleStringBuilder() {
		return new SimpleStringBuilderImpl();
	}

	@Override
	public SimpleStringBuilder createSimpleStringBuilder(int capacity) {
		return new SimpleStringBuilderImpl(capacity);
	}

	@Override
	public SimpleStringBuilder createSimpleStringBuilder(String str) {
		return new SimpleStringBuilderImpl(str);
	}
}
