/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.message.discovery;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.message.Message;
import org.yads.java.message.SOAPHeader;
import org.yads.java.security.CredentialInfo;
import java.util.HashMap;

public abstract class SignableMessage extends Message {

	public SignableMessage(SOAPHeader header) {
		super(header);
	}

	public static final int	UNKNOWN					= -1;

	public static final int	NOT_SIGNED				= 0;

	public static final int	VALID_SIGNATURE			= 1;

	public static final int	INVALID_SIGNATURE		= 2;

	public static final int	CERTIFICATE_NOT_FOUND	= 3;

	private boolean			messageIsSigned			= true;

	private HashMap			checkedCredentials		= new HashMap();

	/**
	 * Return the validation status of a message
	 * 
	 * @return String
	 */

	public static String getValidationStatusText(int status) {
		switch (status) {
			case SignableMessage.NOT_SIGNED:
				return "not signed";
			case SignableMessage.VALID_SIGNATURE:
				return "valid";
			case SignableMessage.CERTIFICATE_NOT_FOUND:
				return "no cert";
			case SignableMessage.INVALID_SIGNATURE:
				return "not valid";
			case SignableMessage.UNKNOWN:
				return "unknown";
		}
		return "unknown";
	}

	/**
	 * Validates a signed or not signed message.
	 * 
	 * @param connectionInfo
	 * @param defaultKeyId
	 * @return int value for validation status
	 */
	public int validateMessage(ConnectionInfo connectionInfo, String defaultKeyId) {
		CredentialInfo credentialInfo = connectionInfo.getLocalCredentialInfo();
		if (!messageIsSigned) {
			return NOT_SIGNED;
		}

		Integer checked = (Integer) checkedCredentials.get(credentialInfo);
		if (checked != null) {
			return checked.intValue();
		}

		return SignableMessage.UNKNOWN;
	}

	public void setMessageIsSigned(boolean messageIsSigned) {
		this.messageIsSigned = messageIsSigned;
	}

	public boolean isMessageSigned() {
		return messageIsSigned;
	}
}