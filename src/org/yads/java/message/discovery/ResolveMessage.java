/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.message.discovery;

import org.yads.java.constants.MessageConstants;
import org.yads.java.message.SOAPHeader;
import org.yads.java.types.EndpointReference;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.StringUtil;
import org.yads.java.util.Toolkit;

public class ResolveMessage extends SignableMessage {

	private EndpointReference	endpointReference;

	/**
	 * Creates a new Resolve message with a new created discovery- {@link SOAPHeader}.
	 */
	public ResolveMessage() {
		this(MessageWithDiscoveryData.createDiscoveryHeader());
	}

	/**
	 * Creates a new Resolve message containing a {@link SOAPHeader}. All
	 * header- and discovery-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 */
	public ResolveMessage(SOAPHeader header) {
		this(header, null);
	}

	/**
	 * Creates a new Resolve message containing a {@link SOAPHeader} and a {@link EndpointReference}. All header- and discovery-related fields are
	 * empty and it is the caller's responsibility to fill them with suitable
	 * values.
	 */
	public ResolveMessage(SOAPHeader header, EndpointReference endpointReference) {
		super(header);
		this.endpointReference = endpointReference;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", endpointReference=").append(endpointReference);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.Message#getType()
	 */
	@Override
	public int getType() {
		return MessageConstants.RESOLVE_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.message.discovery.ResolveMessage#
	 * getEndpointReference()
	 */
	public EndpointReference getEndpointReference() {
		return endpointReference;
	}

	/**
	 * @param endpointReference the endpointReference to set
	 */
	public void setEndpointReference(EndpointReference endpointReference) {
		this.endpointReference = endpointReference;
	}
}
