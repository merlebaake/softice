/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.message.discovery;

import org.yads.java.message.SOAPHeader;
import org.yads.java.service.LocalDevice;
import org.yads.java.types.AttributedURI;
import org.yads.java.types.DiscoveryData;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.QNameSet;
import org.yads.java.types.ScopeSet;
import org.yads.java.types.XAddressInfoSet;
import org.yads.java.util.IDGenerator;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.StringUtil;
import org.yads.java.util.Toolkit;

public abstract class MessageWithDiscoveryData extends SignableMessage {

	private DiscoveryData	discoveryData	= null;

	private LocalDevice		localDevice		= null;

	public static SOAPHeader createDiscoveryHeader() {
		SOAPHeader header = SOAPHeader.createHeader();
		header.setMessageId(new AttributedURI(IDGenerator.getUUIDasURI()));
		return header;
	}

	MessageWithDiscoveryData(SOAPHeader header, DiscoveryData discoveryData, LocalDevice localDevice) {
		super(header);
		this.discoveryData = discoveryData;
		this.localDevice = localDevice;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", discoveryData=").append(discoveryData);
		sb.append(" ]");
		return sb.toString();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.yads.java.types.DiscoveryData#getEndpointReference()
	 */
	public EndpointReference getEndpointReference() {
		return discoveryData.getEndpointReference();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.yads.java.types.DiscoveryData#getMetadataVersion()
	 */
	public long getMetadataVersion() {
		return discoveryData.getMetadataVersion();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.yads.java.types.DiscoveryData#getTypes()
	 */
	public QNameSet getTypes() {
		return discoveryData.getTypes();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.yads.java.types.DiscoveryData#getScopes()
	 */
	public ScopeSet getScopes() {
		return discoveryData.getScopes();
	}

	/**
	 * @return a {@link XAddressInfoSet}
	 */
	public XAddressInfoSet getXAddressInfoSet() {
		return discoveryData.getXAddressInfoSet();
	}

	/**
	 * Get discovery data.
	 * 
	 * @return Discovery data.
	 */
	public DiscoveryData getDiscoveryData() {
		return discoveryData;
	}

	/**
	 * @param discoveryData the discoveryData to set
	 */
	public void setDiscoveryData(DiscoveryData discoveryData) {
		this.discoveryData = discoveryData;
	}

	public LocalDevice getLocalDevice() {
		return localDevice;
	}
}
