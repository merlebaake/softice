/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.message.metadata;

import org.yads.java.constants.MessageConstants;
import org.yads.java.message.Message;
import org.yads.java.message.SOAPHeader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.yads.java.types.HostMData;
import org.yads.java.types.RelationshipMData;
import org.yads.java.types.ThisDeviceMData;
import org.yads.java.types.ThisModelMData;
import org.yads.java.types.UnknownDataContainer;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.StringUtil;
import org.yads.java.util.Toolkit;

public class GetResponseMessage extends Message {

	private ThisModelMData		thisModel;

	private ThisDeviceMData		thisDevice;

	private RelationshipMData	relationship;

	private HashMap				customMData;

	/**
	 * Creates a new GetResponse message containing a new created {@link SOAPHeader}. All header- and transfer-related fields are empty and
	 * it is the caller's responsibility to fill them with suitable values.
	 */
	public GetResponseMessage() {
		this(SOAPHeader.createHeader());
	}

	/**
	 * Creates a new GetResponse message with the given {@link SOAPHeader}.
	 * 
	 * @param header
	 */
	public GetResponseMessage(SOAPHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", thisModel=").append(thisModel);
		sb.append(", thisDevice=").append(thisDevice);
		sb.append(", relationship=").append(relationship);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.Message#getType()
	 */
	@Override
	public int getType() {
		return MessageConstants.GET_RESPONSE_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.metadata.GetResponseMessage #getThisDevice()
	 */
	public ThisDeviceMData getThisDevice() {
		return thisDevice;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.metadata.GetResponseMessage #getThisModel()
	 */
	public ThisModelMData getThisModel() {
		return thisModel;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.metadata.GetResponseMessage #getRelationship()
	 */
	public RelationshipMData getRelationship() {
		return relationship;
	}

	public HostMData getHost() {
		return relationship == null ? null : relationship.getHost();
	}

	public List getHosted() {
		return relationship == null ? null : relationship.getHosted();
	}

	/**
	 * @param thisModel the thisModel to set
	 */
	public void setThisModel(ThisModelMData thisModel) {
		this.thisModel = thisModel;
	}

	/**
	 * @param thisDevice the thisDevice to set
	 */
	public void setThisDevice(ThisDeviceMData thisDevice) {
		this.thisDevice = thisDevice;
	}

	/**
	 * @param relationship the relationship to set
	 */
	public void addRelationship(RelationshipMData relationship) {
		if (this.relationship == null) {
			this.relationship = relationship;
		} else {
			this.relationship.mergeWith(relationship);
		}
	}

	/**
	 * @param container instance of the type {@link UnknownDataContainer}
	 */
	public void addCustomMData(String communicationManagerId, UnknownDataContainer container) {
		ArrayList tempMetaData = null;
		if (customMData == null) {
			customMData = new HashMap();
		} else {
			tempMetaData = (ArrayList) customMData.get(communicationManagerId);
		}

		if (tempMetaData == null) {
			tempMetaData = new ArrayList();
			customMData.put(communicationManagerId, tempMetaData);
		}
		tempMetaData.add(container);
	}

	/**
	 * @param customMData {@link HashMap} of CustomMData
	 */
	public void setCustomMData(HashMap customMData) {
		this.customMData = customMData;
	}

	/**
	 * @return the instance of the typ CustomMData
	 */
	public ArrayList getCustomMData(String communicationManagerId) {
		if (customMData == null) {
			return null;
		}
		return (ArrayList) customMData.get(communicationManagerId);
	}

	/**
	 * @return the instance of the typ CustomMData
	 */
	public HashMap getCustomMData() {
		return customMData;
	}

}
