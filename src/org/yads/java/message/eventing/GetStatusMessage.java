/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.message.eventing;

import org.yads.java.constants.MessageConstants;
import org.yads.java.eventing.ClientSubscription;
import org.yads.java.message.Message;
import org.yads.java.message.SOAPHeader;

public class GetStatusMessage extends Message {

	private final ClientSubscription	subscription;

	/**
	 * Creates a new GetStatus message containing a new created {@link SOAPHeader}. All header- and eventing-related fields are empty and
	 * it is the caller's responsibility to fill them with suitable values.
	 */
	public GetStatusMessage(ClientSubscription subscription) {
		super(SOAPHeader.createRequestHeader());
		this.subscription = subscription;
	}

	/**
	 * Creates a new GetStatus message containing a {@link SOAPHeader}. All
	 * header- and eventing-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 * 
	 * @param header
	 */
	public GetStatusMessage(SOAPHeader header) {
		super(header);
		this.subscription = null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.Message#getType()
	 */
	@Override
	public int getType() {
		return MessageConstants.GET_STATUS_MESSAGE;
	}

	public ClientSubscription getSubscription() {
		return subscription;
	}
}
