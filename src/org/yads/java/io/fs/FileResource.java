/*******************************************************************************
 * Copyright (c) 2016 Andreas Besting (info@besting-it.de)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *
 *******************************************************************************/

package org.yads.java.io.fs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import org.yads.java.communication.RequestHeader;
import org.yads.java.communication.Resource;
import static org.yads.java.description.wsdl.WSDL.HTTP_HEADER_LAST_MODIFIED;
import org.yads.java.security.CredentialInfo;
import org.yads.java.types.ContentType;
import org.yads.java.types.URI;
import org.yads.java.util.StringUtil;


/**
 *
 * @author besting
 */
public class FileResource implements Resource {

    private ContentType	contentType = new ContentType("FILE", "FILE", "file");
    private String localResourcePath, fileName;
    private long lastModified = 0L;

    public FileResource(String localResourcePath) {
        this.localResourcePath = localResourcePath;
        lastModified = new Date().getTime();
    }    

    public FileResource(String localResourcePath, String fileName) {
        this.localResourcePath = localResourcePath;
        this.fileName = fileName;
        lastModified = new Date().getTime();
    }    

    public String getFileName() {
        return fileName != null? fileName : localResourcePath.substring(localResourcePath.lastIndexOf('/') + 1);
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setLocalResourcePath(String localResourcePath) {
        this.localResourcePath = localResourcePath;
        lastModified = new Date().getTime();
    }

    public String getLocalResourcePath() {
        return localResourcePath;
    }

    @Override
    public String toString() {
        return getFileName();
    }    
    
    @Override
    public ContentType getContentType() {
        return contentType;
    }

    @Override
    public void serialize(URI request, RequestHeader requestHeader, InputStream requestBody, OutputStream out, CredentialInfo credentialInfo, String comManId) throws IOException {
        InputStream is = FileResource.class.getResourceAsStream(getLocalResourcePath());
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String nextLine;
            while ((nextLine = br.readLine()) != null) {
                out.write(nextLine.getBytes(Charset.defaultCharset()));
            }
        } finally {
            out.flush();
        }         
    }

    @Override
	public HashMap getHeaderFields() {
		HashMap map = new HashMap();
		map.put(HTTP_HEADER_LAST_MODIFIED, StringUtil.getHTTPDate(lastModified));
		return map;
	}

    @Override
	public long size() {
//        try {
//            return new File(FileResource.class.getResource(getLocalResourcePath()).toURI()).length();
//        } catch (URISyntaxException ex) {
//            Logger.getLogger(FileResource.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return -1;
	}

    @Override
    public long getLastModifiedDate() {
        return lastModified;
    }

    @Override
    public String shortDescription() {
        return getFileName();
    }

    @Override
    public Object getKey() {
        return getFileName();
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }
    
}
