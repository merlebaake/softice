/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.io.xml;

import java.io.IOException;

import org.yads.java.xmlpull.mxp1.MXParser;
import org.yads.java.xmlpull.v1.XmlPullParserException;

public class DefaultWs4dXmlPullParser extends MXParser implements Ws4dXmlPullParser {

	private Ws4dXmlPullParserListener	listener;

	@Override
	protected void reset() {
		listener = null;
		super.reset();
	}

	@Override
	public void setListener(Ws4dXmlPullParserListener listener) {
		if (listener == null) {
			return;
		}
		if (this.listener != null) {
			throw new RuntimeException("listener already set");
		}
		this.listener = listener;
	}

	@Override
	public void removeListener(Ws4dXmlPullParserListener listener) {
		if (listener == this.listener) {
			this.listener = listener;
		}
	}

	@Override
	public void removeListener() {
		listener = null;
	}

	@Override
	protected int nextImpl() throws XmlPullParserException, IOException {
		int result = super.nextImpl();
		if (listener != null) {
			listener.notify(posStart, posEnd - 1);
		}
		return result;
	}
}
