/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.io.xml;

import java.io.IOException;
import java.io.InputStream;

public class CharacterAddInputStream extends InputStream {

	private InputStream	in;

	private byte		b;

	private boolean		firstUse	= true;

	public CharacterAddInputStream(InputStream in, byte b) {
		this.in = in;
		this.b = b;
	}

	@Override
	public int read() throws IOException {
		if (firstUse) {
			firstUse = false;
			return b & 0xFF;
		}
		return in.read();
	}

	@Override
	public int read(byte[] b) throws IOException {
		if (firstUse) {
			if (b == null) {
				throw new NullPointerException();
			}
			if (b.length == 0) {
				return 0;
			}
			firstUse = false;
			b[0] = this.b;
			if (b.length > 1 && in.available() > 0) {
				return read(b, 1, b.length - 1);
			}
			return 1;
		}
		return in.read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (firstUse) {
			if (b == null) {
				throw new NullPointerException();
			}
			if (off < 0 || len < 0 || len > b.length - off) {
				throw new IndexOutOfBoundsException();
			}
			if (b.length == 0 || len == 0) {
				return 0;
			}
			firstUse = false;
			b[off++] = this.b;
			if (--len > b.length - off && in.available() > 0) {
				read(b, off, len);
			}
		}
		return in.read(b, off, len);
	}

	@Override
	public int available() throws IOException {
		if (firstUse) {
			firstUse = false;
			return in.available() + 1;
		}
		return in.available();
	}

	@Override
	public void close() throws IOException {
		in.close();
		firstUse = false;
	}

	@Override
	public long skip(long n) throws IOException {
		if (firstUse) {
			if (n < 1) {
				return 0;
			}
			firstUse = false;
			if (--n > 0) {
				return in.skip(n) + 1;
			}
			return 1;
		} else {
			return in.skip(n);
		}
	}

}
