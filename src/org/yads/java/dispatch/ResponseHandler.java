/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.dispatch;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.message.FaultMessage;
import org.yads.java.message.InvokeMessage;
import org.yads.java.message.Message;
import org.yads.java.message.discovery.ProbeMatchesMessage;
import org.yads.java.message.discovery.ResolveMatchesMessage;
import org.yads.java.message.eventing.RenewResponseMessage;
import org.yads.java.message.eventing.SubscribeResponseMessage;
import org.yads.java.message.eventing.UnsubscribeResponseMessage;
import org.yads.java.message.metadata.GetMetadataResponseMessage;
import org.yads.java.message.metadata.GetResponseMessage;

public interface ResponseHandler {

	public void handle(ProbeMatchesMessage probeMatches, ConnectionInfo connectionInfo);

	public void handle(ResolveMatchesMessage resolveMatches, ConnectionInfo connectionInfo);

	public void handle(GetResponseMessage getResponse, ConnectionInfo connectionInfo);

	public void handle(GetMetadataResponseMessage getMetadataResponse, ConnectionInfo connectionInfo);

	public void handle(SubscribeResponseMessage subscribeResponse, ConnectionInfo connectionInfo);

	public void handle(UnsubscribeResponseMessage unsubscribeResponse, ConnectionInfo connectionInfo);

	public void handle(RenewResponseMessage renewResponse, ConnectionInfo connectionInfo);

	public void handle(InvokeMessage invoke, ConnectionInfo connectionInfo);

	public void handle(FaultMessage fault, ConnectionInfo connectionInfo);

	public Message getRequestMessage();

}
