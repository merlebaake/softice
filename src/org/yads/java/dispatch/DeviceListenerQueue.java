/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.dispatch;

import java.util.LinkedList;
import org.yads.java.YADSFramework;
import org.yads.java.service.Device;
import org.yads.java.service.LocalDevice;
import org.yads.java.service.listener.DeviceListener;
import org.yads.java.service.reference.DeviceReference;
import java.util.List;
import org.yads.java.util.Log;

/**
 *
 */
class DeviceListenerQueue {

	static final byte				DEVICE_RUNNING_EVENT						= 1;

	static final byte				DEVICE_COMPLETELY_DISCOVERED_EVENT			= 2;

	static final byte				DEVICE_BUILT_UP_EVENT						= 3;

	static final byte				DEVICE_BYE_EVENT							= 4;

	static final byte				DEVICE_CHANGED_EVENT						= 5;

	static final byte				DEVICE_CHANGED_AND_BUILT_UP_EVENT			= 6;

	static final byte				DEVICE_RUNNING_AND_BUILT_UP_EVENT			= 7;

	static final byte				DEVICE_COMMUNICATION_ERROR_OR_RESET_EVENT	= 8;

	private final DeviceListener	listener;

	private final DeviceReference	devRef;

	private List					queue										= new LinkedList();

	private DeviceEvent				currentEvent								= null;

	/**
	 * 
	 */
	DeviceListenerQueue(DeviceListener listener, DeviceReference devRef) {
		this.listener = listener;
		this.devRef = devRef;
	}

	synchronized void announce(DeviceEvent event) {
		if (currentEvent == null) {
			currentEvent = event;
			YADSFramework.getThreadPool().execute(new Runnable() {

				/*
				 * (non-Javadoc)
				 * @see java.lang.Runnable#run()
				 */
				@Override
				public void run() {
					while (true) {
						deliverCurrentEvent();
						synchronized (DeviceListenerQueue.this) {
							boolean endOfLoop = true;
							while (queue.size() > 0) {
								DeviceEvent first = (DeviceEvent) queue.remove(0);
								if (first.eventType != currentEvent.eventType) {
									currentEvent = first;
									endOfLoop = false;
									break;
								}
							}
							if (endOfLoop) {
								currentEvent = null;
								return;
							}
						}
					}
				}

			});
		} else {
			queue.add(event);
		}
	}

	private void deliverCurrentEvent() {
		try {
			switch (currentEvent.eventType) {
				case (DEVICE_RUNNING_EVENT): {
					listener.deviceRunning(devRef);
					break;
				}
				case (DEVICE_COMPLETELY_DISCOVERED_EVENT): {
					listener.deviceCompletelyDiscovered(devRef);
					break;
				}
				case (DEVICE_BUILT_UP_EVENT): {
					if (devRef.isDeviceObjectExisting() && checkAuthorization(devRef, currentEvent)) {
						listener.deviceBuiltUp(devRef, currentEvent.device);
					}
					break;
				}
				case (DEVICE_BYE_EVENT): {
					listener.deviceBye(devRef);
					break;
				}
				case (DEVICE_CHANGED_EVENT): {
					listener.deviceChanged(devRef);
					break;
				}
				case (DEVICE_CHANGED_AND_BUILT_UP_EVENT): {
					if (devRef.isDeviceObjectExisting() && checkAuthorization(devRef, currentEvent)) {
						listener.deviceChanged(devRef);
						listener.deviceBuiltUp(devRef, currentEvent.device);
					}
					break;
				}
				case (DEVICE_RUNNING_AND_BUILT_UP_EVENT): {
					if (devRef.isDeviceObjectExisting() && checkAuthorization(devRef, currentEvent)) {
						listener.deviceRunning(devRef);
						listener.deviceBuiltUp(devRef, currentEvent.device);
					}
					break;
				}
				case (DEVICE_COMMUNICATION_ERROR_OR_RESET_EVENT): {
					listener.deviceCommunicationErrorOrReset(devRef);
					break;
				}
			}
		} catch (Throwable t) {
			Log.error("Exception during listener notification: " + t.getMessage());
			Log.printStackTrace(t);
		}
	}

	private boolean checkAuthorization(DeviceReference devRef, DeviceEvent deviceEvent) {
		if (deviceEvent.device instanceof LocalDevice) {
			LocalDevice localDevice = (LocalDevice) deviceEvent.device;
		}
		return true;
	}

	static class DeviceEvent {

		byte	eventType;

		Device	device;

		DeviceEvent(byte eventType, Device device) {
			this.eventType = eventType;
			this.device = device;
		}

	}

}
