/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.eventing;

import org.yads.java.message.FaultMessage;
import org.yads.java.service.InvocationException;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.types.QName;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

/**
 * WS-Eventing exception.
 */
public class EventingException extends InvocationException {

	private static final long	serialVersionUID		= 1L;

	public static final int		UNKNOWN_EXCEPTION		= -1;

	private int					eventingExceptionType	= UNKNOWN_EXCEPTION;

	/**
	 * @param subcode
	 * @param reason
	 */
	public EventingException(int type, String action, QName code, QName subcode, String reason, ParameterValue detail) {
		super(action, code, subcode, createReasonFromString(reason), detail);
		this.eventingExceptionType = type;
	}

	public EventingException(int type, FaultMessage fault) {
		super(fault);
		this.eventingExceptionType = type;
	}

	public int getExceptionType() {
		return eventingExceptionType;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder(getClass().getName());
		sb.append(": [ action=").append(action);
		sb.append(", code=").append(code);
		sb.append(", eventingExceptionType=").append(eventingExceptionType);
		sb.append(", reason=").append(reason);
		sb.append(", detail=").append(detail);
		sb.append(" ]");
		return sb.toString();
	}

}
