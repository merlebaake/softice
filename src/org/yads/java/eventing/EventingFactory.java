/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.eventing;

import java.util.Collection;
import org.yads.java.constants.FrameworkConstants;
import org.yads.java.description.wsdl.WSDLOperation;
import org.yads.java.dispatch.EprInfoHandler.EprInfoProvider;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.LocalService;
import org.yads.java.service.Service;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.QName;
import org.yads.java.util.Clazz;
import org.yads.java.util.Log;

public abstract class EventingFactory {

	private static EventingFactory	instance				= null;

	private static boolean			getInstanceFirstCall	= true;

	/**
	 * Returns an implementation of the eventing factory if available, which
	 * allows to subscribe for other events and to offer own events. If no
	 * implementation is loaded yet attemping to load the <code>DefaultEventingFactory.</code>
	 * <p>
	 * It is necessary to load the corresponding module for attachment support.
	 * </p>
	 * 
	 * @return an implementation of the eventing factory.
	 */
	public static synchronized EventingFactory getInstance() {
		if (getInstanceFirstCall) {
			getInstanceFirstCall = false;
			try {
				// default = "org.yads.java.eventing.DefaultEventingFactory"
				Class clazz = Clazz.forName(FrameworkConstants.DEFAULT_EVENTING_FACTORY_PATH);
				instance = ((EventingFactory) clazz.newInstance());
			} catch (Exception e) {
				if (Log.isDebug()) {
					Log.debug("Unable to create DefaultEventingFactory: " + e.getMessage());
				}
			}
		}
		return instance;
	}

	public abstract EventSource createDefaultEventSource(String name, QName portType);

	public abstract EventSource createDefaultEventSource(WSDLOperation operation);

	public abstract EventSource createEventSourceStub(WSDLOperation operation);

	public abstract ClientSubscription createClientSubscription(EventSink sink, String clientSubscriptionId, EndpointReference serviceSubscriptionId, String comManId, long duration, Service service, CredentialInfo credentialInfo);

	public abstract ClientSubscription createClientSubscription(EventSink sink, String clientSubscriptionId, EprInfoProvider eprInfoProvider, Service service, CredentialInfo credentialInfo);

	public abstract EventSink createEventSink(EventListener eventListener, int configurationId);

	public abstract EventSink createEventSink(EventListener eventListener, Collection bindings);

	public abstract SubscriptionManager getSubscriptionManager(LocalService service, OutgoingDiscoveryInfosProvider provider);

}
