/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.eventing;

import java.util.Collection;
import org.yads.java.description.wsdl.WSDLOperation;
import org.yads.java.dispatch.EprInfoHandler.EprInfoProvider;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.DefaultClientSubscription;
import org.yads.java.service.DefaultEventSource;
import org.yads.java.service.DefaultSubscriptionManager;
import org.yads.java.service.EventSourceStub;
import org.yads.java.service.LocalService;
import org.yads.java.service.Service;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.QName;

public class DefaultEventingFactory extends EventingFactory {

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.EventingFactory#createDefaultEventSource(java.
	 * lang.String, org.yads.java.types.QName)
	 */
	@Override
	public EventSource createDefaultEventSource(String name, QName portType) {
		return new DefaultEventSource(name, portType);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.EventingFactory#createDefaultEventSource(org.ws4d
	 * .java.wsdl.WSDLOperation)
	 */
	@Override
	public EventSource createDefaultEventSource(WSDLOperation operation) {
		return new DefaultEventSource(operation);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.EventingFactory#createEventSourceStub(org.ws4d
	 * .java.wsdl.WSDLOperation)
	 */
	@Override
	public EventSource createEventSourceStub(WSDLOperation operation) {
		return new EventSourceStub(operation);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.EventingFactory#createClientSubscription(org.ws4d
	 * .java.eventing.EventSink, java.lang.String,
	 * org.yads.java.types.EndpointReference, java.lang.String, long,
	 * org.yads.java.service.Service, org.yads.java.security.CredentialInfo)
	 */
	@Override
	public ClientSubscription createClientSubscription(EventSink sink, String clientSubscriptionId, EndpointReference serviceSubscriptionId, String comManId, long duration, Service service, CredentialInfo credentialInfo) {
		return new DefaultClientSubscription(sink, clientSubscriptionId, serviceSubscriptionId, comManId, duration, service, credentialInfo);
	}

	@Override
	public ClientSubscription createClientSubscription(EventSink sink, String clientSubscriptionId, EprInfoProvider eprInfoProvider, Service service, CredentialInfo credentialInfo) {
		return new DefaultClientSubscription(sink, clientSubscriptionId, eprInfoProvider, service, credentialInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.EventingFactory#createEventSink(org.yads.java.
	 * eventing.EventListener, int)
	 */
	@Override
	public EventSink createEventSink(EventListener eventListener, int configurationId) {
		return new DefaultEventSink(eventListener, configurationId);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.EventingFactory#createEventSink(org.yads.java.
	 * eventing.EventListener, org.yads.java.structures.DataStructure)
	 */
	@Override
	public EventSink createEventSink(EventListener eventListener, Collection bindings) {
		return new DefaultEventSink(eventListener, bindings);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.EventingFactory#getSubscriptionManager(org.ws4d
	 * .java.service.LocalService)
	 */
	@Override
	public SubscriptionManager getSubscriptionManager(LocalService service, OutgoingDiscoveryInfosProvider provider) {
		return new DefaultSubscriptionManager(service, provider);
	}
}
