/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication;

import org.yads.java.communication.protocol.http.HTTPBindingFactory;
import org.yads.java.communication.structures.CommunicationAutoBinding;
import org.yads.java.communication.structures.DiscoveryAutoBinding;
import org.yads.java.communication.structures.IPCommunicationAutoBinding;
import org.yads.java.communication.structures.IPDiscoveryAutoBinding;
import org.yads.java.security.CredentialInfo;
import org.yads.java.util.Log;

public class IPAutoBindingFactory implements AutoBindingFactory {

	public static final boolean				STANDARD_AUTOBINDING			= false;

	public static final boolean				DISCOVERY_UNICAST_AUTOBINDING	= true;

	private String							comManId;

	private IPCommunicationBindingFactory	communicationBindingFactory;

	public IPAutoBindingFactory(String comManId) {
		this(comManId, null);
	}

	public IPAutoBindingFactory(String comManId, IPCommunicationBindingFactory communicationBindingFactory) {
		if (comManId == null || comManId.equals("")) {
			throw new IllegalArgumentException("CommunicationManagerId not set");
		}
		this.comManId = comManId;
		this.communicationBindingFactory = (communicationBindingFactory == null) ? HTTPBindingFactory.getInstance() : communicationBindingFactory;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createDiscoveryMulticastAutobinding()
	 */
	@Override
	public DiscoveryAutoBinding createDiscoveryMulticastAutoBinding() {
		return new IPDiscoveryAutoBinding(comManId);

	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createDiscoveryMulticastAutobinding(java.lang.String[],
	 * java.lang.String[], boolean)
	 */
	@Override
	public DiscoveryAutoBinding createDiscoveryMulticastAutoBinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible) {
		return new IPDiscoveryAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createSecureDiscoveryMulticastAutobinding
	 * (org.yads.java.security.CredentialInfo)
	 */
	@Override
	public DiscoveryAutoBinding createSecureDiscoveryMulticastAutoBinding(CredentialInfo credentialInfo) {
		IPDiscoveryAutoBinding dab = new IPDiscoveryAutoBinding(comManId);
		dab.setCredentialInfo(credentialInfo);
		return dab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createSecureDiscoveryMulticastAutobinding(java.lang.String[],
	 * java.lang.String[], boolean, org.yads.java.security.CredentialInfo)
	 */
	@Override
	public DiscoveryAutoBinding createSecureDiscoveryMulticastAutoBinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, CredentialInfo credentialInfo) {
		IPDiscoveryAutoBinding dab = new IPDiscoveryAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible);
		dab.setCredentialInfo(credentialInfo);
		return dab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createDiscoveryMulticastAutobindingForCommunicationAutoBinding
	 * (org.yads.java.communication.structures.CommunicationAutoBinding)
	 */
	@Override
	public DiscoveryAutoBinding createDiscoveryMulticastAutoBindingForCommunicationAutoBinding(CommunicationAutoBinding cab) {
		IPCommunicationAutoBinding ipCab;
		try {
			ipCab = (IPCommunicationAutoBinding) cab;
		} catch (ClassCastException e) {
			if (Log.isWarn()) {
				Log.warn("Could't create DiscoveryAutoBinding from CommunicationAutoBinding, because CommunicationAutoBinding technology is wrong");
			}
			return null;
		}
		IPDiscoveryAutoBinding ipDab = new IPDiscoveryAutoBinding(ipCab.getCommunicationManagerId(), ipCab.getInterfaceNames(), ipCab.getAddressFamilies(), ipCab.isSuppressLoopbackIfPossible());
		ipDab.setCredentialInfo(ipCab.getCredentialInfo());
		return ipDab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createDiscoveryUnicastAutobinding()
	 */
	@Override
	public CommunicationAutoBinding createDiscoveryUnicastAutoBinding() {
		return new IPCommunicationAutoBinding(comManId, true, null, 0, DISCOVERY_UNICAST_AUTOBINDING, communicationBindingFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createDiscoveryUnicastAutobinding(java.lang.String[], java.lang.String[],
	 * boolean, boolean, int)
	 */
	@Override
	public CommunicationAutoBinding createDiscoveryUnicastAutoBinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, boolean suppressMulticastDisabledInterfaces, int port) {
		return new IPCommunicationAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible, suppressMulticastDisabledInterfaces, null, port, DISCOVERY_UNICAST_AUTOBINDING, communicationBindingFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createSecureDiscoveryUnicastAutobinding
	 * (org.yads.java.security.CredentialInfo)
	 */
	@Override
	public CommunicationAutoBinding createSecureDiscoveryUnicastAutoBinding(CredentialInfo credentialInfo) {
		IPCommunicationAutoBinding cab = new IPCommunicationAutoBinding(comManId, true, null, 0, DISCOVERY_UNICAST_AUTOBINDING, communicationBindingFactory);
		cab.setCredentialInfo(credentialInfo);
		return cab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createSecureDiscoveryUnicastAutobinding(java.lang.String[],
	 * java.lang.String[], boolean, boolean, int,
	 * org.yads.java.security.CredentialInfo)
	 */
	@Override
	public CommunicationAutoBinding createSecureDiscoveryUnicastAutoBinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, boolean suppressMulticastDisabledInterfaces, int port, CredentialInfo credentialInfo) {
		IPCommunicationAutoBinding cab = new IPCommunicationAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible, suppressMulticastDisabledInterfaces, null, port, DISCOVERY_UNICAST_AUTOBINDING, communicationBindingFactory);
		cab.setCredentialInfo(credentialInfo);
		return cab;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.AutoBindingFactory#createCommunicationAutobinding
	 * (java.lang.String, int)
	 */
	@Override
	public CommunicationAutoBinding createCommunicationAutoBinding(boolean suppressLoopbackIfPossible, String path, int port) {
		return new IPCommunicationAutoBinding(comManId, suppressLoopbackIfPossible, path, port, STANDARD_AUTOBINDING, communicationBindingFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.AutoBindingFactory#createCommunicationAutobinding
	 * (java.lang.String[], java.lang.String[], boolean, boolean,
	 * java.lang.String, int)
	 */
	@Override
	public CommunicationAutoBinding createCommunicationAutoBinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, boolean suppressMulticastDisabledInterfaces, String path, int port) {
		return new IPCommunicationAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible, suppressMulticastDisabledInterfaces, path, port, STANDARD_AUTOBINDING, communicationBindingFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createCommunicationSecureAutobinding(java.lang.String, int,
	 * org.yads.java.security.CredentialInfo)
	 */
	@Override
	public CommunicationAutoBinding createCommunicationSecureAutoBinding(boolean suppressLoopbackIfPossible, String path, int port, CredentialInfo credentialInfo) {
		IPCommunicationAutoBinding cab = new IPCommunicationAutoBinding(comManId, suppressLoopbackIfPossible, path, port, STANDARD_AUTOBINDING, communicationBindingFactory);
		cab.setCredentialInfo(credentialInfo);
		return cab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createCommunicationSecureAutobinding(java.lang.String[],
	 * java.lang.String[], boolean, boolean, java.lang.String, int,
	 * org.yads.java.security.CredentialInfo)
	 */
	@Override
	public CommunicationAutoBinding createCommunicationSecureAutoBinding(String[] interfacesNames, String[] addressFamilies, boolean suppressLoopbackIfPossible, boolean suppressMulticastDisabledInterfaces, String path, int port, CredentialInfo credentialInfo) {
		IPCommunicationAutoBinding cab = new IPCommunicationAutoBinding(comManId, interfacesNames, addressFamilies, suppressLoopbackIfPossible, suppressMulticastDisabledInterfaces, path, port, STANDARD_AUTOBINDING, communicationBindingFactory);
		cab.setCredentialInfo(credentialInfo);
		return cab;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.AutoBindingFactory#
	 * createCommunicationAutoBindingForDiscoveryAutoBinding
	 * (org.yads.java.communication.structures.DiscoveryAutoBinding)
	 */
	@Override
	public CommunicationAutoBinding createCommunicationAutoBindingForDiscoveryAutoBinding(DiscoveryAutoBinding dab) {
		IPDiscoveryAutoBinding ipDab;
		try {
			ipDab = (IPDiscoveryAutoBinding) dab;
		} catch (ClassCastException e) {
			if (Log.isWarn()) {
				Log.warn("Could't create CommunicationAutoBinding from DiscoveryAutoBinding, because DiscoveryAutoBinding technology is wrong");
			}
			return null;
		}

		IPCommunicationAutoBinding ipCab = new IPCommunicationAutoBinding(ipDab.getCommunicationManagerId(), ipDab.getInterfaceNames(), ipDab.getAddressFamilies(), ipDab.isSuppressLoopbackIfPossible(), ipDab.isSuppressMulticastDisabledInterfaces(), null, 0, STANDARD_AUTOBINDING, communicationBindingFactory);
		ipCab.setCredentialInfo(ipDab.getCredentialInfo());
		return ipCab;
	}
}
