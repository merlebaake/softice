/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.monitor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.yads.java.communication.RequestHeader;
import org.yads.java.communication.Resource;
import org.yads.java.security.CredentialInfo;
import java.util.HashMap;
import org.yads.java.types.ContentType;
import org.yads.java.types.URI;

public class MonitorDummyResource implements Resource {

	String	shortDescription;

	public MonitorDummyResource(String shortDesciption) {
		this.shortDescription = shortDesciption;
	}

	@Override
	public ContentType getContentType() {
		return null;
	}

	@Override
	public void serialize(URI request, RequestHeader requestHeader, InputStream requestBody, OutputStream out, CredentialInfo credentialInfo, String comManId) throws IOException {}

	@Override
	public HashMap getHeaderFields() {
		return null;
	}

	@Override
	public long size() {
		return 0;
	}

	@Override
	public long getLastModifiedDate() {
		return 0;
	}

	@Override
	public String shortDescription() {
		return shortDescription;
	}

	@Override
	public Object getKey() {
		return null;
	}
}
