/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.monitor;

import org.yads.java.YADSFramework;
import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.receiver.MessageReceiver;
import org.yads.java.message.FaultMessage;
import org.yads.java.message.InvokeMessage;
import org.yads.java.message.discovery.ByeMessage;
import org.yads.java.message.discovery.HelloMessage;
import org.yads.java.message.discovery.ProbeMatchesMessage;
import org.yads.java.message.discovery.ProbeMessage;
import org.yads.java.message.discovery.ResolveMatchesMessage;
import org.yads.java.message.discovery.ResolveMessage;
import org.yads.java.message.eventing.GetStatusMessage;
import org.yads.java.message.eventing.GetStatusResponseMessage;
import org.yads.java.message.eventing.RenewMessage;
import org.yads.java.message.eventing.RenewResponseMessage;
import org.yads.java.message.eventing.SubscribeMessage;
import org.yads.java.message.eventing.SubscribeResponseMessage;
import org.yads.java.message.eventing.SubscriptionEndMessage;
import org.yads.java.message.eventing.UnsubscribeMessage;
import org.yads.java.message.eventing.UnsubscribeResponseMessage;
import org.yads.java.message.metadata.GetMessage;
import org.yads.java.message.metadata.GetMetadataMessage;
import org.yads.java.message.metadata.GetMetadataResponseMessage;
import org.yads.java.message.metadata.GetResponseMessage;
import org.yads.java.service.OperationDescription;

public class MonitoredMessageReceiver implements MessageReceiver {

	private MessageReceiver			receiver	= null;

	private MonitoringContext		context		= null;

	private MonitorStreamFactory	monFac		= null;

	public MonitoredMessageReceiver(MessageReceiver receiver, MonitoringContext context) {
		this.receiver = receiver;
		this.context = context;
		this.monFac = YADSFramework.getMonitorStreamFactory();
	}

	@Override
	public void receive(HelloMessage hello, ConnectionInfo connectionInfo) {
		receiver.receive(hello, connectionInfo);
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, hello);
		}
	}

	@Override
	public void receive(ByeMessage bye, ConnectionInfo connectionInfo) {
		receiver.receive(bye, connectionInfo);
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, bye);
		}
	}

	@Override
	public void receive(ProbeMessage probe, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, probe);
		}
		receiver.receive(probe, connectionInfo);
	}

	@Override
	public void receive(ProbeMatchesMessage probeMatches, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, probeMatches);
		}
		receiver.receive(probeMatches, connectionInfo);
	}

	@Override
	public void receive(ResolveMessage resolve, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, resolve);
		}
		receiver.receive(resolve, connectionInfo);
	}

	@Override
	public void receive(ResolveMatchesMessage resolveMatches, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, resolveMatches);
		}
		receiver.receive(resolveMatches, connectionInfo);
	}

	@Override
	public void receive(GetMessage get, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, get);
		}
		receiver.receive(get, connectionInfo);
	}

	@Override
	public void receive(GetResponseMessage getResponse, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, getResponse);
		}
		receiver.receive(getResponse, connectionInfo);
	}

	@Override
	public void receive(GetMetadataMessage getMetadata, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, getMetadata);
		}
		receiver.receive(getMetadata, connectionInfo);
	}

	@Override
	public void receive(GetMetadataResponseMessage getMetadataResponse, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, getMetadataResponse);
		}
		receiver.receive(getMetadataResponse, connectionInfo);
	}

	@Override
	public void receive(SubscribeMessage subscribe, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, subscribe);
		}
		receiver.receive(subscribe, connectionInfo);
	}

	@Override
	public void receive(SubscribeResponseMessage subscribeResponse, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, subscribeResponse);
		}
		receiver.receive(subscribeResponse, connectionInfo);
	}

	@Override
	public void receive(GetStatusMessage getStatus, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, getStatus);
		}
		receiver.receive(getStatus, connectionInfo);
	}

	@Override
	public void receive(GetStatusResponseMessage getStatusResponse, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, getStatusResponse);
		}
		receiver.receive(getStatusResponse, connectionInfo);
	}

	@Override
	public void receive(RenewMessage renew, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, renew);
		}
		receiver.receive(renew, connectionInfo);
	}

	@Override
	public void receive(RenewResponseMessage renewResponse, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, renewResponse);
		}
		receiver.receive(renewResponse, connectionInfo);
	}

	@Override
	public void receive(UnsubscribeMessage unsubscribe, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, unsubscribe);
		}
		receiver.receive(unsubscribe, connectionInfo);
	}

	@Override
	public void receive(UnsubscribeResponseMessage unsubscribeResponse, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, unsubscribeResponse);
		}
		receiver.receive(unsubscribeResponse, connectionInfo);
	}

	@Override
	public void receive(SubscriptionEndMessage subscriptionEnd, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, subscriptionEnd);
		}
		receiver.receive(subscriptionEnd, connectionInfo);
	}

	@Override
	public void receive(InvokeMessage invoke, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, invoke);
		}
		receiver.receive(invoke, connectionInfo);
	}

	@Override
	public void receive(FaultMessage fault, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.received(connectionInfo.getConnectionId(), context, fault);
		}
		receiver.receive(fault, connectionInfo);
	}

	@Override
	public void receiveFailed(Exception e, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.receivedFault(connectionInfo.getConnectionId(), context, e);
		}
		receiver.receiveFailed(e, connectionInfo);

	}

	@Override
	public void sendFailed(Exception e, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.receivedFault(connectionInfo.getConnectionId(), context, e);
		}
		receiver.sendFailed(e, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.protocol.soap.generator.MessageReceiver#
	 * getOperation(java.lang.String)
	 */
	@Override
	public OperationDescription getOperation(String action) {
		return receiver.getOperation(action);
	}

	@Override
	public OperationDescription getEventSource(String action) {
		return receiver.getEventSource(action);
	}

	@Override
	public void receiveNoContent(String reason, ConnectionInfo connectionInfo) {
		if (monFac != null) {
			monFac.receiveNoContent(connectionInfo.getConnectionId(), context, reason);
		}
		receiver.receiveNoContent(reason, connectionInfo);
	}

	public MessageReceiver getReceiver() {
		return receiver;
	}

	@Override
	public int getRequestMessageType() {
		return receiver.getRequestMessageType();
	}
}
