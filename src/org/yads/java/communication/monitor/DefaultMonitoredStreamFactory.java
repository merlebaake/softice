/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.monitor;

import java.io.IOException;
import java.io.OutputStream;

import org.yads.java.communication.Resource;
import org.yads.java.constants.MessageConstants;
import org.yads.java.message.SOAPHeader;
import org.yads.java.types.AttributedURI;
import org.yads.java.types.URI;

/**
 * Simple extension of the <code>MonitorStreamFactory</code> which allows to
 * write any incoming or outgoing <code>Message</code> to the default error
 * output stream.
 */
public class DefaultMonitoredStreamFactory extends MonitorStreamFactory {

	/**
	 * DefaultMonitoredStreamFactory dmsf = new DefaultMonitoredStreamFactory();
	 * JMEDSFramework.setMonitorStreamFactory(dmsf);
	 */

	@Override
	public StreamMonitor createInputMonitor() {
		return new DefaultStreamMonitor();
	}

	@Override
	public StreamMonitor createOutputMonitor() {
		return new DefaultStreamMonitor();
	}

	private class DefaultStreamMonitor implements StreamMonitor {

		private MonitoringContext	context		= null;

		private OutputStream		FORWARDER	= new OutputStream() {

													@Override
													public void write(int b) throws IOException {
														System.err.write(b);
													}
												};

		@Override
		public OutputStream getOutputStream() {
			/*
			 * !!! don't return System.out or System.err directly, as they would
			 * get closed by the monitors!!!
			 */
			return FORWARDER;
		}

		@Override
		public void assign(MonitoringContext context, AttributedURI optionalMessageId) {
			System.out.println("<DefaultMonitorStream> - Assign: " + context.getMessage().toString());
		}

		@Override
		public void fault(MonitoringContext context, Exception e) {
			System.out.println("<DefaultMonitorStream> - Fault:  " + context.getMessage().toString());
		}

		@Override
		public void setMonitoringContext(MonitoringContext context) {
			this.context = context;
		}

		@Override
		public void resetMonitoringContext() {
			setMonitoringContext(null);
		}

		@Override
		public MonitoringContext getMonitoringContext() {
			return context;
		}

		@Override
		public void discard(MonitoringContext context, int reason) {
			String info = "Message not available";
			SOAPHeader header = context.getHeader();
			if (header != null) {
				info = MessageConstants.getMessageNameForType(header.getMessageType());
			}
			System.out.println("<DefaultMonitorStream> - Discard: " + info + ", because: " + context.getDiscardReasonString(reason));
		}

		@Override
		public void resource(MonitoringContext context, Resource resource) {
			System.out.println("<DefaultMonitorStream> - Resource:  " + resource.toString());
		}

		@Override
		public void request(MonitoringContext context, URI location) {
			System.out.println("<DefaultMonitorStream> - Request: " + location);
		}

		@Override
		public void noContent(MonitoringContext context, String reason) {
			System.out.println("<DefaultMonitorStream> - NoContent:  " + reason);
		}
	}
}
