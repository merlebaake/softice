/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication;

import org.yads.java.constants.DPWS2006.DPWSConstants2006;
import org.yads.java.constants.DPWS2009.DPWSConstants2009;
import org.yads.java.constants.DPWS2011.DPWSConstants2011;

public class DPWSProtocolVersion implements ProtocolVersion {

	public static final DPWSProtocolVersion	DPWS_VERSION_2006			= new DPWSProtocolVersion(DPWSConstants2006.DPWS_VERSION_INT);

	public static final DPWSProtocolVersion	DPWS_VERSION_2009			= new DPWSProtocolVersion(DPWSConstants2009.DPWS_VERSION_INT);

	public static final DPWSProtocolVersion	DPWS_VERSION_2011			= new DPWSProtocolVersion(DPWSConstants2011.DPWS_VERSION_INT);

	public static final DPWSProtocolVersion	DPWS_VERSION_NOT_SET		= new DPWSProtocolVersion();

	public static final int					DPWS_VERSION_NOT_SET_ID		= -1;

	public static final String				DPWS_VERSION_NOT_SET_NAME	= "DPWS version unknown";

	private final int						dpwsVersionId;

	private String							displayName					= null;

	private DPWSProtocolVersion() {
		dpwsVersionId = DPWS_VERSION_NOT_SET_ID;
		displayName = DPWS_VERSION_NOT_SET_NAME;
	}

	private DPWSProtocolVersion(int dpwsVersion) {
		super();
		this.dpwsVersionId = dpwsVersion;
	}

	@Override
	public String getCommunicationManagerId() {
		return DPWSCommunicationManager.COMMUNICATION_MANAGER_ID;
	}

	@Override
	public int getVersionNumber() {
		return dpwsVersionId;
	}

	@Override
	public String getDisplayName() {
		if (displayName == null) {
			displayName = DPWSCommunicationManager.getHelper(this).getDisplayName();
		}
		return displayName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dpwsVersionId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		DPWSProtocolVersion other = (DPWSProtocolVersion) obj;
		if (dpwsVersionId != other.dpwsVersionId) return false;
		return true;
	}

	@Override
	public String toString() {
		return "DPWSProtocolVersion [dpwsVersion = " + getDisplayName() + "]";
	}

}
