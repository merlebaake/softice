/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http.header;

import java.util.HashMap;

import org.yads.java.communication.RequestHeader;
import org.yads.java.communication.protocol.http.HTTPParameter;
import org.yads.java.communication.protocol.mime.MIMEUtil;
import org.yads.java.configuration.HTTPProperties;
import org.yads.java.constants.HTTPConstants;
import org.yads.java.constants.MIMEConstants;
import org.yads.java.types.ContentType;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.StringUtil;
import org.yads.java.util.Toolkit;

/**
 * This class represents the HTTP request header.
 */
public class HTTPRequestHeader extends HTTPHeader implements RequestHeader {

	private String			method		= null;

	private String			request		= null;

	private String			version		= null;

	private HTTPParameter	parameter	= null;

	/**
	 * HTTP request header.
	 * 
	 * @param method HTTP method.
	 * @param request Request URI.
	 * @param version HTTP version.
	 * @param headerfields <code>Map</code> containing the HTTP header fields.
	 * @param parameter the HTTP parameter found in the request.
	 */
	public HTTPRequestHeader(String method, String request, boolean secure, String version, HashMap headerfields, HTTPParameter parameter) {
		super(secure);
		this.method = method;
		if (request == null || request.length() == 0) {
			this.request = "/";
		} else {
			this.request = request;
		}
		this.version = version;
		this.parameter = parameter;
		if (headerfields != null) {
			this.headerfields = headerfields;
		}

		if (StringUtil.isEmpty(getHeaderFieldValue(HTTPConstants.HTTP_HEADER_USER_AGENT))) {
			addHeaderFieldValue(HTTPConstants.HTTP_HEADER_USER_AGENT, HTTPProperties.getInstance().getDefaultUserAgent());
		}
	}

	/**
	 * HTTP request header.
	 * 
	 * @param method HTTP method.
	 * @param request Request URI.
	 * @param version HTTP version.
	 * @param headerfields <code>Map</code> containing the HTTP header fields.
	 */
	public HTTPRequestHeader(String method, String request, boolean secure, String version, HashMap headerfields) {
		this(method, request, secure, version, headerfields, null);
	}

	/**
	 * HTTP request header.
	 * 
	 * @param method HTTP method.
	 * @param request Request URI.
	 * @param secure
	 * @param version HTTP version.
	 */
	public HTTPRequestHeader(String method, String request, boolean secure, String version) {
		this(method, request, secure, version, null);
	}

	public void setRequest(String request) {
		this.request = request;
	}

	/**
	 * Returns the HTTP request method.
	 * 
	 * @return the method.
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Returns the HTTP request URI.
	 * 
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * Returns the HTTP request version.
	 * 
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Returns the content type of the response.
	 * 
	 * @return the response's content type
	 */
	public ContentType getContentType() {
		String type = getHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE);
		if (type == null) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_OCTET_STREAM;
		}
		return MIMEUtil.createContentType(type);
	}

	/**
	 * Returns a <code>String</code> representation of the HTTP header
	 * containing all header fields.
	 * 
	 * @return a string representation of the HTTP header.
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sBuf = Toolkit.getInstance().createSimpleStringBuilder();
		sBuf.append((isSecure() ? HTTPConstants.HTTPS_SCHEMA.toUpperCase() : HTTPConstants.HTTP_SCHEMA.toUpperCase()));
		sBuf.append(" request [ method=");
		sBuf.append(method);
		sBuf.append(", version=");
		sBuf.append(version);
		sBuf.append(", URI=");
		sBuf.append(request);
		sBuf.append(" ]");
		return sBuf.toString();
	}

	/**
	 * Returns the parameter with given name for this HTTP request.
	 * 
	 * @param name the parameter name.
	 * @return the parameter value.
	 */
	public String getParameter(String name) {
		if (parameter == null) return null;
		return parameter.getURIParameter(name);
	}

}
