/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http.header;

import java.util.HashMap;

public class HTTPHeader {

	private boolean		secure			= false;

	protected HashMap<String, String>	headerfields	= new HashMap<>();

	public HTTPHeader(boolean secure) {
		this.secure = secure;
	}

	/**
	 * Returns the header value for the requested header field.
	 * 
	 * @param fieldname ,the field to get the value from.
	 * @return the value.
	 */
	public String getHeaderFieldValue(String fieldname) {
		return headerfields.get(fieldname);
	}

	/**
	 * Adds a header value to this header.
	 * 
	 * @param name the field to add.
	 * @param value the value for this field.
	 */
	public void addHeaderFieldValue(String name, String value) {
		headerfields.put(name, value);
	}

	/**
	 * Removes a header value from this header.
	 * 
	 * @param name the field to remove.
	 */
	public void removeHeaderFieldValue(String name) {
		headerfields.remove(name);
	}

	public boolean isSecure() {
		return secure;
	}

    public HashMap<String, String> getHeaderfields() {
        return headerfields;
    }

}
