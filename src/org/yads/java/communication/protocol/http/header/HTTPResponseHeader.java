/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http.header;


import org.yads.java.communication.ResponseHeader;
import org.yads.java.constants.HTTPConstants;
import java.util.HashMap;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

/**
 * This class represents the HTTP response header.
 */
public class HTTPResponseHeader extends HTTPHeader implements ResponseHeader {

	private String	version	= null;

	private int		status	= 0;

	private String	reason	= null;

	/**
	 * HTTP response Header
	 * 
	 * @param version HTTP Version
	 * @param status HTTP status code.
	 * @param reason reason phrase.
	 */
	public HTTPResponseHeader(String version, boolean secure, int status, String reason) {
		super(secure);
		this.version = version;
		this.status = status;
		this.reason = reason;
	}

	/**
	 * HTTP response Header
	 * 
	 * @param version HTTP Version
	 * @param status HTTP status code.
	 * @param reason reason phrase.
	 * @param headerfields the HTTP header fields.
	 */
	public HTTPResponseHeader(String version, boolean secure, int status, String reason, HashMap headerfields) {
		super(secure);
		this.version = version;
		this.status = status;
		this.reason = reason;
		this.headerfields = headerfields;
	}

	/**
	 * Returns the HTTP version for this response.
	 * 
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Returns the HTTP status code for this response.
	 * 
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Returns the HTTP reason phrase for this response.
	 * 
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	@Override
	public String toString() {
		SimpleStringBuilder sBuf = Toolkit.getInstance().createSimpleStringBuilder();
		sBuf.append((isSecure() ? HTTPConstants.HTTPS_SCHEMA.toUpperCase() : HTTPConstants.HTTP_SCHEMA).toUpperCase());
		sBuf.append(" response [ status=");
		sBuf.append(status);
		sBuf.append(", reason=");
		sBuf.append(reason);
		sBuf.append(" ]");
		return sBuf.toString();

	}
    
}
