/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import org.yads.java.YADSFramework;
import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.connection.ip.IPConnectionInfo;
import org.yads.java.communication.monitor.MonitoringContext;
import org.yads.java.communication.protocol.http.header.HTTPRequestHeader;
import org.yads.java.communication.protocol.http.header.HTTPResponseHeader;
import org.yads.java.communication.protocol.mime.MIMEUtil;
import org.yads.java.configuration.HTTPProperties;
import org.yads.java.constants.HTTPConstants;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.configuration.DPWSProperties;
import org.yads.java.types.ContentType;
import org.yads.java.util.Log;
import org.yads.java.util.Sync;

/**
 * Client for asynchronous HTTP communication.
 * 
 * @see HTTPRequest
 */
public class HTTPClient {

	public static int			MAX_CLIENT_CONNECTIONS	= HTTPProperties.getInstance().getMaxConnections();

	private final Map           handlers				= new ConcurrentHashMap();

    private static HTTPClient   INSTANCE                = null;
    
    private final Map<SSLContext, Executor> executors   = new ConcurrentHashMap<>();
    
    private Executor defaultExecutor                    = null;
    
	private HTTPClient() {   
	}

	/**
	 * Registers a handler with given Internet media type which will handle
	 * incoming HTTP responses.
	 * <p>
	 * This {@link HTTPResponseHandler} will ONLY BE USED if NO handler is returned by the {@link HTTPRequest#getResponseHandler(ContentType)} method.
	 * </p>
	 * 
	 * @param type the Internet media type.
	 * @param handler the handler which will handle the HTTP response.
	 * @see HTTPRequest
	 */
	public void register(ContentType type, HTTPResponseHandler handler) {
		handlers.put(type, handler);
	}

	/**
	 * Sends a HTTP request (with the path defined in the HTTP header inside the
	 * request) to the host, defined by the <code>create</code> method.
	 * <p>
	 * The request is not actually sent instantaneously to the host. It is put into a request queue and will be started as soon as possible. The speed depends on the thread scheduler and the Object.notifiy() method..
	 * </p>
	 * 
     * @param sslContext
     * @param dest
	 * @param request the HTTP request.
	 */
	public static void exchange(SSLContext sslContext, HTTPClientDestination dest, HTTPRequest request) {
		performRequest(sslContext, dest, request);
	}
    
	public static HttpClient createHttpClient(SSLContext clientSSLContext) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        final RegistryBuilder<ConnectionSocketFactory> register = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.INSTANCE);
        if (clientSSLContext != null) {
            register.register("https", new SSLConnectionSocketFactory(clientSSLContext, NoopHostnameVerifier.INSTANCE));            
        }
        final PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(register.build());
        HttpClientBuilder hcb = HttpClientBuilder.create()
		        .setConnectionManager(cm);
        if (clientSSLContext != null) {
            hcb.setSSLContext(clientSSLContext);
        }
        return hcb.build();
	}    

	private static void performRequest(SSLContext sslContext, HTTPClientDestination dest, HTTPRequest request) {
		if (YADSFramework.isKillRunning()) {
			Exception e = new RuntimeException("Add request is not possible because framework is shutting down.");
			request.responseReceiveFailed(e, null, null);
			return;
		}
        if (INSTANCE == null) {
            INSTANCE = new HTTPClient();
        }
        INSTANCE.sendRequest(sslContext, dest, request);
	}

	private void sendRequest(SSLContext sslContext, HTTPClientDestination dest, HTTPRequest request) {
        IPConnectionInfo requestConnectionInfo = new IPConnectionInfo(null, ConnectionInfo.DIRECTION_OUT, null, 0, true, dest.getXAddressInfo(), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
        HttpResponse httpResponse;
        
        /*
         * Try to send the request
         */

        try {       
            final String xAddrStr = dest.getXAddressInfo().getXAddressAsString();
            Request req = Request.Post(xAddrStr)
                    .connectTimeout((int) DPWSProperties.getInstance().getHTTPClientRequestTimeout())
                    .socketTimeout((int) DPWSProperties.getInstance().getHTTPClientRequestTimeout());
            HTTPRequestHeader header = request.getRequestHeader(requestConnectionInfo);
            
            for (Map.Entry<String, String> next : header.getHeaderfields().entrySet()) {
                req.addHeader(next.getKey(), next.getValue());
            }
                    
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                request.serializeRequestBody(out, requestConnectionInfo, null);
                out.flush();                    
                req.bodyByteArray(out.toByteArray());
            }

            Log.debug("<O> " + header + " to " + dest.getXAddressInfo().getXAddressAsString(), Log.DEBUG_LAYER_COMMUNICATION);
            
            httpResponse = getExecutor(sslContext).execute(req).returnResponse();
            
            StatusLine statusLine = httpResponse.getStatusLine();
            HttpEntity entity = httpResponse.getEntity();
            if (statusLine.getStatusCode() == HTTPConstants.HTTP_STATUS_ACCEPTED)
                return;
            if (statusLine.getStatusCode() >= 300) {
                throw new HttpResponseException(
                        statusLine.getStatusCode(),
                        statusLine.getReasonPhrase());
            }
            if (entity == null) {
                throw new ClientProtocolException("Response contains no content");
            }   

        } catch (Exception e) {
            Log.printStackTrace(e);
            Log.error("Cannot send HTTP request: " + e.getMessage() + ". Resetting TCP connection (" + requestConnectionInfo.toString() + ").");
            ExceptionNotification eNotification = new ExceptionNotification(requestConnectionInfo, request, e, false, null);
            eNotification.start();
            return;
        }

        /*
         * Try to read the response. This will block on the input
         * stream.
         */
        
        ConnectionInfo responseConnectionInfo = requestConnectionInfo.createSwappedConnectionInfo();
        InputStream in = null;
        
        try {
            Sync streamLock = new Sync();
            final StatusLine statusLine = httpResponse.getStatusLine();

            HTTPResponseHeader response = new HTTPResponseHeader("HTTP/1.1", false, statusLine.getStatusCode(), statusLine.getReasonPhrase());
            for (Header next : httpResponse.getAllHeaders()) {
                response.addHeaderFieldValue(next.getName().toLowerCase(), next.getValue());
            }
            
            Log.debug("<I> " + response + " from " + dest.getXAddressInfo().getXAddressAsString(), Log.DEBUG_LAYER_COMMUNICATION);
            ContentType contentType = MIMEUtil.createContentType(response.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE));

            /*
             * Check for response handler belonging to the
             * request.
             */
            HTTPResponseHandler handler = request.getResponseHandler(contentType);

            in = httpResponse.getEntity().getContent();

            /*
             * No handler found inside the request? Check the
             * internal table.
             */
            if (handler == null) {
                handler = (HTTPResponseHandler) handlers.get(contentType);
            }

            /*
             * HTTP response contains content, read it.
             */           
            StreamConsumerThread consumer = new StreamConsumerThread(handler, response, in, request, responseConnectionInfo, streamLock);

            /*
             * Wait until the current response is fully
             * read.
             */
            streamLock.reset();
            synchronized (streamLock) {
                YADSFramework.getThreadPool().execute(consumer);
                while (!streamLock.isNotified()) {
                    try {
                        streamLock.wait();
                    } catch (InterruptedException e) {}
                }
            }
            Exception e = streamLock.getException();
            if (e != null) {
                if (e instanceof IOException) {
                    throw (IOException) e;
                } else {
                    Log.error("A problem occured during stream read. " + e.getMessage());
                }
            }

        } catch (Exception e) {
            Log.printStackTrace(e);
            Log.error("Cannot handle HTTP response: " + e.getMessage());
            ExceptionNotification eNotification = new ExceptionNotification(responseConnectionInfo, request, e, true, null);
            eNotification.start();
        }
	 
	}

    private Executor getExecutor(SSLContext sslContext) {
        Executor ex = null;
        try {
            if (sslContext == null) {
                if (defaultExecutor != null)
                    ex = defaultExecutor;
                else
                    ex = defaultExecutor = Executor.newInstance(createHttpClient(null));
            } else {
                ex = executors.get(sslContext);
                if (ex != null)
                    return ex;
                ex = Executor.newInstance(createHttpClient(sslContext));
                executors.put(sslContext, ex);                
            }
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException x) {
            Logger.getLogger(HTTPClient.class.getName()).log(Level.SEVERE, null, x);
        }
        return ex;
    }
    
	/**
	 * This thread allows the handling of an incoming response independently
	 * from the thread handling the persistent HTTP connection.
	 */
	private class StreamConsumerThread implements Runnable {

		private HTTPResponseHandler		handler	= null;

		private HTTPResponseHeader		header	= null;

		private InputStream             body	= null;

		private HTTPRequest				request	= null;

		private final ConnectionInfo	ci;
        
        private final Sync sync;

		StreamConsumerThread(HTTPResponseHandler handler, HTTPResponseHeader header, InputStream body, HTTPRequest request, ConnectionInfo ci, Sync sync) {
			this.handler = handler;
			this.header = header;
			this.body = body;
			this.request = request;
			this.ci = ci;
            this.sync = sync;
		}

		@Override
		public void run() {
			try {
				if (request.needsBody() && body == null) {
					request.messageWithoutBodyReceived(header.getStatus(), ci, null);
				} else if (handler != null) {
					handler.handle(header, body, request, ci, null);
				} else {
					request.requestSendFailed(new IllegalArgumentException("No handler found for content-type: " + header.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE)), ci, null);
				}
                if (sync != null)
                    synchronized (sync) {
                        sync.notifyNow();
                    }
			} catch (Exception e) {
				Log.error("Error in handling request: " + e.getMessage());
                if (sync != null)
                    synchronized (sync) {
                        sync.notifyNow(e);
                    }                
			} finally {
				try {
					if (body != null) {
						body.close();
					}
				} catch (IOException e1) {
					Log.error("Could not consume omitted bytes from HTTP response. " + e1.getMessage());
				}
			}
		}
	}

	/**
	 * This thread allows exception handling without blocking the HTTP request
	 * queue.
	 */
	private class ExceptionNotification implements Runnable {

		private HTTPRequest			request			= null;

		private Exception			e				= null;

		private boolean				response		= false;

		private ConnectionInfo		connectionInfo	= null;

		private MonitoringContext	context			= null;

		ExceptionNotification(ConnectionInfo ci, HTTPRequest request, Exception e, boolean response, MonitoringContext context) {
			this.request = request;
			this.e = e;
			this.response = response;
			this.connectionInfo = ci;
			this.context = context;
		}

		@Override
		public void run() {
			if (request != null && e != null) {
				if (response) {
					request.responseReceiveFailed(e, connectionInfo, context);
				} else {
					request.requestSendFailed(e, connectionInfo, context);
				}
			}

		}

		public void start() {
			YADSFramework.getThreadPool().execute(this);
		}
	}

}
