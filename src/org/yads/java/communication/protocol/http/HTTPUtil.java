/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;

import org.yads.java.constants.HTTPConstants;
import org.yads.java.constants.Specialchars;
import org.yads.java.types.URI;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

/**
 * HTTP support class. All RFC methods are here!
 */
public class HTTPUtil {

	/**
	 * List with header fieldvalues who are case-insensitive.
	 */
	public static final HashSet	HTTP_HEADER_CASE_INSENSITIVE_VALUES	= new HashSet();

	/**
	 * Fill the list.
	 */
	static {
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_CONTENT_TYPE);
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_CONTENT_TRANSFER_ENCODING);
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_EXPECT);
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_TRANSFER_ENCODING);
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_CONTENT_ENCODING);
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_CONTENT_LENGTH);
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_CONNECTION);
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_CACHECONTROL);
		HTTP_HEADER_CASE_INSENSITIVE_VALUES.add(HTTPConstants.HTTP_HEADER_TE);
	}

	/**
	 * We are shy!
	 */
	private HTTPUtil() {

	}
    
	/**
	 * Reads a single protocol line from the input stream. HTTP defines the
	 * sequence CR LF as the end-of-line marker. (see RFC2616 2.2)
	 * 
	 * @param in input stream to read from.
	 * @return the protocol line.
	 */
	public static String readRequestLine(InputStream in) throws IOException {
		int i;
		SimpleStringBuilder buffer = Toolkit.getInstance().createSimpleStringBuilder();
		int j = 0;
		// read until new line
		while (((i = in.read()) != -1)) {
			if ((byte) i == Specialchars.CR) {
				j = 1;
				continue;
			}
			if ((byte) i == Specialchars.LF && j == 1) {
				j = 0;
				return buffer.toString();
			}
			buffer.append((char) i);
		}
		throw new IOException(HTTPRequestUtil.FAULT_UNEXPECTED_END);
	}    

	/**
	 * @param uri
	 * @return true if schema of uri is "https"
	 */
	public static boolean isHTTPS(URI uri) {
		return HTTPConstants.HTTPS_SCHEMA.equals(uri.getSchemaDecoded());
	}

}
