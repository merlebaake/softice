/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http.requests;

import java.io.IOException;
import java.io.OutputStream;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.monitor.MonitoringContext;
import org.yads.java.communication.protocol.http.HTTPRequest;
import org.yads.java.communication.protocol.http.HTTPResponseHandler;
import org.yads.java.communication.protocol.http.header.HTTPRequestHeader;
import org.yads.java.constants.HTTPConstants;
import org.yads.java.types.AttributedURI;
import org.yads.java.types.ContentType;
import org.yads.java.types.XAddressInfo;

public class DefaultHTTPGetRequest implements HTTPRequest {

	private HTTPRequestHeader	header				= null;

	private XAddressInfo		targetXAddressInfo	= null;

	public DefaultHTTPGetRequest(String request, boolean secure, XAddressInfo targetXAddressInfo) {
		header = new HTTPRequestHeader(HTTPConstants.HTTP_METHOD_GET, request, secure, HTTPConstants.HTTP_VERSION11);
		this.targetXAddressInfo = targetXAddressInfo;
	}

	@Override
	public long calculateSize(ConnectionInfo connectionInfo) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#serializeRequestBody
	 * (java.io.OutputStream, org.yads.java.communication.ConnectionInfo,
	 * org.yads.java.io.monitor.MonitoringContext)
	 */
	@Override
	public void serializeRequestBody(OutputStream out, ConnectionInfo connectionInfo, MonitoringContext context) throws IOException {}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.httpx.HTTPRequest#getRequestHeader()
	 */
	@Override
	public HTTPRequestHeader getRequestHeader(ConnectionInfo connectionInfo) {
		return header;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#requestSendFailed
	 * (java.lang.Exception, org.yads.java.communication.ConnectionInfo)
	 */
	@Override
	public void requestSendFailed(Exception e, ConnectionInfo connectionInfo, MonitoringContext context) {}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#responseReceiveFailed
	 * (java.lang.Exception, org.yads.java.communication.ConnectionInfo)
	 */
	@Override
	public void responseReceiveFailed(Exception e, ConnectionInfo connectionInfo, MonitoringContext context) {}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#getResponseHandler
	 * (org.yads.java.communication.ContentType)
	 */
	@Override
	public HTTPResponseHandler getResponseHandler(ContentType contentType) throws IOException {
		return null;
	}

	@Override
	public AttributedURI getOptionalMessageId() {
		return null;
	}

	@Override
	public XAddressInfo getTargetXAddressInfo() {
		return targetXAddressInfo;
	}

	@Override
	public void messageWithoutBodyReceived(int code, ConnectionInfo connectionInfo, MonitoringContext context) {}

	@Override
	public boolean needsBody() {
		return true;
	}

}
