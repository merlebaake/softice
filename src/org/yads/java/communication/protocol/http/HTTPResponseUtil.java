/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http;

import org.yads.java.communication.protocol.http.header.HTTPResponseHeader;
import org.yads.java.constants.HTTPConstants;

/**
 * Utility class for the easier creation of HTTP response messages.
 */
public class HTTPResponseUtil {

	private static String	HTTP_STATUS_CONTINUE_VALUE_STRING	= "" + HTTPConstants.HTTP_STATUS_CONTINUE;

	/*
	 * We are shy!
	 */
	private HTTPResponseUtil() {

	}

	/**
	 * Creates HTTP 204 "No Content" Header.
	 * 
	 * @return the HTTP response header.
	 */
	public static HTTPResponseHeader getResponseHeader(boolean secure) {
		return getResponseHeader(HTTPConstants.HTTP_STATUS_NO_CONTENT, secure);
	}

	/**
	 * Returns the default HTTP response header for the given status code.
	 * 
	 * @param status the status code.
	 * @return the HTTP response header.
	 */
	public static HTTPResponseHeader getResponseHeader(int status, boolean secure) {
		String version = HTTPConstants.HTTP_VERSION11;
		String phrase = getHTTPStatusString(status);
		return new HTTPResponseHeader(version, secure, status, phrase);
	}

	public static String getHTTPStatusString(int statusCode) {
		String phrase = null;
		switch (statusCode) {
			case HTTPConstants.HTTP_STATUS_CONTINUE:
				phrase = HTTPConstants.HTTP_STATUS_100;
				break;
			case HTTPConstants.HTTP_STATUS_OK:
				phrase = HTTPConstants.HTTP_STATUS_200;
				break;
			case HTTPConstants.HTTP_STATUS_ACCEPTED:
				phrase = HTTPConstants.HTTP_STATUS_202;
				break;
			case HTTPConstants.HTTP_STATUS_NO_CONTENT:
				phrase = HTTPConstants.HTTP_STATUS_204;
				break;
			case HTTPConstants.HTTP_STATUS_MULTIPLE_CHOICES:
				phrase = HTTPConstants.HTTP_STATUS_300;
				break;
			case HTTPConstants.HTTP_STATUS_MOVED_PERMANENTLY:
				phrase = HTTPConstants.HTTP_STATUS_301;
				break;
			case HTTPConstants.HTTP_STATUS_FOUND:
				phrase = HTTPConstants.HTTP_STATUS_302;
				break;
			case HTTPConstants.HTTP_STATUS_SEE_OTHER:
				phrase = HTTPConstants.HTTP_STATUS_303;
				break;
			case HTTPConstants.HTTP_STATUS_NOT_MODIFIED:
				phrase = HTTPConstants.HTTP_STATUS_304;
				break;
			case HTTPConstants.HTTP_STATUS_TEMPORARY_REDIRECT:
				phrase = HTTPConstants.HTTP_STATUS_307;
				break;
			case HTTPConstants.HTTP_STATUS_BAD_REQUEST:
				phrase = HTTPConstants.HTTP_STATUS_400;
				break;
			case HTTPConstants.HTTP_STATUS_UNAUTHORIZED:
				phrase = HTTPConstants.HTTP_STATUS_401;
				break;
			case HTTPConstants.HTTP_STATUS_FORBIDDEN:
				phrase = HTTPConstants.HTTP_STATUS_403;
				break;
			case HTTPConstants.HTTP_STATUS_NOT_FOUND:
				phrase = HTTPConstants.HTTP_STATUS_404;
				break;
			case HTTPConstants.HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE:
				phrase = HTTPConstants.HTTP_STATUS_415;
				break;
			case HTTPConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR:
				phrase = HTTPConstants.HTTP_STATUS_500;
				break;
			case HTTPConstants.HTTP_STATUS_NOT_IMPLEMENTED:
				phrase = HTTPConstants.HTTP_STATUS_501;
				break;
			case HTTPConstants.HTTP_STATUS_HTTP_VERSION_NOT_SUPPORTED:
				phrase = HTTPConstants.HTTP_STATUS_505;
				break;
		}

		return phrase;
	}

}
