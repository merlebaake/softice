/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import org.apache.http.ConnectionClosedException;
import org.apache.http.ExceptionLogger;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.nio.bootstrap.HttpServer;
import org.apache.http.impl.nio.bootstrap.ServerBootstrap;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.nio.entity.NByteArrayEntity;
import org.apache.http.nio.protocol.BasicAsyncRequestConsumer;
import org.apache.http.nio.protocol.BasicAsyncResponseProducer;
import org.apache.http.nio.protocol.HttpAsyncExchange;
import org.apache.http.nio.protocol.HttpAsyncRequestConsumer;
import org.apache.http.nio.protocol.HttpAsyncRequestHandler;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.ornet.softice.SoftICE;
import org.yads.java.YADSFramework;
import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.connection.ip.IPConnectionInfo;
import org.yads.java.communication.monitor.MonitoredInputStream;
import org.yads.java.communication.monitor.MonitoredOutputStream;
import org.yads.java.communication.protocol.http.HTTPBinding;
import org.yads.java.types.XAddressInfo;
import org.yads.java.util.Log;
import org.yads.java.communication.connection.tcp.TCPConnection;
import org.yads.java.communication.connection.tcp.TCPConnectionHandler;
import org.yads.java.configuration.DPWSProperties;
import org.yads.java.constants.HTTPConstants;

/**
 * HTTP listener which allows listening for incoming HTTP connections.
 * <p>
 * Each incoming connection will be handled in a separate thread.
 * </p>
 */
public class HTTPListener {

    public static final String DEFAULT_ENCODING = "UTF-8";  
    
	private HTTPBinding				httpBinding			= null;

	private volatile boolean		running				= false;

	private HttpServer				httpServer			= null;

	private TCPConnectionHandler	handler				= null;

	private String					comManId;
    
    static class YADSExceptionLogger implements ExceptionLogger {

        @Override
        public void log(final Exception ex) {
            if (ex instanceof SocketTimeoutException) {
                Log.debug("HTTP: read timed out.");
            }            
            else if (ex instanceof ConnectionClosedException) {
                Log.debug("HTTP: client closed connection.");
            }            
            else {
                Log.printStackTrace(ex);
                Log.error(ex.getMessage());
            }
        }

    }     

	/**
	 * Creates a TCP listener for the given address and port.
	 * <p>
	 * This will open a server socket for the given address and port and will pass a {@link TCPConnection} to the given {@link TCPConnectionHandler}
	 * </p>
	 * 
	 * @param address the address to which to listen.
	 * @param port the port.
	 * @param handler the handler which will handle the TCP connection.
	 * @throws IOException
	 */

	HTTPListener(HTTPBinding binding, TCPConnectionHandler handler) throws IOException {
		if (binding == null) {
			throw new IOException("Cannot create TCPListener without any binding!");
		}
		this.httpBinding = binding;

		if (handler == null) {
			throw new IOException("Cannot listen for incoming data. No handler set for connection handling.");
		}
		if (httpBinding.getHostIPAddress() == null) {
			throw new IOException("Cannot listen for incoming data. No IP address given.");
		}

		int port = httpBinding.getPort();
		if (port < 0 || port > 65535) {
			throw new IOException("Cannot listen for incoming data. Port number invalid.");
		}

		this.handler = handler;
		this.comManId = binding.getCommunicationManagerId();
        ServerBootstrap bs = ServerBootstrap.bootstrap();
        bs.setLocalAddress(InetAddress.getByName(binding.getHostIPAddress().getAddressWithoutNicId()))
                .setListenerPort(port)
                .setServerInfo("SoftICE")
                .setIOReactorConfig(IOReactorConfig.custom().setSoKeepAlive(DPWSProperties.getInstance().getHTTPServerKeepAlive()).setSoTimeout((int)DPWSProperties.getInstance().getHTTPServerRequestTimeout()).build())
                .registerHandler("*", getHttpServerHandler())
                .setExceptionLogger(new YADSExceptionLogger())
                .setConnectionReuseStrategy(DefaultConnectionReuseStrategy.INSTANCE);
        SSLContext sc = SoftICE.getInstance().getServerSSLContext();
        if (sc != null) {
            bs.setSslContext(sc);
        }
        this.httpServer = bs.create();
        binding.setPort(port);
	}

	/**
	 * Indicates whether this listener is running or not.
	 * 
	 * @return <code>true</code> if the listener is running and will handle
	 *         incoming TCP connections, <code>false</code> otherwise.
	 */
	public synchronized boolean isRunning() {
		return running;
	}

	/**
	 * Starts the TCP listener.
	 * 
	 * @return <code>true</code> if the listener is started or already running, <code>false</code> otherwise.
	 */
	public synchronized boolean start() {
		if (running) {
			return true;
		}

		try {
			httpServer.start();	
		}
		catch (Exception e) {
			Log.error(e.getMessage());
		}
		if (Log.isDebug()) {
			Log.debug("HTTP listener up for " + httpBinding.getHostIPAddress() + " and port " + httpBinding.getPort() + ".", Log.DEBUG_LAYER_COMMUNICATION);
		}		
        running = true;
		return true;
	}
    
    private Runnable getRequestHandlerRunnable(final HttpContext context, final HttpAsyncExchange httpExchange, final HttpRequest request) {
        return new Runnable() {
                       
            @Override
            public void run() {
                InputStream in = null;
                OutputStream out = new ByteArrayOutputStream();
                if (request instanceof HttpEntityEnclosingRequest) {
                    final HttpEntity entity = ((HttpEntityEnclosingRequest) request).getEntity();
                    if (entity.isStreaming()) {
                        try {                       
                            in = entity.getContent();
                        } catch (IOException | UnsupportedOperationException ex) {
                            Logger.getLogger(HTTPListener.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
				
				IPConnectionInfo connectionInfo = new IPConnectionInfo(null, ConnectionInfo.DIRECTION_IN, httpBinding.getHostIPAddress(), httpBinding.getPort(), true, new XAddressInfo(), comManId);

				connectionInfo.setLocalCredentialInfo(httpBinding.getCredentialInfo());		
				//connectionInfo.setRemoteCredentialInfo(socket.getRemoteCredentialInfo());

				if (YADSFramework.getMonitorStreamFactory() != null) {
					in = new MonitoredInputStream(in, connectionInfo.getConnectionId());
					out = new MonitoredOutputStream(out, connectionInfo.getConnectionId());
				}
				
				TCPConnection connection = new TCPConnection(in, out, connectionInfo, null);
				connection.setHttpExchange(httpExchange);
                try {
                    handler.handle(connection);
                } catch (IOException ex) {
                    Log.printStackTrace(ex);
                    Log.error(ex.getMessage());
                }
				if (Log.isDebug()) {
					Log.debug("<I> Incoming HTTP request (" + connection.getIdentifier() + ") handling done.", Log.DEBUG_LAYER_COMMUNICATION);
				}			
               
                HttpResponse response = httpExchange.getResponse();
                if (response.getStatusLine().getStatusCode() != HTTPConstants.HTTP_STATUS_ACCEPTED) {
                    NByteArrayEntity bae = new NByteArrayEntity(((ByteArrayOutputStream)out).toByteArray());
                    bae.setChunked(DPWSProperties.getInstance().getHTTPResponseChunkedMode() == 1);
                    response.setEntity(bae);                    
                } else {
                    for (Header next : response.getAllHeaders())
                        response.removeHeader(next);
                }
                httpExchange.submitResponse(new BasicAsyncResponseProducer(response));
            }
        };
    }
    

	private HttpAsyncRequestHandler<HttpRequest> getHttpServerHandler() {
		return new HttpAsyncRequestHandler<HttpRequest>() {

            @Override
            public HttpAsyncRequestConsumer<HttpRequest> processRequest(HttpRequest hr, HttpContext hc) throws HttpException, IOException {
                return (HttpAsyncRequestConsumer<HttpRequest>) new BasicAsyncRequestConsumer();
            }

            @Override
            public void handle(final HttpRequest request, final HttpAsyncExchange httpExchange, final HttpContext context) throws HttpException, IOException {
                YADSFramework.getThreadPool().execute(getRequestHandlerRunnable(context, httpExchange, request));
            }
            
		};
	}

	/**
	 * Stops the TCP listener.
	 * <p>
	 * Existing TCP connection will remain active! To stop the TCP server and close all established connections.
	 * </p>
	 */
	public synchronized void stop() throws IOException {
		if (!running) {
			return;
		}
		running = false;
		httpServer.shutdown(2, TimeUnit.SECONDS);
		if (Log.isDebug()) {
			Log.debug("HTTP listener shutdown for " + httpBinding.getHostIPAddress() + " and port " + httpBinding.getPort() + ".", Log.DEBUG_LAYER_COMMUNICATION);
		}
	}

	/**
	 * Stops the TCP listener and kills all established connection.
	 * <p>
	 * This will also close all established connections.
	 * </p>
	 */
	public synchronized void kill() throws IOException {
		stop();
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return httpBinding.getPort();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((httpBinding.getHostIPAddress() == null) ? 0 : httpBinding.getHostIPAddress().hashCode());
		result = prime * result + httpBinding.getPort();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		HTTPListener other = (HTTPListener) obj;
		if (httpBinding.getHostIPAddress() == null) {
			if (other.httpBinding.getHostIPAddress() != null) {
				return false;
			}
		} else if (!httpBinding.getHostIPAddress().equals(other.httpBinding.getHostIPAddress())) {
			return false;
		}
		if (httpBinding.getPort() != other.httpBinding.getPort()) {
			return false;
		}
		return true;
	}

}
