/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http.server;

import java.io.IOException;
import java.io.InputStream;

import org.yads.java.YADSFramework;
import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.Resource;
import org.yads.java.communication.monitor.MonitorStreamFactory;
import org.yads.java.communication.monitor.MonitoringContext;
import org.yads.java.communication.protocol.http.HTTPResponse;
import org.yads.java.communication.protocol.http.header.HTTPRequestHeader;
import org.yads.java.communication.protocol.http.server.responses.DefaultResourceResponse;
import org.yads.java.types.URI;
import org.yads.java.util.Log;

/**
 * Default implementation of an HTTP handler which allows access to a resource.
 */
public class DefaultHTTPResourceHandler implements HTTPRequestHandler {

	private final Resource			resource;

	/**
	 * Creates a default HTTP resource handler with a given buffer size.
	 * 
	 * @param resource the resource to send.
	 */
	public DefaultHTTPResourceHandler(Resource resource) {
		this.resource = resource;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.server.HTTPRequestHandler#handle
	 * (org.yads.java.types.URI,
	 * org.yads.java.communication.protocol.http.header.HTTPRequestHeader,
	 * org.yads.java.communication.protocol.http.HTTPInputStream,
	 * org.yads.java.communication.ConnectionInfo,
	 * org.yads.java.communication.monitor.MonitoringContext)
	 */
	@Override
	public HTTPResponse handle(HTTPRequestHeader header, InputStream body, ConnectionInfo connectionInfo, MonitoringContext context) throws IOException {
		URI request = connectionInfo.getTransportAddress();
		if (Log.isDebug()) {
			Log.debug("<I> Accessing HTTP resource at " + request, Log.DEBUG_LAYER_COMMUNICATION);
		}

		MonitorStreamFactory monFac = YADSFramework.getMonitorStreamFactory();
		if (monFac != null) {
			monFac.receiveResourceRequest(connectionInfo.getConnectionId(), context, request);
		}

		return new DefaultResourceResponse(resource, body, header.isSecure());
	}
}
