/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http.server.responses;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.Resource;
import org.yads.java.communication.monitor.MonitoringContext;
import org.yads.java.communication.protocol.http.HTTPResponse;
import org.yads.java.communication.protocol.http.HTTPResponseUtil;
import org.yads.java.communication.protocol.http.header.HTTPRequestHeader;
import org.yads.java.communication.protocol.http.header.HTTPResponseHeader;
import org.yads.java.communication.protocol.mime.MIMEUtil;
import org.yads.java.constants.HTTPConstants;
import org.yads.java.constants.MIMEConstants;
import org.yads.java.description.wsdl.WSDL;
import org.yads.java.schema.Schema;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import org.yads.java.types.ContentType;
import org.yads.java.types.URI;

public class DefaultResourceResponse extends HTTPResponse {

	private final Resource		resource;

	private final InputStream	requestBody;

	private HTTPResponseHeader	responseHeader	= null;

	private final long				size;

	public DefaultResourceResponse(Resource resource, InputStream requestBody, boolean secure) {
		this.resource = resource;
		this.requestBody = requestBody;

		size = resource.size();
		responseHeader = HTTPResponseUtil.getResponseHeader(HTTPConstants.HTTP_STATUS_OK, secure);
		ContentType contentType = resource.getContentType();
		if (size != 0) {
			if (contentType == WSDL.CONTENT_TYPE || contentType == Schema.CONTENT_TYPE) {
				responseHeader.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE, MIMEUtil.getMimeType(MIMEConstants.CONTENT_TYPE_TEXT_XML));
			} else {
				responseHeader.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE, MIMEUtil.getMimeType(contentType));
			}
		}

		HashMap resourceHeaderFields = resource.getHeaderFields();

		/*
		 * Check for additional header fields.
		 */
		if (resourceHeaderFields != null) {
			Set keys = resourceHeaderFields.keySet();
			Iterator it = keys.iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				String value = (String) resourceHeaderFields.get(key);
				responseHeader.addHeaderFieldValue(key, value);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPResponse#getResponseHeader
	 * ()
	 */
	@Override
	public HTTPResponseHeader getResponseHeader() {
		return responseHeader;
	}

	@Override
	public long calculateSize(ConnectionInfo connectionInfo) {
		return size;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPResponse#serializeResponseBody
	 * (org.yads.java.types.URI,
	 * org.yads.java.communication.protocol.http.header.HTTPRequestHeader,
	 * java.io.OutputStream, org.yads.java.communication.ConnectionInfo,
	 * org.yads.java.communication.monitor.MonitoringContext)
	 */
	@Override
	public void serializeResponseBody(URI request, HTTPRequestHeader header, OutputStream out, ConnectionInfo connectionInfo, MonitoringContext context) throws IOException {
		resource.serialize(request, header, requestBody, out, connectionInfo.getRemoteCredentialInfo(), connectionInfo.getCommunicationManagerId());

		/*
		 * flushes the stream.
		 */
		out.flush();

		if (context != null) {
			context.setResource(resource);
		}
	}
}
