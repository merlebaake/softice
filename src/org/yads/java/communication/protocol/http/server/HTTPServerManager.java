/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.IPConnectionInfo;
import org.yads.java.communication.connection.tcp.TCPConnection;
import org.yads.java.communication.connection.tcp.TCPConnectionHandler;
import org.yads.java.communication.protocol.http.HTTPBinding;
import org.yads.java.communication.protocol.http.HTTPResponse;
import org.yads.java.communication.protocol.http.HTTPResponseUtil;
import org.yads.java.communication.protocol.http.header.HTTPResponseHeader;
import org.yads.java.communication.protocol.http.server.responses.DefaultErrorResponse;
import org.yads.java.communication.protocol.mime.MIMEUtil;
import org.yads.java.constants.HTTPConstants;
import org.yads.java.types.ContentType;
import org.yads.java.types.URI;
import org.yads.java.util.Log;
import org.yads.java.util.StringUtil;

import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.nio.protocol.HttpAsyncExchange;
import org.yads.java.communication.protocol.http.header.HTTPRequestHeader;

/**
 * This class allows the creation of an HTTP server to handle incoming HTTP
 * requests.
 */
public class HTTPServerManager {

	/**
	 * This is a fall back for HTTP path search.
	 * <p>
	 * If <code>true</code> the used handler search will be changed. Usually we try to match the request directly to a registered handler. If no handler were found, the {@link DefaultHTTPNotFoundHandler} will be used to handle the request. Setting {@link #BACKTRACK} <code>true</code> implies that the handlers above the given request will also be searched.
	 * </p>
	 * <h4>Example</h4>
	 * <p>
	 * If no handler is set for <strong>/home/johndoe</strong>. The request for this path will fail. With {@link #BACKTRACK} <code>true</code>, the look up will be done at <strong>/home</strong> and <strong>/</strong> too.
	 * </p>
	 */

	/**
	 * This is the binding with the ipaddress and port for the connection.
	 */
	private HTTPBinding binding	= null;

	/**
	 * This is the root path of the HTTP server.
	 */
	private final URI base;

	/**
	 * A TCP connection handler which will handle the incoming HTTP requests.
	 */
	private final HTTPConnectionHandler connHandler	= new HTTPConnectionHandler();

	/**
	 * This table contains path and handler.
	 */
	private final HashMap<Object, HTTPRequestHandler> handlers = new HashMap();

	/**
	 * This table contains the created HTTP managers.
	 */
	private static final ConcurrentHashMap<String, HTTPServerManager> managers = new ConcurrentHashMap<>();

	public static void stopALLServers(String comManId) {
		ArrayList<HTTPServerManager> hservers = new ArrayList<>(managers.size());
        synchronized(managers) {
            Iterator<HTTPServerManager> it = managers.values().iterator();
            while (it.hasNext()) {
                HTTPServerManager hserver = it.next();
                if (hserver.binding.getCommunicationManagerId().equals(comManId)) {
                    hservers.add(hserver);
                    it.remove();
                }
            }
            it = hservers.iterator();
            while (it.hasNext()) {
                HTTPServerManager hserver = it.next();
                try {
                    HTTPServer.close(hserver.getIPAddress(), hserver.getPort());
                } catch (IOException e) {
                    Log.error("Unable to close HTTPServer: " + e);
                    Log.printStackTrace(e);
                }
            }            
        }
	}

	/**
	 * Returns a HTTP server for the given address and port. If no such server
	 * exists, a new server will be created.
	 * <p>
	 * The HTTP server is started at creation time.
	 * </p>
	 * 
	 * @param binding the address of the HTTP server.
     * @param create
	 * @return a new HTTP server.
	 * @throws IOException Throws exception if the port could not be opened at
	 *             the given address.
	 */
	public synchronized static HTTPServerManager get(HTTPBinding binding, boolean create) throws IOException {
		String key;
		HTTPServerManager server;
		int port = binding.getPort();

		if (port == 0) {
			if (!create) {
				return null;
			}
			server = new HTTPServerManager(binding);
			key = binding.getIpPortKey();
		} else {
			key = binding.getIpPortKey();
			server = (HTTPServerManager) managers.get(key);
			if (server != null) {
				binding.checkSecurityCredentialsEquality(server.binding);
				return server;
			}
			if (!create) {
				return null;
			}
			server = new HTTPServerManager(binding);
		}
		managers.put(key, server);
		return server;
	}

	private HTTPServerManager(HTTPBinding binding) throws IOException {
		this.binding = binding;
		HTTPServer.open(binding, connHandler);
		// generate base URI after actual port has been assigned
		base = new URI(binding.getURISchema() + "://" + binding.getHostIPAddress().getAddressWithoutNicId() + ":" + binding.getPort());
	}

	public synchronized void unregisterAndStop() throws IOException {
		managers.remove(binding.getIpPortKey());
		HTTPServer.close(getIPAddress(), getPort());
	}

	/**
	 * Registers a relative HTTP path with a given {@link HTTPRequestHandler}.
	 * 
	 * @param path the HTTP path.
	 * @param handler the HTTP handler which should handle the request.
     * @throws java.io.IOException
	 */
	public synchronized void register(String path, HTTPRequestHandler handler) throws IOException {
		Object oldValue = handlers.put(path, handler);
		if (oldValue != null) {
            if (Log.isDebug()) {
                Log.warn("Overwriting existing path: " + path);		
            }
        }
	}

	/**
	 * Registers a relative HTTP path and a content type with a given {@link HTTPRequestHandler}.
	 * 
	 * @param path the HTTP path.
	 * @param type the HTTP content type.
	 * @param handler the HTTP handler which should handle the request.
     * @throws java.io.IOException
	 */
	public synchronized void register(String path, ContentType type, HTTPRequestHandler handler) throws IOException {
		MappingEntry entry = new MappingEntry(path, type);

		HTTPRequestHandler oldValue = handlers.put(entry, handler);

		if (oldValue != null) {
			handlers.put(entry, oldValue);
			throw new IOException("Path (" + path + ") with type (" + type + ") already in use: " + path);
		}
	}

	/**
	 * Removes registration of a relative HTTP path for a {@link HTTPRequestHandler}, but will not shut down the HTTP server when
	 * the last handler was removed.
	 * 
	 * @param path
	 * @return
	 */
	public synchronized HTTPRequestHandler unregister(String path) {
		HTTPRequestHandler handler = (HTTPRequestHandler) handlers.remove(path);
		return handler;
	}

	/**
	 * Removes registration of a relative HTTP path for a {@link HTTPRequestHandler}.
	 * 
     * @param binding
	 * @param path the HTTP path.
	 * @return the removed {@link HTTPRequestHandler}.
	 */
	public synchronized HTTPRequestHandler unregister(HTTPBinding binding, String path) {
		HTTPRequestHandler handler = (HTTPRequestHandler) handlers.remove(path);
		if (handlers.isEmpty()) {
			try {
				unregisterAndStop();
				binding.resetAutoPort();
			} catch (IOException e) {
				Log.error("Cannot shutdown TCP server after all registrations removed. " + e.getMessage());
			}
		}
		return handler;
	}

	/**
	 * Removes registration of a relative HTTP path and content type for a HTTP
	 * handler.
	 * 
     * @param binding
	 * @param path the HTTP path.
	 * @param type the HTTP content type.
	 * @return the removed {@link HTTPRequestHandler}.
	 */
	public synchronized HTTPRequestHandler unregister(HTTPBinding binding, String path, ContentType type) {
		if (path == null) {
			path = binding.getPath();
		}

		MappingEntry entry = new MappingEntry(path, type);
		HTTPRequestHandler handler = (HTTPRequestHandler) handlers.remove(entry);
		if (handlers.isEmpty()) {
			try {
				unregisterAndStop();
				binding.resetAutoPort();
			} catch (IOException e) {
				Log.error("Cannot shutdown TCP server after all registrations removed. " + e.getMessage());
			}
		}
		return handler;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return binding.getPort();
	}

	public IPAddress getIPAddress() {
		return binding.getHostIPAddress();
	}

	public HTTPBinding getBinding() {
		return binding;
	}

	/**
	 * Handles the incoming HTTP requests.
	 */
	private class HTTPConnectionHandler implements TCPConnectionHandler {

		@Override
		public void handle(TCPConnection connection) throws IOException {

            InputStream in = null;
			IPConnectionInfo connectionInfo = connection.getConnectionInfo();
			ContentType type = null;
            HTTPResponse response = null;
			try {
                // Streams already point to HTTP body, the http exchange object can be used for other stuff
                in = connection.getInputStream();
                try (OutputStream out = connection.getOutputStream()) {
                    HttpAsyncExchange he = connection.getHttpExchange();
                    HttpRequest request = he.getRequest();
                    Header [] headers = request.getAllHeaders();
                    String requestMethod = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
                    java.net.URI uri = new java.net.URI(request.getRequestLine().getUri());
                    HTTPRequestHeader reqHeader = new HTTPRequestHeader(requestMethod, uri.getPath(), binding.isSecure(), uri.getScheme());
                    for (Header nextEntry : headers)
                        reqHeader.addHeaderFieldValue(nextEntry.getName().toLowerCase(), nextEntry.getValue().toLowerCase());
                    
                    if (methodNotAllowed(requestMethod, he, out))
                        return;
                    
                    String path = uri.getPath();
                    connectionInfo.setTransportAddress(new URI(base, path));
                    final Header contentTypeHeader = request.getFirstHeader(HTTPConstants.HTTP_HEADER_CONTENT_TYPE);
                    
                    type = MIMEUtil.createContentType(contentTypeHeader != null? contentTypeHeader.getValue() : null);
                    
                    /*
                    * This object will contain the HTTP response from the
                    * handler.
                    */
                    boolean [] doReturn = new boolean[1];
                    
                    response = findResponse(response, reqHeader, path, type, in, connectionInfo, he, out, doReturn);
                    while (in != null && in.read() != -1) {
                        // Eat up input stream.
                    }
                    
                    if (doReturn[0])
                        return;
                    
                    /*
                    * Change context from incoming to outgoing.
                    */
                    ConnectionInfo swappedConInfo = connectionInfo.createSwappedConnectionInfo();
                    
                    /*
                    * Analyze and serialize the HTTP response header and create
                    * a output stream to write the HTTP response body.
                    */
                    
                    HttpResponse httpResponse = he.getResponse();
                    HTTPResponseHeader responseHeader = response.getResponseHeader();
                    
                    String date = responseHeader.getHeaderFieldValue(HTTPConstants.HTTP_HEADER_DATE);
                    if (date == null) {
                        Date d = new Date();
                        httpResponse.addHeader(HTTPConstants.HTTP_HEADER_DATE, StringUtil.getHTTPDate(d.getTime()));
                    }
                    
                    if (Log.isDebug()) {
                        Log.debug("<O> " + responseHeader + " to " + connectionInfo.getSourceAddress() + ", " + connection, Log.DEBUG_LAYER_COMMUNICATION);
                    }
                    
                    httpResponse.setStatusCode(response.getResponseHeader().getStatus());

                    // Set response headers
                    for (Entry<String, String> next : responseHeader.getHeaderfields().entrySet())
                        httpResponse.addHeader(next.getKey(), next.getValue());
                    
                    if (response.getResponseHeader().getStatus() != HTTPConstants.HTTP_STATUS_ACCEPTED) {
                        // Write response body to stream
                        response.serializeResponseBody(new URI(uri.toString()), reqHeader, out, swappedConInfo, null);
                        out.flush();                        
                    }
                }
                catch(Exception e) {
                    Log.printStackTrace(e);
                }

			} finally {
			}
		}
    }
    
    private HTTPResponse findResponse(HTTPResponse response, HTTPRequestHeader reqHeader, String path, ContentType type, InputStream in, IPConnectionInfo connectionInfo, HttpAsyncExchange he, OutputStream out, boolean[] doReturn) throws IOException {
        /*
        * Try to find the HTTP handler for this request.
        */
        HTTPRequestHandler requestHandler = getHTTPHandler(path, type);
        if (requestHandler == null) {
            String tempPath = path;
            do {
                int lastIndexOfSlash = tempPath.lastIndexOf(URI.GD_SLASH);
                if (lastIndexOfSlash == -1) {
                    break;
                }
                tempPath = tempPath.substring(0, lastIndexOfSlash);
                requestHandler = getHTTPHandler(tempPath, type);
            } while (requestHandler == null);
        }
        if (requestHandler != null && response == null) {

            try {
                response = requestHandler.handle(reqHeader, in, connectionInfo, null);
            } catch (IOException e) {
                // Send HTTP 500 internal server error
                String note = "The registered HTTP handler (" + requestHandler.getClass().getName() + ") got an exception. " + e.getMessage();
                Log.error(note);
                Log.printStackTrace(e);
                HTTPResponseHeader responseHeader = HTTPResponseUtil.getResponseHeader(HTTPConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR, false);
                he.getResponse().setStatusCode(responseHeader.getStatus());
                out.write(responseHeader.getReason().getBytes());
                out.flush();
                out.close();
                if (Log.isWarn()) {
                    Log.warn("Closing HTTP connection. " + note + ".");
                }
                doReturn[0] = true;
            }
            catch (Exception e) {
                // Send HTTP 500
                HTTPResponseHeader responseHeader = HTTPResponseUtil.getResponseHeader(HTTPConstants.HTTP_STATUS_INTERNAL_SERVER_ERROR, false);
                he.getResponse().setStatusCode(responseHeader.getStatus());
                out.write(responseHeader.getReason().getBytes());
                out.flush();
                out.close();
                doReturn[0] = true;
            }
        }
        if (response == null || response.getResponseHeader() == null) {

                /*
                 * Default 404 Not found.
                 */
                response = DefaultErrorResponse.getDefaultNotFoundResponse(reqHeader);
        }
        return response;
    }    
    
    private boolean methodNotAllowed(String requestMethod, HttpAsyncExchange he, OutputStream out) throws IOException {
        if (!requestMethod.equalsIgnoreCase("GET") && !requestMethod.equalsIgnoreCase("POST")) {
            // Method not supported
            String notAllowed = "405 Method Not Allowed";
            he.getResponse().setStatusCode(405);
            out.write(notAllowed.getBytes());
            out.flush();
            return true;
        }
        return false;
    }    

	/**
	 * Returns the HTTP handler for the given path and content type.
	 * <p>
	 * This method will search for the HTTP handler depending on the value of the {@link HTTPServerManager#BACKTRACK} field.
	 * </p>
	 * 
	 * @param path the path.
	 * @param type the content type.
	 * @return the HTTP handler which match path and content type.
	 */
	private HTTPRequestHandler getHTTPHandler(String path, ContentType type) {
		MappingEntry entry = new MappingEntry(path, type);

		HTTPRequestHandler handler = null;

		/*
		 * Tries to get specific handler for the given type.
		 */
		handler = (HTTPRequestHandler) handlers.get(entry);

		/*
		 * No specific handler found? Tries to find an handler which accepts
		 * every type for this address.
		 */
		if (handler == null) {
			handler = (HTTPRequestHandler) handlers.get(path);
		}

		return handler;
	}

	/**
	 * This entry contains a URI and content type.
	 */
	private class MappingEntry {

		private String		path	= null;

		private ContentType	type	= null;

		MappingEntry(String path, ContentType type) {
			this.path = path;
			this.type = type;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			result = prime * result + ((path == null) ? 0 : path.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			MappingEntry other = (MappingEntry) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (type == null) {
				if (other.type != null) {
					return false;
				}
			} else if (!type.equals(other.type)) {
				return false;
			}
			if (path == null) {
				if (other.path != null) {
					return false;
				}
			} else if (!path.equals(other.path)) {
				return false;
			}
			return true;
		}

		private HTTPServerManager getOuterType() {
			return HTTPServerManager.this;
		}

	}

}
