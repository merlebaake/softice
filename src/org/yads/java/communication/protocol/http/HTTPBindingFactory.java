/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http;

import org.yads.java.communication.IPCommunicationBindingFactory;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.NetworkInterface;
import org.yads.java.communication.structures.Binding;
import org.yads.java.communication.structures.CommunicationBinding;
import org.yads.java.communication.structures.IPDiscoveryBinding;
import org.yads.java.security.CredentialInfo;
import org.yads.java.util.Clazz;
import org.yads.java.util.WS4DIllegalStateException;

public class HTTPBindingFactory implements IPCommunicationBindingFactory {

	/** Default HTTP Binding Factory Class */
	public static final String			DEFAULT_SECURE_HTTP_BINDING_FACTORY_CLASS	= "SecureHTTPBindingFactory";

	public static final String			DEFAULT_SECURE_HTTP_BINDING_FACTORY_PATH	= "org.yads.java.communication.protocol.http." + DEFAULT_SECURE_HTTP_BINDING_FACTORY_CLASS;

	private static HTTPBindingFactory	instance									= null;

	private static boolean				getInstanceFirstCall						= true;

	/**
	 * should only be called by getInstance
	 */
	protected HTTPBindingFactory() {

	}

	public static synchronized HTTPBindingFactory getInstance() {
		if (getInstanceFirstCall) {
			getInstanceFirstCall = false;
			try {
				Class clazz = Clazz.forName(DEFAULT_SECURE_HTTP_BINDING_FACTORY_PATH);
				instance = (HTTPBindingFactory) clazz.newInstance();
			} catch (Exception e1) {
				instance = new HTTPBindingFactory();
			}
		}
		return instance;
	}

	@Override
	public CommunicationBinding createCommunicationBinding(String comManId, NetworkInterface iface, IPAddress ipAddress, int port, String path, CredentialInfo credentialInfo) throws WS4DIllegalStateException {
		if (credentialInfo != null && credentialInfo != CredentialInfo.EMPTY_CREDENTIAL_INFO) {
			throw new WS4DIllegalStateException("Only HTTPBinding is supported by this factory. (Security module for HTTPSBinding not available.)");
		}
		return new HTTPBinding(ipAddress, port, path, comManId);
	}

	@Override
	public Binding createDiscoveryBindingForAddressAndPort(String comManId, NetworkInterface iface, IPAddress ipAddress, int fixedPort) {
		return new IPDiscoveryBinding(comManId, iface, ipAddress, fixedPort);
	}
}
