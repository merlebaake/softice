/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.ornet.softice.SoftICE;

import org.yads.java.communication.ProtocolException;
import org.yads.java.communication.ResourceLoader;
import org.yads.java.configuration.DPWSProperties;
import org.yads.java.constants.HTTPConstants;
import org.yads.java.security.CredentialInfo;
import org.yads.java.types.URI;
import org.yads.java.util.Log;

/**
 * Utility class for handling HTTP requests.
 */
public class HTTPRequestUtil {

	protected static final String	FAULT_METHOD_NOT_SUPPORTED	= "HTTP Method not supported.";

	// predefined exception messages.
	protected static final String	FAULT_UNEXPECTED_END		= "Unexpected end of stream.";

	protected static final String	FAULT_MALFORMED_REQUEST		= "Malformed HTTP request line.";

	protected static final String	FAULT_MALFORMED_HEADERFIELD	= "Malformed HTTP header field.";

	protected static final String	FAULT_MALFORMED_CHUNK		= "Malformed HTTP chunk header.";


    private HTTPRequestUtil() {

	}

	/**
	 * Returns an input stream which allows the reading of a resource from the
	 * given location.
	 * 
	 * @param location the resource's location (e.g.
	 *            http://example.org/test.wsdl).
	 * @param credentialInfo
	 * @return an input stream for the given resource.
	 */
	public static ResourceLoader getResourceAsStream(URI location, CredentialInfo credentialInfo, HashMap<String, String> additionalHeaderfields, String comManId) throws IOException, ProtocolException {
		if (!location.getSchemaDecoded().toLowerCase().startsWith(HTTPConstants.HTTP_SCHEMA) && !location.getSchemaDecoded().startsWith(HTTPConstants.HTTPS_SCHEMA))
            return null;

        if (Log.isDebug()) {
            Log.debug("<O> Accessing resource over " + location.getSchemaDecoded() + " from: " + location, Log.DEBUG_LAYER_COMMUNICATION);
        }

        Request req = Request.Post(location.toString())
                .connectTimeout((int) DPWSProperties.getInstance().getHTTPClientRequestTimeout())
                .socketTimeout((int) DPWSProperties.getInstance().getHTTPClientRequestTimeout());     
        
        if (additionalHeaderfields != null)
            for (Map.Entry<String, String> next : additionalHeaderfields.entrySet())
                req.addHeader(next.getKey(), next.getValue());
        
        Executor ex = null;
        
        try {
            ex = Executor.newInstance(HTTPClient.createHttpClient(SoftICE.getInstance().getClientSSLContext()));
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException ex1) {
            Logger.getLogger(HTTPRequestUtil.class.getName()).log(Level.SEVERE, null, ex1);
            return null;
        }
        
        try {
            InputStream in = ex.execute(req).returnContent().asStream();       
            ResourceLoader rl = new ResourceLoader(in, null);
            return rl;            
        }
        catch(Exception e) {
            Log.error("Can't access resource due to the following reason: " + e.getMessage());
        }
			
        return null;
	}

}
