/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.soap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.yads.java.YADSFramework;
import org.yads.java.communication.CommunicationException;
import org.yads.java.communication.CommunicationManagerRegistry;
import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.DPWSProtocolInfo;
import org.yads.java.communication.ProtocolInfo;
import org.yads.java.communication.monitor.MonitorStreamFactory;
import org.yads.java.communication.monitor.MonitoredMessageReceiver;
import org.yads.java.communication.monitor.MonitoringContext;
import org.yads.java.communication.protocol.http.HTTPRequest;
import org.yads.java.communication.protocol.http.HTTPResponseHandler;
import org.yads.java.communication.protocol.http.HTTPResponseUtil;
import org.yads.java.communication.protocol.http.header.HTTPRequestHeader;
import org.yads.java.communication.protocol.http.header.HTTPResponseHeader;
import org.yads.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.yads.java.communication.receiver.MessageReceiver;
import org.yads.java.configuration.DPWSProperties;
import org.yads.java.configuration.HTTPProperties;
import org.yads.java.constants.HTTPConstants;
import org.yads.java.constants.MIMEConstants;
import org.yads.java.constants.MessageConstants;
import org.yads.java.dispatch.MessageInformer;
import org.yads.java.message.Message;
import org.yads.java.security.CredentialInfo;
import org.yads.java.communication.protocol.mime.MIMEUtil;
import org.yads.java.types.AttributedURI;
import org.yads.java.types.ContentType;
import org.yads.java.types.XAddressInfo;
import org.yads.java.util.Log;
import org.yads.java.util.Toolkit;

/**
 *
 */
public class SOAPRequest implements HTTPRequest {

	private static final MessageInformer	MESSAGE_INFORMER	= MessageInformer.getInstance();

	private final Message					request;

	private final MessageReceiver			receiver;

	private final HTTPRequestHeader			header;

	private long							size				= -42;

	private ByteArrayOutputStream[]			buffer				= null;

	private XAddressInfo					targetXAddressInfo	= null;

	private AttributedURI					optionalMessageId	= null;

	private boolean							secure				= false;

	/**
	 * @param targetADdress
	 * @param request
	 * @param receiver
	 */
	public SOAPRequest(Message request, MessageReceiver receiver, XAddressInfo targetXAddress, AttributedURI optionalMessageId, CredentialInfo credentialInfo) {
		super();
		this.request = request;
		this.receiver = receiver;
		this.targetXAddressInfo = targetXAddress;
		this.optionalMessageId = optionalMessageId;

		this.secure = (targetXAddress.getXAddress().getSchemaDecoded().equals(HTTPConstants.HTTPS_SCHEMA));

		header = new HTTPRequestHeader(HTTPConstants.HTTP_METHOD_POST, targetXAddress.getXAddress().getPath(), secure, HTTPConstants.HTTP_VERSION11);

		String contentTypeStr = MIMEUtil.getMimeType(MIMEConstants.CONTENT_TYPE_SOAPXML);
		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE, contentTypeStr);
		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_TE, HTTPConstants.HTTP_HEADERVALUE_TE_TRAILERS);

		int chunkedMode = DPWSProperties.HTTP_CHUNKED_OFF_IF_POSSIBLE;

		/*
		 * Check for HTTP chunk coding global settings.
		 */
		ProtocolInfo protocolInfo = targetXAddress.getProtocolInfo();
		if (protocolInfo != null) {
			if (protocolInfo instanceof DPWSProtocolInfo) {
				DPWSProtocolInfo dpi = (DPWSProtocolInfo) protocolInfo;
				chunkedMode = dpi.getHttpRequestChunkedMode();
			}

			/*
			 * Check for HTTP chunk coding address settings.
			 */
			String adr = targetXAddress.toString();
			int adrChunkedMode = HTTPProperties.getInstance().getChunkMode(adr);
			if (adrChunkedMode > -1) {
				chunkedMode = adrChunkedMode;
				if (Log.isDebug()) {
					Log.debug("Chunk mode changed to " + chunkedMode + " for address " + adr);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#getRequestHeader()
	 */
	@Override
	public HTTPRequestHeader getRequestHeader(ConnectionInfo connectionInfo) {
		return header;
	}

	@Override
	public XAddressInfo getTargetXAddressInfo() {
		return targetXAddressInfo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#getResponseHandler
	 * (org.yads.java.communication.ContentType)
	 */
	@Override
	public HTTPResponseHandler getResponseHandler(ContentType contentType) throws IOException {
		if (MIMEConstants.CONTENT_TYPE_TEXT_XML.equals(contentType) || MIMEConstants.CONTENT_TYPE_SOAPXML.equals(contentType)) {
			return new HTTPResponseHandler() {

				/*
				 * (non-Javadoc)
				 * @see
				 * org.yads.java.communication.protocol.http.HTTPResponseHandler
				 * #handle(org.yads.java.communication.protocol.http.header.
				 * HTTPResponseHeader, java.io.InputStream,
				 * org.yads.java.communication.protocol.http.HTTPRequest,
				 * org.yads.java.communication.DPWSConnectionInfo,
				 * org.yads.java.io.monitor.MonitoringContext)
				 */
				@Override
				public void handle(HTTPResponseHeader header, InputStream body, HTTPRequest request, ConnectionInfo connectionInfo, MonitoringContext context) throws IOException {
					int httpStatus = header.getStatus();
					// TODO filter other potentially empty HTTP responses
					if (httpStatus == HTTPConstants.HTTP_STATUS_NO_CONTENT) {
						return;
					}

					MessageReceiver r;
					MonitorStreamFactory monFac = YADSFramework.getMonitorStreamFactory();
					if (monFac != null) {
						r = new MonitoredMessageReceiver(receiver, context);
					} else {
						r = receiver;
					}

					if (body != null) {
						SOAPMessageGeneratorFactory.getInstance().getSOAP2MessageGenerator().deliverMessage(body, r, connectionInfo, null);
					} else {
						/*
						 * regardless of the actual HTTP status code (be it a
						 * 4xx, 5xx or another one), if we get here, this means
						 * the other side is responding with a content type of
						 * application/soap+xml, but without a SOAP message
						 * within the HTTP body; so we can safely assume this is
						 * a faulty condition and deliver a dummy SOAP fault
						 * instead
						 */
						r.receiveFailed(new CommunicationException("Missing HTTP body in message"), connectionInfo);
					}
				}
			};

		} else if (MIMEConstants.CONTENT_TYPE_TEXT_HTML.equals(contentType)) {
			/*
			 * we may get text/html response e.g. when other side sends a
			 * HTTP-level error like 404, etc.
			 */
			return new HTTPResponseHandler() {

				/*
				 * (non-Javadoc)
				 * @see
				 * org.yads.java.communication.protocol.http.HTTPResponseHandler
				 * #handle(org.yads.java.communication.protocol.http.header.
				 * HTTPResponseHeader, java.io.InputStream,
				 * org.yads.java.communication.protocol.http.HTTPRequest,
				 * org.yads.java.communication.DPWSConnectionInfo,
				 * org.yads.java.io.monitor.MonitoringContext)
				 */
				@Override
				public void handle(HTTPResponseHeader header, InputStream body, HTTPRequest request, ConnectionInfo connectionInfo, MonitoringContext context) throws IOException {
					MessageReceiver r;
					MonitorStreamFactory monFac = YADSFramework.getMonitorStreamFactory();
					if (monFac != null) {
						r = new MonitoredMessageReceiver(receiver, context);
					} else {
						r = receiver;
					}

					DPWSCommunicationManager comMan = (DPWSCommunicationManager) CommunicationManagerRegistry.getCommunicationManager(connectionInfo.getCommunicationManagerId());
					if (header.getStatus() == HTTPConstants.HTTP_STATUS_UNAUTHORIZED) {
						r.receive(comMan.createAuthorizationFault(SOAPRequest.this.request), connectionInfo);
					} else {
						r.receive(comMan.createGenericFault(SOAPRequest.this.request, HTTPResponseUtil.getHTTPStatusString(header.getStatus())), connectionInfo);
					}
				}

			};
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#requestSendFailed
	 * (java.lang.Exception, org.yads.java.communication.ConnectionInfo)
	 */
	@Override
	public void requestSendFailed(Exception e, ConnectionInfo connectionInfo, MonitoringContext context) {
		MonitorStreamFactory monFac = YADSFramework.getMonitorStreamFactory();
		if (monFac != null) {
			monFac.receivedFault(connectionInfo.getConnectionId(), context, e);
		}
		receiver.sendFailed(e, connectionInfo);
	}

	@Override
	public void messageWithoutBodyReceived(int code, ConnectionInfo connectionInfo, MonitoringContext context) {
		MonitorStreamFactory monFac = YADSFramework.getMonitorStreamFactory();
		String reason = HTTPResponseUtil.getResponseHeader(code, header.isSecure()).getReason();
		if (monFac != null) {
			monFac.receiveNoContent(connectionInfo.getConnectionId(), context, reason);
		}
		receiver.receiveNoContent(reason, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#responseReceiveFailed
	 * (java.lang.Exception, org.yads.java.communication.ConnectionInfo)
	 */
	@Override
	public void responseReceiveFailed(Exception e, ConnectionInfo connectionInfo, MonitoringContext context) {
		MonitorStreamFactory monFac = YADSFramework.getMonitorStreamFactory();
		if (monFac != null && connectionInfo != null) {
			monFac.receivedFault(connectionInfo.getConnectionId(), context, e);
		}
		receiver.receiveFailed(e, connectionInfo);
	}

	@Override
	public long calculateSize(ConnectionInfo connectionInfo) {
		if (request == null) {
			return 0;
		}

		if (size != -42) {
			return size;
		}

		try {
			buffer = new ByteArrayOutputStream[1];
			DPWSCommunicationManager comMan = (DPWSCommunicationManager) CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
			size = comMan.serializeMessage(request, buffer, connectionInfo, optionalMessageId);
			if (size == -1) {
				buffer = null;
			}
		} catch (IOException ex) {
			if (Log.isError()) {
				Log.printStackTrace(ex);
			}
			size = -1;
			buffer = null;
		}
		return size;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPRequest#serializeRequestBody
	 * (java.io.OutputStream, org.yads.java.communication.ConnectionInfo,
	 * org.yads.java.io.monitor.MonitoringContext)
	 */
	@Override
	public void serializeRequestBody(OutputStream out, ConnectionInfo connectionInfo, MonitoringContext context) throws IOException {
		if (request == null) {
			// omit one-ways
			return;
		}
		if (buffer == null) {
			DPWSCommunicationManager comMan = (DPWSCommunicationManager) CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
			comMan.serializeMessage(request, out, connectionInfo, optionalMessageId);
		} else {
            Toolkit.getInstance().writeBufferToStream(buffer[0], out);
            out.flush();
        }

		MESSAGE_INFORMER.forwardMessage(request, connectionInfo, optionalMessageId);

		if (context != null) {
			context.setMessage(request);
		}
	}

	@Override
	public AttributedURI getOptionalMessageId() {
		return optionalMessageId;
	}

	@Override
	public boolean needsBody() {
		return true;
	}

}
