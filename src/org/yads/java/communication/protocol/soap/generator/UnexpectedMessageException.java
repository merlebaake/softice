/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.soap.generator;

import org.yads.java.message.Message;
import org.yads.java.message.discovery.ProbeMessage;
import org.yads.java.message.discovery.ResolveMatchesMessage;

/**
 * Thrown when a {@link Message} is received in response to a previously sent
 * request message, but the response message type doesn't correspond to the
 * request message type (e.g. when receiving a {@link ResolveMatchesMessage} in
 * response to a {@link ProbeMessage}).
 */
public class UnexpectedMessageException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -6075038208769929699L;

	/**
	 * 
	 */
	public UnexpectedMessageException() {}

	/**
	 * @param message
	 */
	public UnexpectedMessageException(String message) {
		super(message);
	}

}
