/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.soap;

import org.yads.java.YADSFramework;
import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.callback.ResponseCallback;
import org.yads.java.eventing.EventSource;
import org.yads.java.message.FaultMessage;
import org.yads.java.message.InvokeMessage;
import org.yads.java.message.Message;
import org.yads.java.message.discovery.ProbeMatchesMessage;
import org.yads.java.message.discovery.ProbeMessage;
import org.yads.java.message.discovery.ResolveMatchesMessage;
import org.yads.java.message.discovery.ResolveMessage;
import org.yads.java.message.eventing.GetStatusMessage;
import org.yads.java.message.eventing.GetStatusResponseMessage;
import org.yads.java.message.eventing.RenewMessage;
import org.yads.java.message.eventing.RenewResponseMessage;
import org.yads.java.message.eventing.SubscribeMessage;
import org.yads.java.message.eventing.SubscribeResponseMessage;
import org.yads.java.message.eventing.UnsubscribeMessage;
import org.yads.java.message.eventing.UnsubscribeResponseMessage;
import org.yads.java.message.metadata.GetMessage;
import org.yads.java.message.metadata.GetMetadataMessage;
import org.yads.java.message.metadata.GetMetadataResponseMessage;
import org.yads.java.message.metadata.GetResponseMessage;
import org.yads.java.service.OperationDescription;
import org.yads.java.types.AttributedURI;
import org.yads.java.types.XAddressInfo;

/**
 * This is a special implementation of the {@link ResponseCallback} which allows
 * to continue handling further messages, without waiting for the callback to be
 * finished.
 */
class NonBlockingResponseCallback implements ResponseCallback {

	private final ResponseCallback	callback;

	private XAddressInfo			targetXAddresInfo	= null;

	/**
	 * Creates a non-blocking callback for SOAP messages.
	 * 
	 * @param to the origin callback.
	 */
	NonBlockingResponseCallback(XAddressInfo targetXAddressInfo, ResponseCallback to) {
		this.targetXAddresInfo = targetXAddressInfo;
		this.callback = to;
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message,
	 * org.yads.java.message.discovery.ProbeMatchesMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final ProbeMessage probe, final ProbeMatchesMessage probeMatches, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(probe, probeMatches, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message,
	 * org.yads.java.message.discovery.ResolveMatchesMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final ResolveMessage resolve, final ResolveMatchesMessage resolveMatches, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(resolve, resolveMatches, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message,
	 * org.yads.java.message.metadata.GetResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final GetMessage get, final GetResponseMessage getResponse, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(get, getResponse, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message, org.yads.java.message.metadata
	 * .GetMetadataResponseMessage, org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final GetMetadataMessage getMetadata, final GetMetadataResponseMessage getMetadataResponse, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(getMetadata, getMetadataResponse, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message,
	 * org.yads.java.message.eventing.SubscribeResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final SubscribeMessage subscribe, final SubscribeResponseMessage subscribeResponse, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(subscribe, subscribeResponse, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message,
	 * org.yads.java.message.eventing.GetStatusResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final GetStatusMessage getStatus, final GetStatusResponseMessage getStatusResponse, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(getStatus, getStatusResponse, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message,
	 * org.yads.java.message.eventing.RenewResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final RenewMessage renew, final RenewResponseMessage renewResponse, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(renew, renewResponse, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message,
	 * org.yads.java.message.eventing.UnsubscribeResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final UnsubscribeMessage unsubscribe, final UnsubscribeResponseMessage unsubscribeResponse, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(unsubscribe, unsubscribeResponse, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message,
	 * org.yads.java.message.invocation.InvokeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final InvokeMessage invokeRequest, final InvokeMessage invokeResponse, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(invokeRequest, invokeResponse, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.ResponseCallback#handle(org.yads.java.
	 * communication.message.Message, org.yads.java.message.FaultMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handle(final Message request, final FaultMessage fault, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handle(request, fault, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.ResponseCallback#handleTransmissionException
	 * (org.yads.java.message.Message, java.lang.Exception,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handleTransmissionException(final Message request, final Exception exception, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handleTransmissionException(request, exception, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.ResponseCallback#handleMalformedResponseException
	 * (org.yads.java.message.Message, java.lang.Exception,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void handleMalformedResponseException(final Message request, final Exception exception, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handleMalformedResponseException(request, exception, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.ResponseCallback#handleTimeout(org.yads.java
	 * .communication.message.Message)
	 */
	@Override
	public void handleTimeout(final Message request, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {

		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handleTimeout(request, connectionInfo, optionalMessageId);
			}

		});
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.ResponseCallback#getOperation()
	 */
	@Override
	public OperationDescription getOperation() {
		return callback.getOperation();
	}

	@Override
	public EventSource getEvent() {
		return callback.getEvent();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.ResponseCallback#setTargetAddress(org.ws4d
	 * .java.types.XAddressInfo)
	 */
	@Override
	public void setTargetAddress(XAddressInfo targetXAddressInfo) {
		this.targetXAddresInfo = targetXAddressInfo;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.ResponseCallback#getTargetAddress()
	 */
	@Override
	public XAddressInfo getTargetAddress() {
		return targetXAddresInfo;
	}

	@Override
	public void handleNoContent(final Message request, final String reason, final ConnectionInfo connectionInfo, final AttributedURI optionalMessageId) {
		YADSFramework.getThreadPool().execute(new Runnable() {

			/*
			 * (non-Javadoc)
			 * @see java.lang.Runnable#run()
			 */
			@Override
			public void run() {
				callback.handleNoContent(request, reason, connectionInfo, optionalMessageId);
			}

		});

	}

	@Override
	public void requestStartedWithTimeout(long duration, Message message, String communicationInterfaceDescription) {
	}
}
