/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.soap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.DPWSProtocolInfo;
import org.yads.java.communication.ProtocolInfo;
import org.yads.java.communication.monitor.MonitoringContext;
import org.yads.java.communication.protocol.http.HTTPResponse;
import org.yads.java.communication.protocol.http.HTTPResponseUtil;
import org.yads.java.communication.protocol.http.header.HTTPRequestHeader;
import org.yads.java.communication.protocol.http.header.HTTPResponseHeader;
import org.yads.java.communication.protocol.mime.MIMEUtil;
import org.yads.java.configuration.DPWSProperties;
import org.yads.java.constants.HTTPConstants;
import org.yads.java.constants.MIMEConstants;
import org.yads.java.constants.MessageConstants;
import org.yads.java.dispatch.MessageInformer;
import org.yads.java.message.Message;
import org.yads.java.communication.CommunicationManagerRegistry;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.types.URI;
import org.yads.java.util.Log;
import org.yads.java.util.Toolkit;

/**
 *
 */
public class SOAPResponse extends HTTPResponse {

	private static final MessageInformer	MESSAGE_INFORMER	= MessageInformer.getInstance();

	private final Message					response;

	private final HTTPResponseHeader		header;

	private long							size				= -42;

	private ByteArrayOutputStream[]			buffer				= null;

	/**
	 * @param httpStatus
	 * @param response
	 */
	public SOAPResponse(int httpStatus, boolean secure, Message response, ProtocolInfo protocolInfo) {
		super();
		this.response = response;

		header = HTTPResponseUtil.getResponseHeader(httpStatus, secure);

		String contentType = MIMEUtil.getMimeType(MIMEConstants.CONTENT_TYPE_SOAPXML);
		header.addHeaderFieldValue(HTTPConstants.HTTP_HEADER_CONTENT_TYPE, contentType);

		int chunkedMode = DPWSProperties.HTTP_CHUNKED_OFF_IF_POSSIBLE;

		/*
		 * Check for HTTP chunk coding global settings.
		 */
		if (protocolInfo != null) {
			if (protocolInfo instanceof DPWSProtocolInfo) {
				DPWSProtocolInfo dpvi = (DPWSProtocolInfo) protocolInfo;
				chunkedMode = dpvi.getHttpResponseChunkedMode();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.http.HTTPResponse#getResponseHeader
	 * ()
	 */
	@Override
	public HTTPResponseHeader getResponseHeader() {
		return header;
	}

	@Override
	public long calculateSize(ConnectionInfo connectionInfo) {
		if (response == null) {
			return 0;
		}

		if (size != -42) {
			return size;
		}

		try {
			buffer = new ByteArrayOutputStream[1];
			DPWSCommunicationManager comMan = (DPWSCommunicationManager) CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
			size = comMan.serializeMessage(response, buffer, connectionInfo, null);
			if (size == -1) {
				buffer = null;
			}
		} catch (IOException ex) {
			if (Log.isError()) {
				Log.printStackTrace(ex);
			}
			size = -1;
			buffer = null;
		}
		return size;
	}

	@Override
	public void serializeResponseBody(URI request, HTTPRequestHeader header, OutputStream out, ConnectionInfo connectionInfo, MonitoringContext context) throws IOException {
		if (response == null) {
			// omit one-ways
			return;
		}

		if (buffer == null) {
			DPWSCommunicationManager comMan = (DPWSCommunicationManager) CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
			comMan.serializeMessage(response, out, connectionInfo, null);
		} else {        
            Toolkit.getInstance().writeBufferToStream(buffer[0], out);
            out.flush();
        }		

		MESSAGE_INFORMER.forwardMessage(response, connectionInfo, null);

		if (context != null) {
			context.setMessage(response);
		}
	}

}
