/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.mime;

import org.yads.java.constants.MIMEConstants;
import java.util.Iterator;
import java.util.Map;
import org.yads.java.types.ContentType;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.StringUtil;
import org.yads.java.util.Toolkit;

/**
 * Utility class for MIME handling.
 */
public class MIMEUtil {

	public static int				DEFAULT_MIME_BUFFER			= 1024;

	// predefined exception messages.
	protected static final String	FAULT_UNEXPECTED_END		= "Unexpected end of stream.";

	protected static final String	FAULT_MALFORMED_HEADERFIELD	= "Malformed MIME header field.";

	protected static final String	FAULT_NOT_FINISHED			= "Previous part not finished.";

	/**
	 * Gets estimated Content-Type via filename.
	 * 
	 * @param filename fileName with extension.
	 * @return Content-Type estimated Content-Type based on the file extension.
	 */
	public static ContentType estimateContentType(String filename) {
		int last = 0;
		last = filename.lastIndexOf('.');

		String fileExt = filename.substring(last + 1);
		return extensionContentType(fileExt);
	}

	/**
	 * Returns a file extension that is most likely with given Content-Type
	 * 
	 * @param mimeType MIME Type
	 * @return File extension (e.g. "png"), null if Content- Type is unknown
	 */
	public static String contentToFileExtension(String mimeType) {
		int slashPos = mimeType.indexOf(MIMEConstants.SEPARATOR);
		if ((slashPos < 1) || (slashPos == (mimeType.length() - 1))) {
			return null;
		}

		String type = mimeType.substring(0, slashPos);
		String subtype;
		int semicolonPos = mimeType.indexOf(";", slashPos + 1);
		if (semicolonPos < 0) {
			subtype = mimeType.substring(slashPos + 1).trim();
		} else {
			subtype = mimeType.substring(slashPos + 1, semicolonPos).trim();
		}

		return contentToFileExtension(type, subtype);
	}

	/**
	 * Returns a file extension that is most likely with given Content-Type
	 * 
	 * @param mediatype
	 * @param subtype
	 * @return File extension (e.g. "png"), null if Content- Type is unknown
	 */
	public static String contentToFileExtension(String mediatype, String subtype) {
		if (StringUtil.equalsIgnoreCase(mediatype, MIMEConstants.MEDIATYPE_IMAGE)) {
			if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_GIF)) {
				return "gif";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_JPEG)) {
				return "jpg";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_PNG)) {
				return "png";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_TIFF)) {
				return "tiff";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_ICON)) {
				return "ico";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_SVG)) {
				return "svg";
			}
		} else if (StringUtil.equalsIgnoreCase(mediatype, MIMEConstants.MEDIATYPE_TEXT)) {
			if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_CSS)) {
				return "css";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_HTML)) {
				return "html";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_JAVASCRIPT)) {
				return "js";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_PLAIN)) {
				return "txt";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_RICHTEXT)) {
				return "rtf";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_SOAPXML)) {
				return "xml";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_CSV)) {
				return "csv";
			}

		} else if (StringUtil.equalsIgnoreCase(mediatype, MIMEConstants.MEDIATYPE_APPLICATION)) {
			if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_MSEXCEL)) {
				return "xls";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_MSWORD)) {
				return "doc";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_RAR)) {
				return "rar";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_PDF)) {
				return "pdf";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_SHOCKWAVEFLASH)) {
				return "swf";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_WINDOWSEXECUTEABLE)) {
				return "exe";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_ZIP)) {
				return "zip";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_POSTSCRIPT)) {
				return "ps";
			}
		} else if (StringUtil.equalsIgnoreCase(mediatype, MIMEConstants.MEDIATYPE_VIDEO)) {
			if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_WINDOWSMEDIA)) {
				return "wmv";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_AVI)) {
				return "avi";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_MP4)) {
				return "mp4";
			}
		} else if (StringUtil.equalsIgnoreCase(mediatype, MIMEConstants.MEDIATYPE_AUDIO)) {
			if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_MPEG3) || StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_MPEG)) {
				return "mp3";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_WINDOWSAUDIO)) {
				return "wma";
			} else if (StringUtil.equalsIgnoreCase(subtype, MIMEConstants.SUBTYPE_MP4)) {
				return "mp4";
			}
		}

		return null;
	}

	/**
	 * Gets Content-Type via file extension.
	 * 
	 * @param fileExt file extension.
	 * @return Content-Type (type and subtype).
	 */
	public static ContentType extensionContentType(String fileExt) {

		fileExt = fileExt.toLowerCase();

		if ("tiff".equals(fileExt) || "tif".equals(fileExt)) {
			// return MIMEConstants.
		} else if ("zip".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_ZIP;
		} else if ("pdf".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_PDF;
		} else if ("wmv".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_VIDEO_WMV;
		} else if ("rar".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_RAR;
		} else if ("swf".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_SWF;
		} else if ("exe".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_WINDOWSEXEC;
		} else if ("avi".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_VIDEO_AVI;
		} else if ("doc".equals(fileExt) || "dot".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_WORD;
		} else if ("ico".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_IMAGE_ICO;
		} else if ("mp2".equals(fileExt) || "mp3".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_AUDIO_MPEG;
		} else if ("rtf".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_TEXT_RTF;
		} else if ("xls".equals(fileExt) || "xla".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_EXCEL;
		} else if ("jpg".equals(fileExt) || "jpeg".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_IMAGE_JPEG;
		} else if ("gif".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_IMAGE_GIF;
		} else if ("svg".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_IMAGE_SVG;
		} else if ("png".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_IMAGE_PNG;
		} else if ("csv".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_TEXT_CSV;
		} else if ("ps".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_POSTSCRIPT;
		} else if ("html".equals(fileExt) || "htm".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_TEXT_HTML;
		} else if ("css".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_TEXT_CSS;
		} else if ("xml".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_TEXT_XML;
		} else if ("js".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_APPLICATION_JAVASCRIPT;
		} else if ("wma".equals(fileExt)) {
			return MIMEConstants.CONTENT_TYPE_AUDIO_WMA;
		}

		return MIMEConstants.CONTENT_TYPE_TEXT_PLAIN;
	}

	/**
	 * Returns the complete media type.
	 * 
	 * @return the media type, e.g.\ "application/xml" for media type
	 *         "application/xml"
	 */
	public static String getMimeType(ContentType contentType) {
		return contentType.getType() + MIMEConstants.SEPARATOR + contentType.getSubtype();
	}

	/**
	 * Returns a string representation of this media type.
	 */
	public static String getMimeTypeWithParameters(ContentType contentType) {
		SimpleStringBuilder retval = Toolkit.getInstance().createSimpleStringBuilder();
		retval.append(contentType.getType());
		retval.append(MIMEConstants.SEPARATOR);
		retval.append(contentType.getSubtype());

		Iterator iter = contentType.getParameterEntries();

		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			retval.append(";").append(entry.getKey()).append('=').append(entry.getValue());
		}

		return retval.toString();
	}

	/**
	 * Create a new media type object from the information given.
	 * 
	 * @param mimeType the string describing the media type.
	 */
	public static ContentType createContentType(String mimeType) {
		if (mimeType == null) {
			return MIMEConstants.CONTENT_TYPE_NOT_SET;
		}
		int slashPos = mimeType.indexOf(MIMEConstants.SEPARATOR);
		if ((slashPos < 1) || (slashPos == (mimeType.length() - 1))) {
			return new ContentType(null, null, contentToFileExtension(mimeType), null);
		}

		String type = mimeType.substring(0, slashPos);
		String subtype;
		int semicolonPos = mimeType.indexOf(";", slashPos + 1);
		if (semicolonPos < 0) {
			subtype = mimeType.substring(slashPos + 1).trim();
		} else {
			subtype = mimeType.substring(slashPos + 1, semicolonPos).trim();
		}

		ContentType result = new ContentType(type, subtype, contentToFileExtension(mimeType));

		String tmp;
		int quotePosStart;
		int quotePosEnd;
		while (semicolonPos > 0) {
			int nextSemicolonPos = mimeType.indexOf(";", semicolonPos + 1);
			if (nextSemicolonPos > 0) {

				tmp = mimeType.substring(semicolonPos + 1, nextSemicolonPos);

				quotePosStart = checkQuotation(tmp);
				if (quotePosStart > 0) {
					quotePosEnd = searchQuotationEnd(mimeType.substring(semicolonPos + 1), quotePosStart + 1) + semicolonPos + 2;
					addParameter(mimeType.substring(semicolonPos + 1, quotePosEnd), result);
					semicolonPos = mimeType.indexOf(";", quotePosEnd);
				} else {
					addParameter(tmp, result);
					semicolonPos = nextSemicolonPos;
				}

			} else {
				addParameter(mimeType.substring(semicolonPos + 1), result);
				semicolonPos = -1;
			}
		}

		return result;
	}

	/**
	 * Adds a parameter with its value to the parameter table.
	 * 
	 * @param parameter the parameter to add. In the form attribute=value.
	 * @return <code>true</code> if the given parameter was well-formed, <code>false</code> otherwise.
	 */
	private static boolean addParameter(String parameter, ContentType contentType) {
		int equalsPos = parameter.indexOf("=");
		if ((equalsPos < 1) || (equalsPos == parameter.length() - 1)) {
			return false;
		}
		contentType.addParameter(parameter.substring(0, equalsPos), parameter.substring(equalsPos + 1));
		return true;
	}

	/**
	 * Checks if value of the parameter in the string is quoted.
	 * 
	 * @param s string to be checked.
	 * @return position of the beginning quotation mark of the parameter value.
	 *         Returns 0 if the value isn't quoted.
	 */
	private static int checkQuotation(String s) {
		int equalsPos = s.indexOf("=");
		int quotePos = s.indexOf("\"");
		return (equalsPos + 1 == quotePos ? quotePos : 0);
	}

	/**
	 * Searches closing quotation mark in String.
	 * 
	 * @param s string to search in.
	 * @param index start position of search in string.
	 * @return position of the closing quotation mark of the parameter value.
	 */
	private static int searchQuotationEnd(String s, int index) {
		int quotePos = s.indexOf("\"", index);

		while (s.charAt(quotePos - 1) == '\\') {
			int even = 1;
			for (int i = quotePos - 2; s.charAt(i) == '\\'; i--)
				even++;
			if (even % 2 == 0) return quotePos;

			quotePos = s.indexOf("\"", quotePos + 1);
		}

		return quotePos;
	}

}
