/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.filter;

import java.io.IOException;

import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.types.Memento;

/**
 * Filter class for a range of {@link IPAddress}.
 */
public class IPFilterRange extends IPFilter {

	private static final String	KEY_LOWERBOUND	= "lower";

	private static final String	KEY_UPPERBOUND	= "upper";

	private IPAddress			lowerbound;

	private IPAddress			upperbound;

	protected IPFilterRange() {
		// Memento
	}

	/**
	 * @param ipAddress
	 * @param type
	 * @param enable
	 */
	public IPFilterRange(IPAddress lowerbound, IPAddress upperbound, boolean allow, boolean inverted) {
		super(allow, inverted);
		if (lowerbound.isIPv6() != upperbound.isIPv6()) {
			throw new IllegalArgumentException("Different address types for lowerbound and upperbound are not allowed");
		}
		this.lowerbound = lowerbound;
		this.upperbound = upperbound;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.configuration.IPFilter#check(org.yads.java.communication
	 * .connection.ip.IPAddress)
	 */
	@Override
	public boolean check(Object[] key) {
		try {
			Long[] tmpKey = (Long[]) key;

			if ((tmpKey[0] == null) != (lowerbound.getKey()[0] == null)) {
				return false;
			}
			return calculateInversion(IPAddress.isAddressInRange(lowerbound.getKey(), upperbound.getKey(), tmpKey));
		} catch (ClassCastException e) {
			return false;
		}
	}

	@Override
	protected String getInfo() {
		return "range from: " + this.lowerbound + " to: " + this.upperbound;
	}

	@Override
	public int getType() {
		return IPFilter.FILTER_TYPE_ADDRESS_RANGE;
	}

	/**
	 * @return the lowerbound
	 */
	public IPAddress getLowerbound() {
		return lowerbound;
	}

	/**
	 * @return the upperbound
	 */
	public IPAddress getUpperbound() {
		return upperbound;
	}

	@Override
	public void readFromMemento(Memento m) throws IOException {
		super.readFromMemento(m);

		if (m.containsKey(KEY_LOWERBOUND)) {
			lowerbound = IPAddress.createRemoteIPAddress(m.getStringValue(KEY_LOWERBOUND));
		}
		if (m.containsKey(KEY_UPPERBOUND)) {
			upperbound = IPAddress.createRemoteIPAddress(m.getStringValue(KEY_UPPERBOUND));
		}
	}

	@Override
	public void saveToMemento(Memento m) {
		super.saveToMemento(m);

		if (lowerbound != null) {
			m.putValue(KEY_LOWERBOUND, lowerbound.getAddress());
		}

		if (upperbound != null) {
			m.putValue(KEY_UPPERBOUND, upperbound.getAddress());
		}
	}
}
