/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.filter;

import java.io.IOException;

import org.yads.java.communication.connection.ip.IPNetworkDetection;
import org.yads.java.types.Memento;

public class IPFilterOwnAddresses extends IPFilter {

	protected IPFilterOwnAddresses() {
		// Memento
	}

	public IPFilterOwnAddresses(boolean allow, boolean inverted) {
		super(allow, inverted);
	}

	@Override
	public boolean check(Object[] key) {
		try {
			Long[] tmpKey = (Long[]) key;
			return calculateInversion(IPNetworkDetection.getInstance().hasIPAddress(tmpKey));
		} catch (ClassCastException e) {
			return false;
		}
	}

	@Override
	protected String getInfo() {
		return "OWN";
	}

	@Override
	public int getType() {
		return IPFilter.FILTER_TYPE_OWN_ADDRESSES;
	}

	@Override
	public void readFromMemento(Memento m) throws IOException {
		super.readFromMemento(m);

		// Nothing to do...
	}

	@Override
	public void saveToMemento(Memento m) {
		super.saveToMemento(m);

		// Nothing to do...
	}
}
