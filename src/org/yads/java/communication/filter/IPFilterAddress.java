/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.filter;

import java.io.IOException;

import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.types.Memento;

/**
 * Filter class for single {@link IPAddress}.
 */
public class IPFilterAddress extends IPFilter {

	private static final String	KEY_IPADDRESS	= "ipaddress";

	private IPAddress			ipAddress;

	protected IPFilterAddress() {
		super();
	}

	/**
	 * @param enable
	 */
	public IPFilterAddress(IPAddress ipAddress, boolean allow, boolean inverted) {
		super(allow, inverted);
		this.ipAddress = ipAddress;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.configuration.IPFilter#check(org.yads.java.communication
	 * .connection.ip.IPAddress)
	 */
	@Override
	public boolean check(Object[] key) {
		try {
			Long[] tmpKey = (Long[]) key;
			Long[] ip = ipAddress.getKey();

			if (tmpKey[0] == null && ip[0] == null) {
				if (tmpKey[1].longValue() == ip[1].longValue()) {
					return calculateInversion(true);
				}
			}

			return calculateInversion(false);
		} catch (ClassCastException e) {
			return false;
		}
	}

	@Override
	protected String getInfo() {
		return "address: " + this.ipAddress;
	}

	@Override
	public int getType() {
		return IPFilter.FILTER_TYPE_ADDRESS;
	}

	public IPAddress getIpAddress() {
		return this.ipAddress;
	}

	@Override
	public void readFromMemento(Memento m) throws IOException {
		super.readFromMemento(m);

		if (m.containsKey(KEY_IPADDRESS)) {
			ipAddress = IPAddress.createRemoteIPAddress(m.getStringValue(KEY_IPADDRESS));
			// ipAddress =
			// IPNetworkDetection.getInstance().getIPAddressOfAnyLocalInterface(m.getStringValue(KEY_IPADDRESS),
			// false);
		}
	}

	@Override
	public void saveToMemento(Memento m) {
		super.saveToMemento(m);

		if (ipAddress != null) {
			m.putValue(KEY_IPADDRESS, ipAddress.getAddress());
		}
	}
}
