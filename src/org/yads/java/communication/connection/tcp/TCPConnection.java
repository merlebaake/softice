/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.connection.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.yads.java.YADSFramework;
import org.yads.java.communication.connection.ip.IPConnectionInfo;

import org.apache.http.nio.protocol.HttpAsyncExchange;

/**
 * TCP connection.
 * <p>
 * This class represents an existing TCP connection based on a socket implementation. The socket implementation is stored inside this class to allow to correctly close the connection.
 * </p>
 */
public class TCPConnection {

	private final InputStream		in;

	private final OutputStream		out;

	private final IPConnectionInfo	connectionInfo;

	private boolean					closed			= false;

	private boolean					fstRead			= true;

	private boolean					fstWrite		= true;

	private ConnectionCloseListener	closeListener	= null;
	
	private HttpAsyncExchange httpExchange;

	public TCPConnection(InputStream in, OutputStream out, IPConnectionInfo ci, ConnectionCloseListener closeListner) {
		this.in = in;
		this.out = out;
		this.connectionInfo = ci;
		this.closeListener = closeListner;
	}
	
	public HttpAsyncExchange getHttpExchange() {
		return httpExchange;
	}
	
	public void setHttpExchange(HttpAsyncExchange httpExchange) {
		this.httpExchange = httpExchange;
	}

	/**
	 * Returns the input stream for this connection.
	 * 
	 * @return input stream for this connection.
	 */
	public InputStream getInputStream() {
        return in;
	}

	/**
	 * Returns the output stream for this connection.
	 * 
	 * @return output stream for this connection.
	 */
	public OutputStream getOutputStream() {
		return out;
	}

	/**
	 * Returns the transport/addressing information belonging to this TCP
	 * connection. This includes the unique connection ID and the source and
	 * destination addresses and ports.
	 * 
	 * @return the addressing information belonging to this connection
	 */
	public IPConnectionInfo getConnectionInfo() {
		return connectionInfo;
	}

	/**
	 * Closes this connection.
	 * <p>
	 * This will close the input and output stream and the socket.
	 * </p>
	 * 
	 * @throws IOException
	 */
	public void close() throws IOException {
		if (closed) {
			return;
		}
		if (!YADSFramework.isKillRunning()) {
            if (out != null)
                out.flush();
		}
        if (in != null)
            in.close();
        if (out != null)
            out.close();
		closed = true;
		if (closeListener != null) {
			closeListener.connectionClosed(this);
		}
	}

	/**
	 * Returns the identifier for this connection.
	 * 
	 * @return identifier for this connection.
	 */
	public Long getIdentifier() {
		return connectionInfo.getConnectionId();
	}

	@Override
	public String toString() {
		if (connectionInfo != null) {
			return "TCP Connection [ id = " + connectionInfo.getConnectionId() + " ]";
		} else {
			return "TCP Connection";
		}
	}

	synchronized boolean isFirstRead() {
		return fstRead;
	}

	synchronized boolean isFirstWrite() {
		return fstWrite;
	}

	synchronized void firstRead() {
		fstRead = false;
	}

	synchronized void firstWrite() {
		fstWrite = false;
	}

	/**
	 * Returns <code>true</code> if the connection is closed, <code>false</code> otherwise.
	 * 
	 * @return <code>true</code> if the connection is closed, <code>false</code> otherwise.
	 */
	public boolean isClosed() {
		return closed;
	}

}
