/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.connection.udp;

import java.io.IOException;

import org.yads.java.util.TimedEntry;
import org.yads.java.util.WatchDog;

public class DatagramSocketTimer extends TimedEntry {

	final Object			syncObject;

	public long				timeout;

	public DatagramSocket	datagramSocket;

	public DatagramSocketTimer(DatagramSocket datagramSocket, long timeout, Object syncObject) {
		this.datagramSocket = datagramSocket;
		this.timeout = timeout;
		this.syncObject = syncObject;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.util.TimedEntry#timedOut()
	 */
	@Override
	public void timedOut() {
		synchronized (syncObject) {
			if (datagramSocket != null) {
				try {
					datagramSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				datagramSocket = null;
			}
		}
	}

	public void update() {
		WatchDog.getInstance().update(this, timeout);
	}

}