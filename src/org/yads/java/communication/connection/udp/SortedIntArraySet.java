/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.connection.udp;

import java.util.Arrays;

public class SortedIntArraySet {

	private static final int	DEFAULT_INITIAL_CAPACITY	= 10;

	private int					initialCapacity;

	final int					capacityIncrement;

	private int					capacityIncrementLimit		= 1000;

	private int					size						= 0;

	private int[]				elements;

	public SortedIntArraySet() {
		this(DEFAULT_INITIAL_CAPACITY);
	}

	public SortedIntArraySet(final int initialCapacity) {
		this(initialCapacity, 0);
	}

	public SortedIntArraySet(final int initialCapacity, final int capacityIncrement) {
		this.initialCapacity = initialCapacity;
		this.elements = new int[initialCapacity];
		this.capacityIncrement = capacityIncrement;
		if (capacityIncrementLimit < capacityIncrement) {
			capacityIncrementLimit = capacityIncrement;
		}
		Arrays.fill(elements, 0, initialCapacity, Integer.MAX_VALUE);
	}

	public void add(int value) {
		int index = Arrays.binarySearch(elements, value);
		if (index >= 0 && index < size) {
			return;
		}

		index = -1 * (index + 1);
		size++;
		if (size > elements.length) {
			int[] oldElements = elements;
			int newCapacity = calculateNewCapacity(elements.length);
			elements = new int[newCapacity];

			if (index > 0) {
				System.arraycopy(oldElements, 0, elements, 0, index);
			}
			elements[index] = value;
			if (index < size - 1) {
				System.arraycopy(oldElements, index, elements, index + 1, size - index - 1);
			}
			Arrays.fill(elements, size, newCapacity, Integer.MAX_VALUE);
		} else {
			if (index < size - 1) {
				System.arraycopy(elements, index, elements, index + 1, size - index - 1);
			}
			elements[index] = value;
		}
	}

	private int calculateNewCapacity(int currentLength) {
		int newCapacity = (capacityIncrement <= 0 ? ((currentLength << 1) + 1) : (currentLength + capacityIncrement));
		if (newCapacity - currentLength > capacityIncrementLimit) {
			newCapacity = currentLength + capacityIncrementLimit;
		}

		if (newCapacity < size) {
			newCapacity = size;
		}
		return newCapacity;
	}

	public void remove(int value) {
		int index = Arrays.binarySearch(elements, value);
		if (index < 0 || index >= size) {
			return;
		}

		size--;
		int newCapacity = calculateNewCapacity(size);
		if (elements.length > initialCapacity && elements.length / 2 > newCapacity) {
			if (newCapacity < initialCapacity) {
				newCapacity = initialCapacity;
			}
			int[] oldElements = elements;
			elements = new int[newCapacity];

			if (index > 0) {
				System.arraycopy(oldElements, 0, elements, 0, index);
			}
			if (index < size) {
				System.arraycopy(oldElements, index + 1, elements, index, size - index);
			}
			Arrays.fill(elements, size, newCapacity, Integer.MAX_VALUE);
		} else {
			if (index < size) {
				System.arraycopy(elements, index + 1, elements, index, size - index);
				elements[size] = Integer.MAX_VALUE;
			} else {
				elements[index] = Integer.MAX_VALUE;
			}
		}
	}

	public boolean contains(int value) {
		int index = Arrays.binarySearch(elements, value);
		return (index >= 0 && index < size);
	}

	public int getSize() {
		return size;
	}

}
