/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication;

import org.yads.java.constants.general.DPWSConstants;
import org.yads.java.types.URI;

public class DPWSMetadataValidator extends MetadataValidator {

	private String checkMaxFieldSize(String field) {
		if (field.length() < DPWSConstants.DPWS_MAX_FIELD_SIZE) {
			return null;
		}
		return "DPWS MAX_FIELD_SIZE exceedet";
	}

	private String checkMaxUriSize(URI uri) {
		if (uri.getOctetLength() < DPWSConstants.DPWS_MAX_URI_SIZE) {
			return null;
		}
		return "DPWS MAX_URI_SIZE exceedet";
	}

	@Override
	public String checkManufacturer(String manufacturer) {
		return checkMaxFieldSize(manufacturer);
	}

	@Override
	public String checkModelName(String modelName) {
		return checkMaxFieldSize(modelName);
	}

	@Override
	public String checkModelNumber(String modelNumber) {
		return checkMaxFieldSize(modelNumber);
	}

	@Override
	public String checkFriendlyName(String friendlyName) {
		return checkMaxFieldSize(friendlyName);
	}

	@Override
	public String checkFirmwareVersion(String firmwareVersion) {
		return checkMaxFieldSize(firmwareVersion);
	}

	@Override
	public String checkSerialNumber(String serialNumber) {
		return checkMaxFieldSize(serialNumber);
	}

	@Override
	public String checkManufacturerUrl(URI manufacturerUrl) {
		return checkMaxUriSize(manufacturerUrl);
	}

	@Override
	public String checkModelUrl(URI modelUrl) {
		return checkMaxUriSize(modelUrl);
	}

	@Override
	public String checkPresentationUrl(URI presentationUrl) {
		return checkMaxUriSize(presentationUrl);
	}

}
