/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.receiver;

import org.yads.java.communication.CommunicationException;
import org.yads.java.communication.CommunicationManagerRegistry;
import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.connection.ip.IPConnectionInfo;
import org.yads.java.communication.connection.ip.IPDiscoveryDomain;
import org.yads.java.communication.structures.IPDiscoveryBinding;
import org.yads.java.constants.MessageConstants;
import org.yads.java.constants.general.DPWSConstantsHelper;
import org.yads.java.dispatch.DeviceServiceRegistry;
import org.yads.java.dispatch.MessageInformer;
import org.yads.java.dispatch.RequestResponseCoordinator;
import org.yads.java.dispatch.ResponseHandler;
import org.yads.java.message.FaultMessage;
import org.yads.java.message.InvokeMessage;
import org.yads.java.message.Message;
import org.yads.java.message.discovery.ByeMessage;
import org.yads.java.message.discovery.HelloMessage;
import org.yads.java.message.discovery.ProbeMatch;
import org.yads.java.message.discovery.ProbeMatchesMessage;
import org.yads.java.message.discovery.ProbeMessage;
import org.yads.java.message.discovery.ResolveMatchesMessage;
import org.yads.java.message.discovery.ResolveMessage;
import org.yads.java.message.eventing.GetStatusMessage;
import org.yads.java.message.eventing.GetStatusResponseMessage;
import org.yads.java.message.eventing.RenewMessage;
import org.yads.java.message.eventing.RenewResponseMessage;
import org.yads.java.message.eventing.SubscribeMessage;
import org.yads.java.message.eventing.SubscribeResponseMessage;
import org.yads.java.message.eventing.SubscriptionEndMessage;
import org.yads.java.message.eventing.UnsubscribeMessage;
import org.yads.java.message.eventing.UnsubscribeResponseMessage;
import org.yads.java.message.metadata.GetMessage;
import org.yads.java.message.metadata.GetMetadataMessage;
import org.yads.java.message.metadata.GetMetadataResponseMessage;
import org.yads.java.message.metadata.GetResponseMessage;
import org.yads.java.security.CredentialInfo;
import org.yads.java.security.SecurityKey;
import org.yads.java.service.Device;
import org.yads.java.service.OperationDescription;
import org.yads.java.service.reference.DeviceReference;
import org.yads.java.service.reference.ServiceReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.HelloData;
import org.yads.java.types.QNameSet;
import org.yads.java.util.Log;

/**
 * 
 */
public class UDPResponseReceiver implements MessageReceiver {

	private static final MessageInformer		MESSAGE_INFORMER	= MessageInformer.getInstance();

	private final RequestResponseCoordinator	rrc;

	/**
	 * @param rrc
	 */
	public UDPResponseReceiver(RequestResponseCoordinator rrc) {
		super();
		this.rrc = rrc;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.HelloMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(HelloMessage hello, ConnectionInfo connectionInfo) {
		IncomingSOAPReceiver.markIncoming(hello);

		if (hello.getTypes() != null) {
			DPWSConstantsHelper helper = DPWSCommunicationManager.getHelper(connectionInfo.getProtocolInfo().getVersion());

			if (hello.getTypes().contains(helper.getWSDDiscoveryProxyType())) {
				DPWSCommunicationManager comMan = (DPWSCommunicationManager) CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
				IPDiscoveryBinding binding = (IPDiscoveryBinding) comMan.getDiscoveryBinding(connectionInfo);
				if (binding != null) {
					IPDiscoveryDomain domain = (IPDiscoveryDomain) binding.getDiscoveryDomain();
					HelloData helloData = new HelloData(hello, connectionInfo);

					CredentialInfo credentialInfo = connectionInfo.getLocalCredentialInfo();

					HashSet outgoingDiscoveryInfos = new HashSet();
					outgoingDiscoveryInfos.add(comMan.getOutgoingDiscoveryInfo(binding, false, credentialInfo));
					SecurityKey key = new SecurityKey(outgoingDiscoveryInfos, credentialInfo);
					DeviceReference devRef = DeviceServiceRegistry.getDeviceReference(helloData, key, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

					devRef.addListener(credentialInfo);
					credentialInfo.addDiscoveryProxyForDomain(domain, devRef);
					if (Log.isDebug()) {
						Log.debug("Discovery Proxy added for domain: " + domain);
					}
				} else {
					if (Log.isDebug()) {
						Log.debug("No Domain found for Discovery Proxy (interface: " + ((IPConnectionInfo) connectionInfo).getIface() + ")");
					}
				}
				return;
			}
		}

		receiveUnexpectedMessage(hello, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ByeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ByeMessage bye, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(bye, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ProbeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ProbeMessage probe, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(probe, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ProbeMatchesMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ProbeMatchesMessage probeMatches, ConnectionInfo connectionInfo) {
		IncomingSOAPReceiver.markIncoming(probeMatches);

		ResponseHandler handler = rrc.getResponseHandlerAndUpdateConnectionInfo(probeMatches, connectionInfo);
		if (handler != null) {
			ProbeMessage probe = (ProbeMessage) handler.getRequestMessage();

			if (probe.getSearchType() == ProbeMessage.SEARCH_TYPE_SERVICE) {

				List matches = probeMatches.getProbeMatches();
				if (matches != null && !matches.isEmpty()) {

					Iterator it = matches.iterator();
					while (it.hasNext()) {
						ProbeMatch match = (ProbeMatch) it.next();
						DeviceReference devRef = DeviceServiceRegistry.getUpdatedDeviceReference(match, new SecurityKey(probe.getOutgoingDiscoveryInfos(), connectionInfo.getLocalCredentialInfo()), probeMatches, connectionInfo);

						try {
							Device device = devRef.getDevice();
							QNameSet serviceTypes = probe.getServiceTypes();
							boolean noneFound = true;
							for (Iterator it_servRef = device.getServiceReferences(devRef.getSecurityKey()); it_servRef.hasNext();) {
								ServiceReference servRef = (ServiceReference) it_servRef.next();
								if (servRef.containsAllPortTypes(serviceTypes)) {
									noneFound = false;
									break;
								}
							}
							if (noneFound) {
								it.remove();
							}
						} catch (CommunicationException e) {
							Log.printStackTrace(e);
						}

					}
				}
				if (matches.isEmpty()) {
					/* if all matches are deleted by this action */
					return;
				}
			}

			String keyId = null;

			DPWSConstantsHelper helper = DPWSCommunicationManager.getHelper(connectionInfo.getProtocolInfo().getVersion());
			if (probe.getTo() != null && probe.getTo() != helper.getWSDTo()) {
				// for discovery proxy -> field "to" is epr of discovery
				// proxy
				keyId = new EndpointReference(probe.getTo()).getAddress().toString();
			} else if (probeMatches.getProbeMatchCount() > 0) {
				// for multicast use
				keyId = probeMatches.getProbeMatch(0).getEndpointReference().getAddress().toString();
			} else {
				if (Log.isWarn()) {
					Log.warn("Empty probeMatches received, but not from a discovery proxy.");
				}
				return;
			}

            handler.handle(probeMatches, connectionInfo);
            MESSAGE_INFORMER.forwardMessage(probeMatches, connectionInfo, null);
			
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ResolveMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ResolveMessage resolve, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(resolve, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ResolveMatchesMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ResolveMatchesMessage resolveMatches, ConnectionInfo connectionInfo) {
		IncomingSOAPReceiver.markIncoming(resolveMatches);

		ResponseHandler handler = rrc.getResponseHandlerAndUpdateConnectionInfo(resolveMatches, connectionInfo);
		if (handler != null) {
			handler.handle(resolveMatches, connectionInfo);
			MESSAGE_INFORMER.forwardMessage(resolveMatches, connectionInfo, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.metadata.GetMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetMessage get, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(get, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.metadata.GetResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetResponseMessage getResponse, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(getResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.metadata.GetMetadataMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetMetadataMessage getMetadata, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(getMetadata, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.metadata. GetMetadataResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetMetadataResponseMessage getMetadataResponse, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(getMetadataResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.SubscribeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(SubscribeMessage subscribe, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(subscribe, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.SubscribeResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(SubscribeResponseMessage subscribeResponse, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(subscribeResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.GetStatusMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetStatusMessage getStatus, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(getStatus, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.GetStatusResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetStatusResponseMessage getStatusResponse, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(getStatusResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.RenewMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(RenewMessage renew, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(renew, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.RenewResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(RenewResponseMessage renewResponse, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(renewResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.UnsubscribeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(UnsubscribeMessage unsubscribe, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(unsubscribe, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.UnsubscribeResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(UnsubscribeResponseMessage unsubscribeResponse, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(unsubscribeResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.SubscriptionEndMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(SubscriptionEndMessage subscriptionEnd, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(subscriptionEnd, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.invocation.InvokeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(InvokeMessage invoke, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(invoke, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.FaultMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(FaultMessage fault, ConnectionInfo connectionInfo) {
		receiveUnexpectedMessage(fault, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.protocol.soap.generator.MessageReceiver#
	 * receiveFailed(java.lang.Exception,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receiveFailed(Exception e, ConnectionInfo connectionInfo) {
        if (Log.isDebug())
            Log.error("Unable to receive SOAP-over-UDP response from " + connectionInfo.getSourceAddress() + " (" + e.getMessage() + ")");
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.protocol.soap.generator.MessageReceiver#
	 * sendFailed(java.lang.Exception, org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void sendFailed(Exception e, ConnectionInfo connectionInfo) {
		// void
	}

	private void receiveUnexpectedMessage(Message message, ConnectionInfo connectionInfo) {
		IncomingSOAPReceiver.markIncoming(message);

		String actionName = MessageConstants.getMessageNameForType(message.getType());

		if (Log.isWarn()) {
			Log.warn("<I> Unexpected unicast SOAP-over-UDP response message from " + connectionInfo.getSourceAddress() + ": " + ((actionName != null) ? actionName.toString() : "NO ACTION IN HEADER"));
		}
		if (Log.isDebug()) {
			Log.error(message.toString());
		}
		MESSAGE_INFORMER.forwardMessage(message, connectionInfo, null);
	}

	@Override
	public OperationDescription getOperation(String action) {
		return null;
	}

	@Override
	public void receiveNoContent(String reason, ConnectionInfo connectionInfo) {}

	@Override
	public OperationDescription getEventSource(String action) {
		return null;
	}

	@Override
	public int getRequestMessageType() {
		return MessageConstants.UNKNOWN_MESSAGE;
	}
}
