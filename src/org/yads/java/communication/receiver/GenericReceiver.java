/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.receiver;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.constants.MessageConstants;
import org.yads.java.dispatch.MessageInformer;
import org.yads.java.message.FaultMessage;
import org.yads.java.message.InvokeMessage;
import org.yads.java.message.Message;
import org.yads.java.message.discovery.ByeMessage;
import org.yads.java.message.discovery.HelloMessage;
import org.yads.java.message.discovery.ProbeMatchesMessage;
import org.yads.java.message.discovery.ProbeMessage;
import org.yads.java.message.discovery.ResolveMatchesMessage;
import org.yads.java.message.discovery.ResolveMessage;
import org.yads.java.message.eventing.GetStatusMessage;
import org.yads.java.message.eventing.GetStatusResponseMessage;
import org.yads.java.message.eventing.RenewMessage;
import org.yads.java.message.eventing.RenewResponseMessage;
import org.yads.java.message.eventing.SubscribeMessage;
import org.yads.java.message.eventing.SubscribeResponseMessage;
import org.yads.java.message.eventing.SubscriptionEndMessage;
import org.yads.java.message.eventing.UnsubscribeMessage;
import org.yads.java.message.eventing.UnsubscribeResponseMessage;
import org.yads.java.message.metadata.GetMessage;
import org.yads.java.message.metadata.GetMetadataMessage;
import org.yads.java.message.metadata.GetMetadataResponseMessage;
import org.yads.java.message.metadata.GetResponseMessage;
import org.yads.java.service.OperationDescription;

/**
 *
 */
public class GenericReceiver implements MessageReceiver {

	private static final MessageInformer	MESSAGE_INFORMER	= MessageInformer.getInstance();

	/**
	 * 
	 */
	public GenericReceiver() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.HelloMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(HelloMessage hello, ConnectionInfo connectionInfo) {
		receiveGeneric(hello, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ByeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ByeMessage bye, ConnectionInfo connectionInfo) {
		receiveGeneric(bye, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ProbeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ProbeMessage probe, ConnectionInfo connectionInfo) {
		receiveGeneric(probe, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ProbeMatchesMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ProbeMatchesMessage probeMatches, ConnectionInfo connectionInfo) {
		receiveGeneric(probeMatches, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ResolveMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ResolveMessage resolve, ConnectionInfo connectionInfo) {
		receiveGeneric(resolve, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.discovery.ResolveMatchesMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(ResolveMatchesMessage resolveMatches, ConnectionInfo connectionInfo) {
		receiveGeneric(resolveMatches, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.metadata.GetMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetMessage get, ConnectionInfo connectionInfo) {
		receiveGeneric(get, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.metadata.GetResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetResponseMessage getResponse, ConnectionInfo connectionInfo) {
		receiveGeneric(getResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.metadata.GetMetadataMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetMetadataMessage getMetadata, ConnectionInfo connectionInfo) {
		receiveGeneric(getMetadata, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.metadata. GetMetadataResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetMetadataResponseMessage getMetadataResponse, ConnectionInfo connectionInfo) {
		receiveGeneric(getMetadataResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.SubscribeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(SubscribeMessage subscribe, ConnectionInfo connectionInfo) {
		receiveGeneric(subscribe, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.SubscribeResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(SubscribeResponseMessage subscribeResponse, ConnectionInfo connectionInfo) {
		receiveGeneric(subscribeResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.GetStatusMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetStatusMessage getStatus, ConnectionInfo connectionInfo) {
		receiveGeneric(getStatus, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.GetStatusResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(GetStatusResponseMessage getStatusResponse, ConnectionInfo connectionInfo) {
		receiveGeneric(getStatusResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.RenewMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(RenewMessage renew, ConnectionInfo connectionInfo) {
		receiveGeneric(renew, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.RenewResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(RenewResponseMessage renewResponse, ConnectionInfo connectionInfo) {
		receiveGeneric(renewResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.UnsubscribeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(UnsubscribeMessage unsubscribe, ConnectionInfo connectionInfo) {
		receiveGeneric(unsubscribe, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.UnsubscribeResponseMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(UnsubscribeResponseMessage unsubscribeResponse, ConnectionInfo connectionInfo) {
		receiveGeneric(unsubscribeResponse, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.eventing.SubscriptionEndMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(SubscriptionEndMessage subscriptionEnd, ConnectionInfo connectionInfo) {
		receiveGeneric(subscriptionEnd, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.invocation.InvokeMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(InvokeMessage invoke, ConnectionInfo connectionInfo) {
		receiveGeneric(invoke, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.communication.protocol.soap.generator.MessageReceiver#receive
	 * (org.yads.java.message.FaultMessage,
	 * org.yads.java.communication.ProtocolData)
	 */
	@Override
	public void receive(FaultMessage fault, ConnectionInfo connectionInfo) {
		receiveGeneric(fault, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.protocol.soap.generator.MessageReceiver#
	 * receiveFailed(java.lang.Exception)
	 */
	@Override
	public void receiveFailed(Exception e, ConnectionInfo connectionInfo) {
		// ignore, no one cares about it
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.protocol.soap.generator.MessageReceiver#
	 * sendFailed(java.lang.Exception)
	 */
	@Override
	public void sendFailed(Exception e, ConnectionInfo connectionInfo) {
		// ignore, will be logged from within HTTP layer
	}

	private void receiveGeneric(Message message, ConnectionInfo connectionInfo) {
		IncomingSOAPReceiver.markIncoming(message);
		MESSAGE_INFORMER.forwardMessage(message, connectionInfo, null);
	}

	@Override
	public OperationDescription getOperation(String action) {
		return null;
	}

	@Override
	public OperationDescription getEventSource(String action) {
		return null;
	}

	@Override
	public void receiveNoContent(String reason, ConnectionInfo connectionInfo) {}

	@Override
	public int getRequestMessageType() {
		return MessageConstants.UNKNOWN_MESSAGE;
	}
}
