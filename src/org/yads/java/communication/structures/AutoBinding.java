/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.structures;

import org.yads.java.communication.CommunicationManager;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.listener.AutoBindingAndOutgoingDiscoveryInfoListener;
import org.yads.java.service.listener.NetworkChangeListener;
import org.yads.java.types.MementoSupport;

public interface AutoBinding extends MementoSupport {

	/**
	 * Returns the {@link CommunicationManager} id.
	 * 
	 * @return {@link CommunicationManager} id
	 */
	public String getCommunicationManagerId();

	/**
	 * Returns the {@link CredentialInfo}.
	 * 
	 * @return the {@link CredentialInfo}
	 */
	public CredentialInfo getCredentialInfo();

	/**
	 * Sets the {@link CredentialInfo} if given {@link CredentialInfo} is not
	 * null or {@link CredentialInfo#EMPTY_CREDENTIAL_INFO}.
	 * 
	 * @param credentialInfo
	 */
	public void setCredentialInfo(CredentialInfo credentialInfo);

	/**
	 * Add a {@link AutoBindingAndOutgoingDiscoveryInfoListener} to the {@link AutoBinding}. This listener will be announced if changes for
	 * bindings are performed.
	 * 
	 * @param listener
	 */
	public void addAutoBindingListener(AutoBindingAndOutgoingDiscoveryInfoListener bindingListener, NetworkChangeListener networkChangeListener);

	/**
	 * Remove {@link AutoBindingAndOutgoingDiscoveryInfoListener} from the {@link AutoBinding}.
	 * 
	 * @param listener
	 */
	public void removeAutoBindingListener(AutoBindingAndOutgoingDiscoveryInfoListener bindingListener, NetworkChangeListener networkChangeListener);

	/**
	 * Returns the unique key of a {@link AutoBinding}.
	 * 
	 * @return the unique key
	 */
	public Integer getKey();
}