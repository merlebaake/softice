/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.structures;

import java.util.Iterator;
import org.yads.java.service.listener.AutoBindingAndOutgoingDiscoveryInfoListener;

public interface DiscoveryAutoBinding extends AutoBinding {

	/**
	 * If it is first call new {@link DiscoveryBinding}s will created and {@link Iterator} will returned, else the {@link Iterator} for created {@link DiscoveryBinding}s for {@link AutoBindingAndOutgoingDiscoveryInfoListener} will returned.
	 * 
	 * @param listener
	 * @return {@link Iterator} over {@link DiscoveryBinding}s
	 */
	public Iterator getDiscoveryBindings(AutoBindingAndOutgoingDiscoveryInfoListener listener);

	/**
	 * Returns count of {@link DiscoveryBinding}s for {@link AutoBindingAndOutgoingDiscoveryInfoListener}.
	 * 
	 * @param listener
	 * @return count of {@link DiscoveryBinding}s
	 */
	public int getDiscoveryBindingsCount(AutoBindingAndOutgoingDiscoveryInfoListener listener);

	/**
	 * If it is first call new {@link OutgoingDiscoveryInfo}s will created and {@link Iterator} will returned, else the {@link Iterator} for created {@link OutgoingDiscoveryInfo}s for {@link AutoBindingAndOutgoingDiscoveryInfoListener} will returned.
	 * 
	 * @param listener
	 * @return {@link Iterator} over {@link OutgoingDiscoveryInfo}s
	 */
	public Iterator getOutgoingDiscoveryInfos(AutoBindingAndOutgoingDiscoveryInfoListener listener);

	/**
	 * Returns count of {@link OutgoingDiscoveryInfo}s for {@link AutoBindingAndOutgoingDiscoveryInfoListener}.
	 * 
	 * @param listener
	 * @return count of {@link OutgoingDiscoveryInfo}s
	 */
	public int getOutgoingDiscoveryInfosCount(AutoBindingAndOutgoingDiscoveryInfoListener listener);

	@Override
	public String getCommunicationManagerId();

	public String getInfoText();
}
