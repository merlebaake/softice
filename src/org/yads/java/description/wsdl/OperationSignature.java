/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.description.wsdl;

import org.yads.java.service.OperationDescription;
import org.yads.java.service.Service;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

public class OperationSignature {

	private final String	name;

	private final String	inputName;

	private final String	outputName;

	public OperationSignature(WSDLOperation operation) {
		this(operation.getName(), operation.getInputName(), operation.getOutputName());
	}

	public OperationSignature(OperationDescription operation) {
		this(operation.getName(), operation.getInputName(), operation.getOutputName());
	}

	/**
	 * @param name
	 * @param inputName
	 * @param outputName
	 */
	public OperationSignature(String name, String inputName, String outputName) {
		super();
		this.name = name;
		this.inputName = inputName == null ? Service.NO_PARAMETER : inputName;
		this.outputName = outputName == null ? Service.NO_PARAMETER : outputName;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder();
		sb.append("OperationSignature [ name=").append(name);
		sb.append(", inputName=").append(inputName);
		sb.append(", outputName=").append(outputName).append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OperationSignature other = (OperationSignature) obj;

		if (inputName == null) {
			if (other.inputName != null) {
				return false;
			}
		} else if (other.inputName == null || !inputName.equals(other.inputName)) {
			return false;
		}
		if (outputName == null) {
			if (other.outputName != null) {
				return false;
			}
		} else if (other.outputName == null || !outputName.equals(other.outputName)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((inputName == null) ? 0 : inputName.hashCode());
		result = prime * result + ((outputName == null) ? 0 : outputName.hashCode());
		return result;
	}

}