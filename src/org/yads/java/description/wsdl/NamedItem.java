/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.description.wsdl;

import org.yads.java.types.QName;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

/**
 * 
 */
public abstract class NamedItem {

	protected QName	name;

	/**
	 * 
	 */
	public NamedItem() {
		this(null);
	}

	public NamedItem(QName name) {
		super();
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder();
		sb.append("name=").append(getName());
		return sb.toString();
	}

	public String getNamespace() {
		QName name = getName();
		return name == null ? null : name.getNamespace();
	}

	public String getLocalName() {
		QName name = getName();
		return name == null ? null : name.getLocalPart();
	}

	/**
	 * @return the name
	 */
	public QName getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(QName name) {
		this.name = name;
	}

}
