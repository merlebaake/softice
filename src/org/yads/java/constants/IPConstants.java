/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.constants;

import org.yads.java.communication.connection.ip.IPAddress;

public interface IPConstants {

	public static final String		IPv4											= "IPv4";

	public static final String		IPv6											= "IPv6";

	// Multicast lower and upper bound
	public static final IPAddress	MULTICAST_IPv4_LOWER_BOUND						= IPAddress.createRemoteIPAddress("224.0.0.0");

	public static final IPAddress	MULTICAST_IPv4_UPPER_BOUND						= IPAddress.createRemoteIPAddress("239.255.255.255");

	// Multicast lower bound

	public static final IPAddress	MULTICAST_IPv6_LOWER_BOUND						= IPAddress.createRemoteIPAddress("[FF00::0]");

	/** Default IP Network Detection Class */
	public static final String		DEFAULT_IP_NETWORK_DETECTION_CLASS				= "PlatformIPNetworkDetection";

	public static final String		DEFAULT_IP_NETWORK_DETECTION_PATH				= "org.yads.java.communication.connection.ip." + DEFAULT_IP_NETWORK_DETECTION_CLASS;

	public static final String		DEFAULT_SECURE_PLATFORM_SOCKET_FACTORY_CLASS	= "SecurePlatformSocketFactory";

	public static final String		DEFAULT_SECURE_PLATFORM_SOCKET_FACTORY_PATH		= "org.yads.java.communication.connection.tcp." + DEFAULT_SECURE_PLATFORM_SOCKET_FACTORY_CLASS;

	public static final String		DEFAULT_PLATFORM_SOCKET_FACTORY_CLASS			= "PlatformSocketFactory";

	public static final String		DEFAULT_PLATFORM_SOCKET_FACTORY_PATH			= "org.yads.java.communication.connection.tcp." + DEFAULT_PLATFORM_SOCKET_FACTORY_CLASS;
}
