/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.constants.DPWS2011;

import org.yads.java.communication.DPWSProtocolVersion;
import org.yads.java.constants.MessageConstants;
import org.yads.java.constants.WSAConstants;
import org.yads.java.constants.WSAConstants2009;
import org.yads.java.constants.WSEConstants;
import org.yads.java.constants.DPWS2009.WSDConstants2009;
import org.yads.java.constants.general.DPWSConstants;
import org.yads.java.constants.general.DPWSConstantsHelper;
import org.yads.java.constants.general.WSDConstants;
import org.yads.java.constants.general.WSMEXConstants;
import org.yads.java.schema.ComplexType;
import org.yads.java.schema.Element;
import org.yads.java.schema.SchemaUtil;
import org.yads.java.types.AttributedURI;
import org.yads.java.types.QName;
import org.yads.java.types.URI;

public class DefaultDPWSConstantsHelper2011 extends DPWSConstantsHelper {

	private static String							displayName	= "DPWS 1.2";

	private static DefaultDPWSConstantsHelper2011	instance	= null;

	private static final Element					WSA_2011_PROBLEM_ACTION_SCHEMA_ELEMENT;

	public static synchronized DefaultDPWSConstantsHelper2011 getInstance() {
		if (instance == null) {
			instance = new DefaultDPWSConstantsHelper2011();
		}
		return instance;
	}

	static {
		ComplexType detailType = new ComplexType("WSAProblemActionType", WSAConstants2009.WSA_NAMESPACE_NAME, ComplexType.CONTAINER_ALL);
		Element actionElement = new Element(new QName(WSAConstants.WSA_ELEM_ACTION, WSAConstants2009.WSA_NAMESPACE_NAME), SchemaUtil.TYPE_ANYURI);
		detailType.addElement(actionElement);
		WSA_2011_PROBLEM_ACTION_SCHEMA_ELEMENT = new Element(WSAConstants.WSA_PROBLEM_ACTION, detailType);
	}

	@Override
	public DPWSProtocolVersion getDPWSVersion() {
		return DPWSProtocolVersion.DPWS_VERSION_2011;
	}

	@Override
	public String getDPWSName() {
		return DPWSConstants2011.DPWS_VERSION_NAME;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}

	@Override
	public int getRandomApplicationDelay() {
		return DPWSConstants2011.DPWS_APP_MAX_DELAY;
	}

	@Override
	public int getUnicastUDPRepeat() {
		return DPWSConstants2011.MULTICAST_UDP_REPEAT;
	}

	@Override
	public int getMulticastUDPRepeat() {
		return DPWSConstants2011.UNICAST_UDP_REPEAT;
	}

	/**
	 * DPWS Constants
	 */
	@Override
	public String getDPWSNamespace() {
		return DPWSConstants2011.DPWS_NAMESPACE_NAME;
	}

	@Override
	public String getDPWSNamespacePrefix() {
		return DPWSConstants2011.DPWS_NAMESPACE_PREFIX;
	}

	@Override
	public String getActionName(int messageType) {
		switch (messageType) {
			case MessageConstants.HELLO_MESSAGE:
				return getWSDActionHello();
			case MessageConstants.BYE_MESSAGE:
				return getWSDActionBye();
			case MessageConstants.PROBE_MESSAGE:
				return getWSDActionProbe();
			case MessageConstants.PROBE_MATCHES_MESSAGE:
				return getWSDActionProbeMatches();
			case MessageConstants.RESOLVE_MESSAGE:
				return getWSDActionResolve();
			case MessageConstants.RESOLVE_MATCHES_MESSAGE:
				return getWSDActionResolveMatches();
			case MessageConstants.GET_MESSAGE:
				return WXFConstants2011.WXF_ACTION_GET;
			case MessageConstants.GET_RESPONSE_MESSAGE:
				return WXFConstants2011.WXF_ACTION_GETRESPONSE;
			case MessageConstants.GET_METADATA_MESSAGE:
				return WSMEXConstants2011.WSX_ACTION_GETMETADATA_REQUEST;
			case MessageConstants.GET_METADATA_RESPONSE_MESSAGE:
				return WSMEXConstants2011.WSX_ACTION_GETMETADATA_RESPONSE;
			case MessageConstants.SUBSCRIBE_MESSAGE:
				return WSEConstants2011.WSE_ACTION_SUBSCRIBE;
			case MessageConstants.SUBSCRIBE_RESPONSE_MESSAGE:
				return WSEConstants2011.WSE_ACTION_SUBSCRIBERESPONSE;
			case MessageConstants.UNSUBSCRIBE_MESSAGE:
				return WSEConstants2011.WSE_ACTION_UNSUBSCRIBE;
			case MessageConstants.UNSUBSCRIBE_RESPONSE_MESSAGE:
				return WSEConstants2011.WSE_ACTION_UNSUBSCRIBERESPONSE;
			case MessageConstants.RENEW_MESSAGE:
				return WSEConstants2011.WSE_ACTION_RENEW;
			case MessageConstants.RENEW_RESPONSE_MESSAGE:
				return WSEConstants2011.WSE_ACTION_RENEWRESPONSE;
			case MessageConstants.GET_STATUS_MESSAGE:
				return WSEConstants2011.WSE_ACTION_GETSTATUS;
			case MessageConstants.GET_STATUS_RESPONSE_MESSAGE:
				return WSEConstants2011.WSE_ACTION_GETSTATUSRESPONSE;
			case MessageConstants.SUBSCRIPTION_END_MESSAGE:
				return WSEConstants2011.WSE_ACTION_SUBSCRIPTIONEND;
			default:
				return MessageConstants.getMessageNameForType(-1);
		}
	}

	@Override
	public URI getDPWSUriFilterEventingAction() {
		return DPWSConstants2011.DPWS_URI_FILTER_EVENTING_ACTION;
	}

	@Override
	public QName getDPWSFaultFilterActionNotSupported() {
		return DPWSConstants2011.DPWS_QN_FAULT_FILTER_ACTION_NOT_SUPPORTED;
	}

	/** METADATA. */
	@Override
	public String getMetadataDialectThisModel() {
		return DPWSConstants2011.DPWS_NAMESPACE_NAME + DPWSConstants.METADATA_DIALECT_THISMODEL;
	}

	@Override
	public String getMetadataDialectThisDevice() {
		return DPWSConstants2011.DPWS_NAMESPACE_NAME + DPWSConstants.METADATA_DIALECT_THISDEVICE;
	}

	@Override
	public String getMetatdataDialectRelationship() {
		return DPWSConstants2011.DPWS_NAMESPACE_NAME + DPWSConstants.METADATA_DIALECT_RELATIONSHIP;
	}

	@Override
	public String getMetadataRelationshipHostingType() {
		return DPWSConstants2011.DPWS_NAMESPACE_NAME + DPWSConstants.METADATA_RELATIONSHIP_HOSTING_TYPE;
	}

	/** The DPWS SOAP fault action. */
	@Override
	public URI getDPWSActionFault() {
		return DPWSConstants2011.DPWS_ACTION_FAULT;
	}

	/** QualifiedName of "Manufacturer". */
	@Override
	public QName getDPWSQnManufacturer() {
		return DPWSConstants2011.DPWS_QN_MANUFACTURER;
	}

	/** QualifiedName of "ManufacturerUrl". */
	@Override
	public QName getDPWSQnManufactuerURL() {
		return DPWSConstants2011.DPWS_QN_MANUFACTURER_URL;
	}

	/** QualifiedName of "ModelName". */
	@Override
	public QName getDPWSQnModelname() {
		return DPWSConstants2011.DPWS_QN_MODELNAME;
	}

	/** QualifiedName of "ModelNumber". */
	@Override
	public QName getDPWSQnModelnumber() {
		return DPWSConstants2011.DPWS_QN_MODELNUMBER;
	}

	/** QualifiedName of "ModelUrl". */
	@Override
	public QName getDPWSQnModelURL() {
		return DPWSConstants2011.DPWS_QN_MODEL_URL;
	}

	/** QualifiedName of "PresentationUrl". */
	@Override
	public QName getDPWSQnPresentationURL() {
		return DPWSConstants2011.DPWS_QN_PRESENTATION_URL;
	}

	// QualifiedNames of ThisDevice

	/** QualifiedName of "FriendlyName". */
	@Override
	public QName getDPWSQnFriendlyName() {
		return DPWSConstants2011.DPWS_QN_FRIENDLYNAME;
	}

	/** QualifiedName of "FirmwareVersion". */
	@Override
	public QName getDPWSQnFirmware() {
		return DPWSConstants2011.DPWS_QN_FIRMWARE_VERSION;
	}

	/** QualifiedName of "SerialNumber". */
	@Override
	public QName getDPWSQnSerialnumber() {
		return DPWSConstants2011.DPWS_QN_SERIALNUMBER;
	}

	// QualifiedNames of Host

	/** QualifiedName of "ServiceId". */
	@Override
	public QName getDPWSQnServiceID() {
		return DPWSConstants2011.DPWS_QN_SERVICE_ID;
	}

	/** QualifiedName of "EndpointReference". */
	@Override
	public QName getDPWSQnEndpointReference() {
		return DPWSConstants2011.DPWS_QN_ENDPOINT_REFERENCE;
	}

	/** QualifiedName of "Types". */
	@Override
	public QName getDPWSQnTypes() {
		return DPWSConstants2011.DPWS_QN_TYPES;
	}

	/** DPWS dpws:Device type like described in R1020 */
	@Override
	public QName getDPWSQnDeviceType() {
		return DPWSConstants2011.DPWS_QN_DEVICETYPE;
	}

	/**
	 * WSA Constants
	 */
	@Override
	public String getWSANamespace() {
		return WSAConstants2009.WSA_NAMESPACE_NAME;
	}

	@Override
	public String getWSAElemReferenceProperties() {
		return null;
	}

	@Override
	public String getWSAElemPortType() {
		return null;
	}

	@Override
	public String getWSAElemServiceName() {
		return null;
	}

	@Override
	public String getWSAElemPolicy() {
		return null;
	}

	@Override
	public AttributedURI getWSAAnonymus() {
		return WSAConstants2009.WSA_ANONYMOUS;
	}

	@Override
	public URI getWSAActionAddressingFault() {
		return WSAConstants2009.WSA_ACTION_ADDRESSING_FAULT;
	}

	@Override
	public URI getWSAActionSoapFault() {
		return WSAConstants2009.WSA_ACTION_SOAP_FAULT;
	}

	@Override
	public Element getWSAProblemActionSchemaElement() {
		return WSA_2011_PROBLEM_ACTION_SCHEMA_ELEMENT;
	}

	/* faults */
	@Override
	public QName getWSAFaultDestinationUnreachable() {
		return WSAConstants2009.WSA_QN_FAULT_DESTINATION_UNREACHABLE;
	}

	@Override
	public QName getWSAFaultInvalidAddressingHeader() {
		return WSAConstants2009.WSA_QN_FAULT_INVALID_ADDRESSING_HEADER;
	}

	@Override
	public QName getWSAFaultMessageAddressingHeaderRequired() {
		return WSAConstants2009.WSA_QN_FAULT_MESSAGE_ADDRESSING_HEADER_REQUIRED;
	}

	@Override
	public QName getWSAFaultActionNotSupported() {
		return WSAConstants2009.WSA_QN_FAULT_ACTION_NOT_SUPPORTED;
	}

	@Override
	public QName getWSAFaultEndpointUnavailable() {
		return WSAConstants2009.WSA_QN_FAULT_ENDPOINT_UNAVAILABLE;
	}

	@Override
	public QName getWSAProblemHeaderQName() {
		return WSAConstants2009.WSA_QN_PROBLEM_HEADER;
	}

	@Override
	public QName getWSAProblemAction() {
		return WSAConstants2009.WSA_QN_PROBLEM_ACTION;
	}

	/**
	 * WSD Constants
	 */
	@Override
	public String getWSDNamespace() {

		return WSDConstants2009.WSD_NAMESPACE_NAME;
	}

	@Override
	public AttributedURI getWSDTo() {
		return WSDConstants2009.WSD_TO;
	}

	@Override
	public String getWSDActionHello() {
		return WSDConstants2009.WSD_ACTION_HELLO;
	}

	@Override
	public String getWSDActionBye() {
		return WSDConstants2009.WSD_ACTION_BYE;
	}

	@Override
	public String getWSDActionProbe() {
		return WSDConstants2009.WSD_ACTION_PROBE;
	}

	@Override
	public String getWSDActionProbeMatches() {
		return WSDConstants2009.WSD_ACTION_PROBEMATCHES;
	}

	@Override
	public String getWSDActionResolve() {
		return WSDConstants2009.WSD_ACTION_RESOLVE;
	}

	@Override
	public String getWSDActionResolveMatches() {
		return WSDConstants2009.WSD_ACTION_RESOLVEMATCHES;
	}

	@Override
	public String getWSDActionFault() {
		return WSDConstants2009.WSD_NAMESPACE_NAME + WSDConstants.WSD_ACTION_WSD_FAULT;
	}

	@Override
	public QName getWSDDiscoveryProxyType() {
		return WSDConstants2009.WSD_DISCOVERY_PROXY_TYPE;
	}

	@Override
	public URI getMetadataDialectCustomMetadata() {
		return DPWSConstants2011.METADATA_DIALECT_CUSTOMIZE_METADATA;
	}

	/**
	 * MEX Constants
	 */
	@Override
	public String getWSMEXNamespace() {
		return WSMEXConstants2011.WSX_NAMESPACE_NAME;
	}

	@Override
	public String getWSMEXNamespacePrefix() {
		return WSMEXConstants.WSX_NAMESPACE_PREFIX;
	}

	@Override
	public String getWSMEXActionGetMetadataRequest() {
		return WSMEXConstants2011.WSX_ACTION_GETMETADATA_REQUEST;
	}

	@Override
	public String getWSMEXActionGetMetadataResponse() {
		return WSMEXConstants2011.WSX_ACTION_GETMETADATA_RESPONSE;
	}

	/**
	 * WXF Constants
	 */

	@Override
	public String getWXFNamespace() {
		return WXFConstants2011.WXF_NAMESPACE_NAME;
	}

	@Override
	public String getWXFNamespacePrefix() {
		return WXFConstants2011.WXF_NAMESPACE_PREFIX;
	}

	@Override
	public String getWXFActionGet() {
		return WXFConstants2011.WXF_ACTION_GET;
	}

	@Override
	public String getWXFActionGetResponse() {
		return WXFConstants2011.WXF_ACTION_GETRESPONSE;
	}

	@Override
	public String getWXFActionGet_Request() {
		return WXFConstants2011.WXF_ACTION_GET_REQUEST;
	}

	@Override
	public String getWXFActionGet_Response() {
		return WXFConstants2011.WXF_ACTION_GET_RESPONSE;
	}

	/**
	 * WSE Constants
	 */

	@Override
	public String getWSENamespace() {
		return WSEConstants2011.WSE_NAMESPACE_NAME;
	}

	@Override
	public String getWSENamespacePrefix() {
		return WSEConstants.WSE_NAMESPACE_PREFIX;
	}

	@Override
	public String getWSEActionSubscribe() {
		return WSEConstants2011.WSE_ACTION_SUBSCRIBE;
	}

	@Override
	public String getWSEActionSubscribeResponse() {
		return WSEConstants2011.WSE_ACTION_SUBSCRIBERESPONSE;
	}

	@Override
	public String getWSEActionUnsubscribe() {
		return WSEConstants2011.WSE_ACTION_UNSUBSCRIBE;
	}

	@Override
	public String getWSEActionUnsubscribeResponse() {
		return WSEConstants2011.WSE_ACTION_UNSUBSCRIBERESPONSE;
	}

	@Override
	public String getWSEActionRenew() {
		return WSEConstants2011.WSE_ACTION_RENEW;
	}

	@Override
	public String getWSEActionRenewResponse() {
		return WSEConstants2011.WSE_ACTION_RENEWRESPONSE;
	}

	@Override
	public String getWSEActionSubscriptionEnd() {
		return WSEConstants2011.WSE_ACTION_SUBSCRIPTIONEND;
	}

	@Override
	public String getWSEActionGetStatus() {
		return WSEConstants2011.WSE_ACTION_GETSTATUS;
	}

	@Override
	public String getWSEActionGetStatusResponse() {
		return WSEConstants2011.WSE_ACTION_GETSTATUSRESPONSE;
	}

	@Override
	public QName getWSEQNIdentifier() {
		return WSEConstants2011.WSE_QN_IDENTIFIER;
	}

	@Override
	public QName getWSESupportedDeliveryMode() {
		return WSEConstants2011.WSE_SUPPORTED_DELIVERY_MODE;
	}

	@Override
	public QName getWSESupportedDialect() {
		return WSEConstants2011.WSE_SUPPORTED_DIALECT;
	}

	@Override
	public String getWSEDeliveryModePush() {
		return WSEConstants2011.WSE_DELIVERY_MODE_PUSH;
	}

	@Override
	public String getWSEStatusDeliveryFailure() {
		return WSEConstants2011.WSE_STATUS_DELIVERY_FAILURE;
	}

	@Override
	public String getWSEStatusSourceShuttingDown() {
		return WSEConstants2011.WSE_STATUS_SOURCE_SHUTTING_DOWN;
	}

	@Override
	public String getWSEStatusSourceCanceling() {
		return WSEConstants2011.WSE_STATUS_SOURCE_CANCELING;
	}

	@Override
	public QName getWSEFaultFilteringNotSupported() {
		return WSEConstants2011.WSE_FAULT_FILTERING_NOT_SUPPORTED;
	}

	@Override
	public QName getWSEFaultFilteringRequestedUnavailable() {
		return WSEConstants2011.WSE_FAULT_FILTERING_REQUESTED_UNAVAILABLE;
	}

	@Override
	public QName getWSEFaultUnsupportedExpirationType() {
		return WSEConstants2011.WSE_FAULT_UNSUPPORTED_EXPIRATION_TYPE;
	}

	@Override
	public QName getWSEFaultDeliveryModeRequestedUnavailable() {
		return WSEConstants2011.WSE_FAULT_DELIVERY_MODE_REQUESTED_UNVAILABLE;
	}

	@Override
	public QName getWSEFaultInvalidExpirationTime() {
		return WSEConstants2011.WSE_FAULT_INVALID_EXPIRATION_TIME;
	}

	@Override
	public QName getWSEFaultInvalidMessage() {
		return WSEConstants2011.WSE_FAULT_INVALID_MESSAGE;
	}

	@Override
	public QName getWSEFaultEventSourceUnableToProcess() {
		return WSEConstants2011.WSE_FAULT_EVENT_SOURCE_UNABLE_TO_PROCESS;
	}

	@Override
	public QName getWSEFaultUnableToRenew() {
		return WSEConstants2011.WSE_FAULT_UNABLE_TO_RENEW;
	}
}
