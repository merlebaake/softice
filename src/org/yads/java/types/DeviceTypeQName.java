/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.types;

import org.yads.java.communication.ProtocolVersion;

public class DeviceTypeQName extends QName {

	private ProtocolVersion	protocolVersion;

	public DeviceTypeQName(String localPart, String namespace, int priority, ProtocolVersion protocolVersion) {
		super(localPart, namespace, priority);
		this.protocolVersion = protocolVersion;
	}

	public DeviceTypeQName(String localPart, String namespace, String prefix, int priority, ProtocolVersion protocolVersion) {
		super(localPart, namespace, prefix, priority);
		this.protocolVersion = protocolVersion;
	}

	public DeviceTypeQName(String localPart, String namespace, String prefix, ProtocolVersion protocolVersion) {
		super(localPart, namespace, prefix);
		this.protocolVersion = protocolVersion;
	}

	public DeviceTypeQName(String localPart, String namespace, ProtocolVersion protocolVersion) {
		super(localPart, namespace);
		this.protocolVersion = protocolVersion;
	}

	public DeviceTypeQName(String localPart, ProtocolVersion protocolVersion) {
		super(localPart);
		this.protocolVersion = protocolVersion;
	}

	public ProtocolVersion getProtocolVersion() {
		return protocolVersion;
	}
}
