/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.types;

import java.io.IOException;

import java.util.HashMap;
import org.yads.java.xmlpull.v1.XmlSerializer;

/**
 * Custom attribute value representation as a plain string.
 */
public class StringAttributeValue implements CustomAttributeValue {

	private String	value;

	/**
	 * @param value
	 */
	public StringAttributeValue(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.types.CustomAttributeValue#getNamespaces()
	 */
	@Override
	public HashMap getNamespaces() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.types.CustomAttributeValue#getValue()
	 */
	@Override
	public Object getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.types.CustomAttributeValue#serialize(org.xmlpull.v1.
	 * XmlSerializer)
	 */
	@Override
	public void serialize(XmlSerializer serializer, QName attributeName) throws IOException {
		String ns = attributeName.getNamespace();
		if ("".equals(ns)) {
			ns = null;
		}
		serializer.attribute(ns, attributeName.getLocalPart(), value == null ? "" : value);
	}

}
