/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.schema;

import java.io.IOException;

import java.util.LinkedList;
import org.yads.java.xmlpull.v1.XmlSerializer;

/**
 * Container for complexType:sequence.
 */
public class SequenceContainer extends ElementContainer {

	public SequenceContainer() {
		super(new LinkedList());
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.types.schema.ElementContainer#toString()
	 */
	@Override
	public String toString() {
		int all = getElementCount();
		return "SequenceContainer [ own=" + elementCount + ", inherit=" + (all - elementCount) + ", all=" + all + ", min=" + min + ", max=" + max + ", container=" + container + " ]";
	}

	@Override
	public int getSchemaIdentifier() {
		return XSD_SEQUENCEMODEL;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.types.schema.ElementContainer#getContainerType()
	 */
	@Override
	public int getContainerType() {
		return ComplexType.CONTAINER_SEQUENCE;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.types.schema.ElementContainer#serialize(org.xmlpull.v1.
	 * XmlSerializer, org.yads.java.types.schema.Schema)
	 */
	@Override
	void serialize(XmlSerializer serializer, Schema schema) throws IOException {
		serializer.startTag(XMLSCHEMA_NAMESPACE, TAG_SEQUENCE);
		serialize0(serializer, schema);
		serializer.endTag(XMLSCHEMA_NAMESPACE, TAG_SEQUENCE);
	}

}
