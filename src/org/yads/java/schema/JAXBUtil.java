/*******************************************************************************
 * Copyright (c) 2016 Andreas Besting (info@besting-it.de)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *
 *******************************************************************************/

package org.yads.java.schema;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.structures.MaxHashMap;

/**
 *
 * @author besting
 */
public class JAXBUtil {

    private static JAXBUtil instance;
    private boolean skipParameterValueParsing = false;
    private Transformer transformer;
    
    // Note: JAXBContext instances itself are thread-safe.
    private final Map<String, JAXBContext> contexts = Collections.synchronizedMap(new MaxHashMap<String, JAXBContext>(256));
    private JAXBContext staticContext;
    private ValidationEventHandler validationEventHandler;
    private javax.xml.validation.Schema schema;
    
    private JAXBUtil() {
    }
    
    public static JAXBUtil getInstance() {
        if (instance == null)
            instance = new JAXBUtil();
        return instance;
    }

    public void setValidationEventHandler(ValidationEventHandler validationEventHandler, javax.xml.validation.Schema schema) {
        this.validationEventHandler = validationEventHandler;
        this.schema = schema;
    }

    public ValidationEventHandler getValidationEventHandler() {
        return validationEventHandler;
    }
    
    public <T> T createInputParameterValue(ParameterValue pv, Class<T> type) {
        String typeStr = type.toString();
        try {
            JAXBContext c = getContext(typeStr, type);
            Unmarshaller m = c.createUnmarshaller();
            if (validationEventHandler != null && schema != null) {
                m.setEventHandler(validationEventHandler);      
                m.setSchema(schema);
            }
            String xmlString = pv.getParameterRawData();
            Node xmlNode = pv.getParameterXmlData();
            return xmlNode != null? type.cast(m.unmarshal(xmlNode)) 
                    : type.cast(m.unmarshal(new ByteArrayInputStream(xmlString.getBytes(Charset.defaultCharset()))));
        } catch (JAXBException ex) {
            Logger.getLogger(JAXBUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public <T> ParameterValue createOutputParameterValue(T object) {
        StringWriter writer = new StringWriter();
        if (!marshallTo(object, writer))
            return null;
        String str = writer.toString();
        ParameterValue pv = new ParameterValue();
        pv.setParameterRawData(str.substring(str.indexOf('<', 1)));   
        return pv;
    }
    
    public <T> boolean marshallTo(T object, Object target) {
        String typeStr = object.getClass().toString();
        try {
            JAXBContext c = getContext(typeStr, object.getClass());
            Marshaller m = c.createMarshaller();
            if (validationEventHandler != null && schema != null) {
                m.setEventHandler(validationEventHandler);      
                m.setSchema(schema);
            }       
            if (target instanceof StringWriter) {
                m.marshal(object, (StringWriter)target);
            }
            else if (target instanceof Document) {
                m.marshal(object, (Document)target);
            }
            else throw new Exception("Type not supported.");
        } catch (Exception ex) {
            Logger.getLogger(JAXBUtil.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }        
        return true;
    }

    private <T> JAXBContext getContext(String typeStr, Class<T> type) throws JAXBException {
        if (staticContext != null) {
            return staticContext;
        }
        JAXBContext c;
        if (contexts.containsKey(typeStr)) {
            c = contexts.get(typeStr);
        }
        else {
            c = JAXBContext.newInstance(type);
            contexts.put(typeStr, c);
        }
        return c;
    }
    
    public void setStaticContext(JAXBContext context) {
        this.staticContext = context;
    }

    public synchronized void setSkipParameterValueParsing(boolean skipParameterValueParsing) {
        this.skipParameterValueParsing = skipParameterValueParsing;
    }

    public synchronized boolean isSkipParameterValueParsing() {
        return skipParameterValueParsing;
    }
    
    public String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            if (transformer == null)
                transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            te.printStackTrace();
            return null;
        }
        return sw.toString();
    }
    
}
