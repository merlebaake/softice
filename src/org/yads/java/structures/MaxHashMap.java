/*******************************************************************************
 * Copyright (c) 2016 Andreas Besting (info@besting-it.de)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *
 *******************************************************************************/

package org.yads.java.structures;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author besting
 */
public class MaxHashMap<K, V> extends LinkedHashMap<K, V> {
 
    private int maxSize;
    private RemoveEldestHandler<K, V> removeEldestHandler;

    public MaxHashMap(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        boolean b = size() > getMaxSize();
        if (b && removeEldestHandler != null)
            removeEldestHandler.handle(eldest);
        return b;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }
    
    public void touch(K key) {
        V val = remove(key);
        put(key, val);
    }
        
}
