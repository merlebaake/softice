/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.structures;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Heap extends ArrayList {

	Comparator	comparator;

	public Heap(Comparator comp) {
		this.comparator = comp;
	}

	/**
	 * Returns and removes the root element of the heap.
	 * 
	 * @return
	 */
	public Object getRoot() {
		if (this.size() <= 0) {
			return null;
		}

		Object o = null;
		if (this.size() > 1) {
            try {
                o = this.set(0, this.remove(this.size() - 1));
                Field field = this.getClass().getDeclaredField("elementData");
                field.setAccessible(true);
                siftDown((Object[])field.get(this), 0, size());
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(Heap.class.getName()).log(Level.SEVERE, null, ex);
            }
		} else {
			o = this.remove(0);
		}

		return o;
	}

	/**
	 * Adds an element to the data structure. The element will be build into the
	 * heap at the correct location to fulfill the heap property.
	 */
	@Override
	public boolean add(Object object) {
        try {
            super.add(object);
            
            Field field = this.getClass().getDeclaredField("elementData");
            field.setAccessible(true);
            siftUp((Object[])field.get(this), size() - 1);
            
            return true;
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(Heap.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
	}

	private void siftDown(Object[] a, int start, int end) {
		int child;

		for (int root = start; (child = root * 2 + 1) < end;) {
			int swap = root;

			if (comparator.compare(a[swap], a[child]) <= -1) {
				swap = child;
			}
			if (child + 1 < end && comparator.compare(a[swap], a[child + 1]) <= -1) {
				swap = child + 1;
			}

			if (swap != root) {
				Object t = a[root];
				a[root] = a[swap];
				a[swap] = t;
			} else {
				return;
			}

			root = swap;
		}
	}

	private void siftUp(Object[] a, int start) {
		int parent;
		for (int root = start; (parent = (root - 1) / 2) >= 0;) {
			int swap = root;

			if (comparator.compare(a[swap], a[parent]) >= 1) {
				swap = parent;
			}
			if (swap != root) {
				Object t = a[root];
				a[root] = a[swap];
				a[swap] = t;
			} else {
				return;
			}

			root = swap;
		}
	}
    
}
