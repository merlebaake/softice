/*******************************************************************************
 * Copyright (c) 2016 Andreas Besting (info@besting-it.de)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *
 *******************************************************************************/

package org.yads.java.structures;

import java.util.Map;

/**
 *
 * @author besting
 */
public interface RemoveEldestHandler<K, V> {
    
    void handle(Map.Entry<K, V> entry);
    
}
