/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.util;

import org.yads.java.YADSFramework;
import org.yads.java.concurrency.LockSupport;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutorService;

public class WatchDog implements Runnable {

	/** extra time buffer for removing callbacks */
	final static int			ADDITIONAL_TIME_BEFORE_CALLBACK_REMOVAL	= 50;					// milliseconds

	/** list<TimedEntry> */
	protected final LinkedList	listEntries								= new LinkedList();

	/** <code>true</code> if class was started */
	private boolean				running									= false;

	/** lock protects list */
	protected LockSupport		lock									= new LockSupport();

	/** this */
	private static WatchDog		instance								= null;

	/**
	 * Private constructor.
	 */
	private WatchDog() {}

	public static synchronized WatchDog getInstance() {
		if (instance == null) {
			instance = new WatchDog();
		}
		return instance;
	}

	/**
	 * Registers timed object to observe.
	 * 
	 * @param timedEntry
	 * @param timeUntilTimeout
	 */
	public void register(TimedEntry timedEntry, long timeUntilTimeout) {

		lock.exclusiveLock();
		try {
			if (timedEntry.registered) {
				update(timedEntry, timeUntilTimeout);
				return;
			}

			timedEntry.setTimer(timeUntilTimeout);

			if (listEntries.size() == 0) {
				listEntries.add(timedEntry);
			} else {
				boolean added = false;
				ListIterator it = listEntries.listIterator(listEntries.size());
				while (it.hasPrevious()) {
					TimedEntry temp = (TimedEntry) it.previous();
					if (temp.compareTo(timedEntry) < 0) {
						if (it.hasNext()) {
							it.next();
						}
						it.add(timedEntry);
						added = true;
						break;
					}
				}
				if (!added) {
					it.add(timedEntry);
				}
			}
			timedEntry.registered = true;
		} finally {
			lock.releaseExclusiveLock();
		}
		synchronized (this) {
			this.notify();
		}
	}

	/**
	 * Disables timed entry from managed objects of watchdog. Removing will
	 * occur later.
	 * 
	 * @param timedEntry
	 */
	public void unregister(TimedEntry timedEntry) {
		lock.sharedLock();
		try {
			// XXX: Will be faster, if we can use entries of linked list.
			// listEntries.remove(timedEntry);
			// entry will be removed at timeout
			timedEntry.disabled = true;
		} finally {
			lock.releaseSharedLock();
		}
	}

	/**
	 * Updates timed entry with new time until timeout within managed objects of
	 * watchdog.
	 * 
	 * @param timedEntry
	 * @param timeUntilTimeout
	 */
	public void update(TimedEntry timedEntry, long timeUntilTimeout) {
		List timeoutObjects = new ArrayList(3);

		lock.exclusiveLock();
		try {
			if (!timedEntry.registered) {
				register(timedEntry, timeUntilTimeout);
				return;
			}

			timedEntry.setTimer(timeUntilTimeout);
			long currentTime = System.currentTimeMillis();

			if (listEntries.size() > 1) {
				boolean added = false;
				ListIterator it = listEntries.listIterator();
				while (it.hasNext()) {
					TimedEntry entry = (TimedEntry) it.next();

					if (entry.equals(timedEntry)) {
						it.remove();
						entry.registered = false;
					} else if (entry.timeToRemove <= currentTime) {
						it.remove();
						if (!entry.disabled) {
							// previous unregistered entries won't receive
							// timeout
							timeoutObjects.add(entry);
						}
						entry.registered = false;
						entry.disabled = false;
					} else if (entry.compareTo(timedEntry) > 0 && !added) {
						it.previous();
						it.add(timedEntry);
						added = true;
						if (!entry.registered) {
							// we only break, if entry removed from old position
							break;
						}
					} else if (!entry.registered) {
						// we only break, if entry removed from old position
						break;
					}

				}
				if (!added) {
					it.add(timedEntry);
				}
			}

			timedEntry.registered = true;
			timedEntry.disabled = false;
		} finally {
			lock.releaseExclusiveLock();
		}

		/*
		 * timeout for all removed objects
		 */
		callTimeouts(timeoutObjects);

		synchronized (this) {
			this.notify();
		}
	}

	// ------------------------ RUNNABLE ------------------------------

	/**
	 * Starts thread to remove timed out message requests.
	 */
	@Override
	public void run() {
		running = true;

		while (running) {
			try {
				lock.sharedLock();
				// (INGO) potentially DANGEROUS because the variable size in
				// class LinkedList is not volatile
				if (listEntries.size() == 0) {
					synchronized (this) {
						lock.releaseSharedLock();
						wait();
					}
				} else {
					TimedEntry e = (TimedEntry) listEntries.getFirst();
					long millis2Sleep = e.timeToRemove - System.currentTimeMillis();

					if (millis2Sleep > 0) {
						// some extra millis;
						millis2Sleep += ADDITIONAL_TIME_BEFORE_CALLBACK_REMOVAL;
						synchronized (this) {
							lock.releaseSharedLock();
							this.wait(millis2Sleep);
						}
					} else {
						lock.releaseSharedLock();
					}
					checkEntries();
				}
			} catch (InterruptedException e1) {
				// e1.printStackTrace();
			}
		}
	}

	/**
	 * Stops thread, which handles timed entries.
	 */
	public void stop() {
		running = false;
		synchronized (this) {
			notifyAll();
		}
		clearEntries();
	}

	// ---------------------------- PRIVATE ----------------------------

	private void clearEntries() {
		List timeoutObjects = new ArrayList(listEntries.size());

		lock.exclusiveLock();
		try {
			for (Iterator it = listEntries.iterator(); it.hasNext();) {
				final TimedEntry entry = (TimedEntry) it.next();
				it.remove();
				entry.registered = false;
				if (!entry.disabled) {
					timeoutObjects.add(entry);
					entry.disabled = false;
				}
			}
		} finally {
			lock.releaseExclusiveLock();
		}

		callTimeouts(timeoutObjects);
	}

	/**
	 * Removes all entries with expired lifetime. Sends timeout to all removed
	 * callbacks.
	 */
	private void checkEntries() {
		long currentTime = System.currentTimeMillis();
		List timeoutObjects = new ArrayList(5);

		lock.exclusiveLock();
		try {
			for (Iterator it = listEntries.iterator(); it.hasNext();) {
				TimedEntry entry = (TimedEntry) it.next();
				if (entry.timeToRemove <= currentTime) {
					it.remove();
					if (!entry.disabled) {
						// previous unregistered entries won't receive
						// timeout
						timeoutObjects.add(entry);
					}
					entry.registered = false;
					entry.disabled = false;
				} else {
					break;
				}
			}
		} finally {
			lock.releaseExclusiveLock();
		}

		/*
		 * timeout for all removed objects
		 */
		callTimeouts(timeoutObjects);
	}

	/**
	 * Calls timeout callback methods of timed entries in separate threads.
	 * 
	 * @param timeoutObjects
	 */
	private void callTimeouts(List timeoutObjects) {
		ExecutorService pool = YADSFramework.getThreadPool();
		if (pool == null) {
			return;
		}

		for (Iterator it = timeoutObjects.iterator(); it.hasNext();) {
			final TimedEntry entry = (TimedEntry) it.next();
			pool.execute(new Runnable() {

				@Override
				public void run() {
					entry.timedOut();
				}
			});
		}
	}

}
