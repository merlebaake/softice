/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.util;

import java.util.UUID;

import org.yads.java.types.URI;

/**
 * This class can be used to create RFC-4122 time based Universally Unique
 * Identifiers with random host node part. Portions adopted from the JUG UUID
 * generator. Also offers methods for generating random numbers.
 * 
 * @see <a href="http://jug.safehaus.org/">http://jug.safehaus.org/</a>
 * @see <a href="http://www.faqs.org/rfcs/rfc4122.html">http://www.faqs.org/rfcs/rfc4122.html</a>
 */
public final class IDGenerator {

	public static final String	UUID_PREFIX				= "uuid";

	/**
	 * the prefix to be used.
	 */
	public static final String	URI_UUID_PREFIX			= "urn:" + UUID_PREFIX + ":";

	/**
	 * Start of the Gregorian calendar.
	 */

	private static long			commonSequenceNumber	= 1;

	/** static internal message counter */
	private static int			lastUsedInternalMsgId	= 0;


	/**
	 * Utility class. Hides default constructor.
	 */
	private IDGenerator() {

	}

	/**
	 * Returns a RFC-4122 conformant UUID, normal form.
	 * 
	 * @return a RFC-4122 conformant UUID, normal form.
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	/**
	 * Returns the UUID as URI. e.g.
	 * "urn:uuid:550e8400-e29b-11d4-a716-446655440000". URI schemas:
	 * http://www.iana.org/assignments/uri-schemes.html URN namespaces:
	 * http://www.iana.org/assignments/urn-namespaces/
	 * 
	 * @return the UUID URI.
	 */
	public static URI getUUIDasURI() {
		return new URI(URI_UUID_PREFIX + getUUID());
	}

	/**
	 * Returns a sequence number that is unique within a single VM until this VM
	 * is restarted.
	 * 
	 * @return the sequence number
	 */
	public static synchronized long getSequenceNumber() {
		return commonSequenceNumber++;
	}

	/**
	 * Increment static message id.
	 * 
	 * @return internal message id
	 */
	public static synchronized int getStaticMsgId() {
		return ++lastUsedInternalMsgId;
	}

}
