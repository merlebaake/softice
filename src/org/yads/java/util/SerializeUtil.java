/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.util;

import java.io.IOException;

import org.yads.java.io.xml.Ws4dXmlSerializer;
import org.yads.java.xmlpull.v1.IllegalStateException;

public class SerializeUtil {

	/**
	 * Serialize a Tag with String as attribut.
	 * 
	 * @param serializer
	 * @param namespace
	 * @param elementName
	 * @param elementText
	 * @throws IllegalArgumentException
	 * @throws IllegalStateException
	 * @throws IOException
	 */

	public static void serializeTag(Ws4dXmlSerializer serializer, String namespace, String elementName, String elementText) throws IllegalArgumentException, IllegalStateException, IOException {
		serializer.startTag(namespace, elementName);
		serializer.text(elementText);
		serializer.endTag(namespace, elementName);
	}

	/**
	 * Serialize a Tag with attribute.
	 * 
	 * @param serializer
	 * @param namespace
	 * @param elementName
	 * @param elementText
	 * @param attNamespace
	 * @param attName
	 * @param attValue
	 * @throws IllegalArgumentException
	 * @throws IllegalStateException
	 * @throws IOException
	 */

	public static void serializeTagWithAttribute(Ws4dXmlSerializer serializer, String namespace, String elementName, String elementText, String attNamespace, String attName, String attValue) throws IllegalArgumentException, IllegalStateException, IOException {
		serializer.startTag(namespace, elementName);
		serializer.attribute(attNamespace, attName, attValue);
		serializer.text(elementText);
		serializer.endTag(namespace, elementName);
	}
}
