/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.presentation;

import org.yads.java.communication.structures.CommunicationBinding;
import org.yads.java.constants.FrameworkConstants;
import org.yads.java.service.LocalDevice;
import org.yads.java.service.LocalService;
import java.util.Iterator;
import java.util.Set;
import org.yads.java.util.Clazz;
import org.yads.java.util.Log;

/**
 * Device and service presentation.
 */
public abstract class DeviceServicePresentation {

	private static DeviceServicePresentation	instance				= null;

	private static boolean						getInstanceFirstCall	= true;

	public static synchronized DeviceServicePresentation getInstance() {
		if (getInstanceFirstCall) {
			getInstanceFirstCall = false;
			try {
				// default =
				// "org.yads.java.presentation.DefaultDeviceServicePresentation"
				Class clazz = Clazz.forName(FrameworkConstants.DEFAULT_DEVICE_SERVICE_PRESENTATION_PATH);
				instance = ((DeviceServicePresentation) clazz.newInstance());
			} catch (Exception e) {
				if (Log.isDebug()) {
					Log.info("Presentation module not available: " + e.getMessage());
				}
			}
		}
		return instance;
	}

	public abstract void deployForDeviceAt(CommunicationBinding binding, LocalDevice device);

	public abstract void deployForServiceAt(CommunicationBinding binding, LocalService service);

	public abstract void undeployForDeviceAt(CommunicationBinding binding);

	public abstract void undeployForServiceAt(CommunicationBinding binding);

	public abstract void addWSDLLocationsForService(LocalService service, Set addresses);

	public abstract void removeWSDLLocationsForService(LocalService service, Set addresses);

	public abstract Iterator getWSDLLocationsForService(LocalService service);
}
