package org.yads.java.xmlpull.v1;

/**
 *
 */
public class IllegalStateException extends RuntimeException {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -7176635726024702020L;

	/**
	 * 
	 */
	public IllegalStateException() {}

	/**
	 * @param s
	 */
	public IllegalStateException(String s) {
		super(s);
	}

}
