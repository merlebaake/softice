/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.configuration;

import org.yads.java.communication.structures.CommunicationBinding;
import org.yads.java.communication.structures.DiscoveryBinding;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.Toolkit;

/**
 * 
 */
public class BindingProperties implements PropertiesHandler {

	public static final String	PROP_BINDING_ID		= "BindingId";

	public static final Integer	DEFAULT_BINDING_ID	= -1;

	// ------------------------------------------------

	private final HashMap				bindings			= new HashMap();

	private final HashMap				discoveryBindings	= new HashMap();

	// private static BindingProperties handler = null;
	//
	private static String		className			= null;

	// -------------------------------------------------

	public BindingProperties() {
		super();
		// if (handler != null) {
		// throw new
		// RuntimeException("BindingProperties: class already instantiated!");
		// }
		className = this.getClass().getName();
		// handler = this;
	}

	public static BindingProperties getInstance() {
		// if (handler == null) {
		// handler = new BindingProperties();
		// }
		// return handler;
		return (BindingProperties) Properties.forClassName(Properties.BINDING_PROPERTIES_HANDLER_CLASS);
	}

	/**
	 * Returns class name, if object of this class was already created, else
	 * null.
	 * 
	 * @return Class name, if object of this class was already created, else
	 *         null.
	 */
	public static String getClassName() {
		return className;
	}

	// ----------------------------------------------------------------------

	@Override
	public void setProperties(PropertyHeader header, Property property) {
		// no common binding properties
	}

	@Override
	public void finishedSection(int depth) {}

	void addCommunicationBinding(Integer bindingId, ArrayList binding) {
		bindings.put(bindingId, binding);
	}

	void addDiscoveryBinding(Integer bindingId, DiscoveryBinding binding) {
		discoveryBindings.put(bindingId, binding);
	}

	/**
	 * Returns a List with bindings.
	 * 
	 * @param bindingId List with Bindings
	 * @return list with communicationBinding for given binding id
	 */
	public List getCommunicationBinding(Integer bindingId) {
		return (ArrayList) bindings.get(bindingId);
	}

	public CommunicationBinding getDiscoveryBinding(Integer bindingId) {
		return (CommunicationBinding) discoveryBindings.get(bindingId);
	}

	@Override
	public String toString() {
		SimpleStringBuilder out = Toolkit.getInstance().createSimpleStringBuilder(50 * bindings.size());

		for (Iterator it = bindings.entrySet().iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			out.append(entry.getKey()).append('=').append(entry.getValue()).append(" | ");
		}

		return out.toString();
	}

}
