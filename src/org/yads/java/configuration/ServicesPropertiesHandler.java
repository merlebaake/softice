/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.configuration;

import java.util.HashMap;

public class ServicesPropertiesHandler implements PropertiesHandler {

	private HashMap				servProps			= new HashMap();

	private ServiceProperties	buildUpProperties	= null;

	/** default properties for all services */
	private ServiceProperties	defaultProperties	= null;

	ServicesPropertiesHandler() {
		super();
	}

	/**
	 * Returns instance of service properties handler.
	 * 
	 * @return the singleton instance of the service properties
	 */
	public static ServicesPropertiesHandler getInstance() {
		return (ServicesPropertiesHandler) Properties.forClassName(Properties.SERVICES_PROPERTIES_HANDLER_CLASS);
	}

	/**
	 * Gets service properties.
	 * 
	 * @param configurationId
	 * @return service properties
	 */
	public ServiceProperties getServiceProperties(Integer configurationId) {
		return (ServiceProperties) servProps.get(configurationId);
	}

	// -------------------------------------------------------------

	@Override
	public void setProperties(PropertyHeader header, Property property) {

		if (Properties.HEADER_SECTION_SERVICES.equals(header)) {
			// Properties of "Services" Section, default for subsections
			if (defaultProperties == null) {
				defaultProperties = new ServiceProperties();
			}

			defaultProperties.addProperty(property);
		}

		else if (Properties.HEADER_SUBSECTION_SERVICE.equals(header)) {
			// Properties of "Service" Section
			if (buildUpProperties == null) {
				if (defaultProperties != null) {
					buildUpProperties = new ServiceProperties(defaultProperties);
				} else {
					buildUpProperties = new ServiceProperties();
				}
			}

			buildUpProperties.addProperty(property);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.configuration.PropertiesHandler#finishedSection(int)
	 */
	@Override
	public void finishedSection(int depth) {
		if (depth == 2 && buildUpProperties != null) {
			// initialize DeviceProperties
			if (!buildUpProperties.getConfigurationId().equals(ServiceProperties.DEFAULT_CONFIGURATION_ID)) {
				Integer id = buildUpProperties.getConfigurationId();
				servProps.put(id, buildUpProperties);
			}
			buildUpProperties = null;
		} else if (depth <= 1) {
			// remove all management structure, it is not used anymore
			defaultProperties = null;
			buildUpProperties = null;
		}

	}

}
