/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.mdpws;

import java.io.ByteArrayOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.WaveformStream;
import org.ornet.softice.provider.SDCProvider;
import org.w3c.dom.Document;
import org.yads.java.constants.DPWS2009.DPWSConstants2009;
import org.yads.java.constants.DPWS2009.WSDConstants2009;
import org.yads.java.constants.WSAConstants2009;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.types.AppSequence;

class StreamSender {
        
    private final List<DatagramSocket> sockets;
    private final SDCProvider provider;
    private final InetAddress mCastDestination;
    private final int mCastPort;
    private MessageFactory messageFactory;
    private DocumentBuilderFactory docFactory;
	private DocumentBuilder docBuilder;
    
    StreamSender(List<DatagramSocket> sockets, InetAddress mCastDestination, int mCastPort, SDCProvider provider) {
        this.sockets = sockets;
        this.mCastDestination = mCastDestination;
        this.mCastPort = mCastPort;
        this.provider = provider;
        try {
            messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();            
        } catch (Exception ex) {
            Logger.getLogger(StreamSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    SDCProvider getProvider() {
        return provider;
    }

    synchronized void sendAll(RealTimeSampleArrayMetricState state) {
        try {
            SOAPMessage msg = createStreamSOAPMessage(state);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            msg.writeTo(bos);
            byte [] buf = bos.toByteArray();
            for (DatagramSocket socket : sockets) {
                DatagramPacket packet = new DatagramPacket(buf, buf.length, mCastDestination, mCastPort);
                socket.send(packet);                
            }
        } catch (Exception ex) {
            Logger.getLogger(StreamSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private SOAPMessage createStreamSOAPMessage(RealTimeSampleArrayMetricState state) throws Exception {
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("wsa", WSAConstants2009.WSA_NAMESPACE_NAME);
        envelope.addNamespaceDeclaration("dpws", DPWSConstants2009.DPWS_NAMESPACE_NAME);
        envelope.addNamespaceDeclaration("wsd", WSDConstants2009.WSD_NAMESPACE_NAME);
        SOAPHeader header = envelope.getHeader();
        header.addChildElement("MessageID", "wsa").addTextNode(UUID.randomUUID().toString());
        header.addChildElement("Action", "wsa").addTextNode(MDPWSStreamingManager.ACTION_URI);
        header.addChildElement("To", "wsa").addTextNode(MDPWSStreamingManager.PROTOCOL_PREFIX + mCastDestination.getHostAddress() + ":" + mCastPort);
        //header.addChildElement("From", "wsa").addChildElement("Address", "wsa", "").addTextNode(provider.getEndpointReference());
        AppSequence next = provider.getAppSequencer().getNext();
        SOAPElement appElem = header.addChildElement("AppSequence", "wsd").addAttribute(envelope.createName("InstanceId"), Long.toString(next.getInstanceId()));
        appElem.addAttribute(envelope.createName("MessageNumber"), Long.toString(next.getMessageNumber()));
        SOAPBody body = envelope.getBody();
        WaveformStream wfs = new WaveformStream();
        wfs.getState().add(state);
        wfs.setSequenceId("0");
        Document streamDoc = docBuilder.newDocument();
        JAXBUtil.getInstance().marshallTo(wfs, streamDoc);
        body.addDocument(streamDoc);
        soapMessage.saveChanges();
        return soapMessage;
    }

    void close() {
        sockets.stream().forEach((next) -> {
            next.close();
        });
    }
        
}
