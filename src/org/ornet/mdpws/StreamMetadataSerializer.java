/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.mdpws;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import static org.ornet.mdpws.MDPWSStreamingManager.ACTION_URI;
import static org.ornet.mdpws.MDPWSStreamingManager.STREAMING_NAMESPACE;
import static org.ornet.mdpws.MDPWSStreamingManager.STREAM_TYPE;
import static org.ornet.mdpws.MDPWSStreamingManager.TARGET_NAMESPACE;

import org.ornet.cdm.mdpws.*;
import org.yads.java.constants.DPWS2009.WSMEXConstants2009;
import org.yads.java.io.xml.ElementHandler;
import org.yads.java.io.xml.ElementParser;
import org.yads.java.io.xml.Ws4dXmlSerializer;
import org.yads.java.types.QName;
import org.yads.java.util.WS4DIllegalStateException;
import org.yads.java.xmlpull.v1.XmlPullParser;
import org.yads.java.xmlpull.v1.XmlPullParserException;

public class StreamMetadataSerializer {

    public static ElementHandler StreamMEXSerializer() {
        return new ElementHandler() {

            @Override
            public Object handleElement(QName elementName, ElementParser parser) throws XmlPullParserException, IOException {
                String entryName = parser.getName();
                int event = parser.nextTag();
                List<String> addresses = new ArrayList<>();
                while (event != XmlPullParser.END_TAG || !parser.getName().equals(entryName)) {
                    String name = parser.getName();
                    String nameSpace = parser.getNamespace();
                    if (name.equalsIgnoreCase("StreamAddress") && nameSpace.equals(STREAMING_NAMESPACE) && event != XmlPullParser.END_TAG) {
                        parser.next();
                        addresses.add(parser.getText());
                    }
                    if (name.equalsIgnoreCase("StreamPeriod") && nameSpace.equals(STREAMING_NAMESPACE) && event != XmlPullParser.END_TAG) {
                        // Ignore
                        parser.next();
                    }
                    event = parser.nextTag();
                }
                return addresses;
            }

            @Override
            public void serializeElement(Ws4dXmlSerializer serializer, QName qname, Object value) throws IllegalArgumentException, WS4DIllegalStateException, IOException {
                StreamDescriptionsType description = new StreamDescriptionsType();
                description.setTargetNamespace(TARGET_NAMESPACE);

                if (qname.equals(new QName("StreamDescriptions", STREAMING_NAMESPACE))) {
                    for (String nextAddress : (List<String>) value) {
                        StreamTypeType type = new StreamTypeType();
                        type.setActionUri(ACTION_URI);
                        type.setId("WaveforStream");
                        type.setStreamType(STREAM_TYPE);

                        StreamTransmissionType transmission = new StreamTransmissionType();
                        transmission.setStreamAddress(nextAddress);
                        type.setStreamTransmission(transmission);

                        description.getStreamType().add(type);
                    }
                }

                String descriptionString = null;
                try {
                    ObjectFactory factory = new ObjectFactory();

                    // Description marshalling
                    StringWriter sw = new StringWriter();
                    JAXBContext context = JAXBContext.newInstance(StreamDescriptionsType.class);
                    Marshaller m = context.createMarshaller();

                    m.setProperty(Marshaller.JAXB_FRAGMENT, true);
                    m.setProperty("com.sun.xml.internal.bind.xmlHeaders", "");
                    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

                    m.marshal(factory.createStreamDescriptions(description), sw);
                    descriptionString = sw.toString();
                } catch (Exception e) {
                    Logger.getLogger(StreamMetadataSerializer.class.getName()).log(Level.SEVERE, null, e);
                }

                // Build output
                serializer.startTag(WSMEXConstants2009.WSX_NAMESPACE_NAME, "MetadataSection");
                serializer.attribute(null, "Dialect", STREAMING_NAMESPACE);
                serializer.attribute(null, "Identifier", TARGET_NAMESPACE);
                serializer.setPrefix("wsstm", STREAMING_NAMESPACE);

                serializer.plainText(descriptionString);

                serializer.endTag(WSMEXConstants2009.WSX_NAMESPACE_NAME, "MetadataSection");

            }

        };
    }

}
