//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ChannelDescriptor describes a CHANNEL to group METRICs and alerts. It is used for organizational purposes only.
 * 
 * Example: an example would be a blood pressure VMD with one CHANNEL to group together all METRICs that deal with the blood pressure (e.g., pressure value, pressure waveform). A second CHANNEL object could be used to group together METRICs that deal with heart rate.
 * 
 * <p>Java-Klasse für ChannelDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChannelDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractDeviceComponentDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Metric" type="{http://p11073-10207/draft10/pm/2017/10/05}AbstractMetricDescriptor" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChannelDescriptor", propOrder = {
    "metric"
})
public class ChannelDescriptor
    extends AbstractDeviceComponentDescriptor
{

    @XmlElement(name = "Metric")
    protected List<AbstractMetricDescriptor> metric;

    /**
     * Gets the value of the metric property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metric property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetric().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractMetricDescriptor }
     * 
     * 
     */
    public List<AbstractMetricDescriptor> getMetric() {
        if (metric == null) {
            metric = new ArrayList<AbstractMetricDescriptor>();
        }
        return this.metric;
    }

}
