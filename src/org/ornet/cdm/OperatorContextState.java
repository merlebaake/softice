//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A context state that identifies an operator of an MDS or a part of it.
 * 
 * <p>Java-Klasse für OperatorContextState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OperatorContextState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractContextState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OperatorDetails" type="{http://p11073-10207/draft10/pm/2017/10/05}BaseDemographics" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperatorContextState", propOrder = {
    "operatorDetails"
})
public class OperatorContextState
    extends AbstractContextState
{

    @XmlElement(name = "OperatorDetails")
    protected BaseDemographics operatorDetails;

    /**
     * Ruft den Wert der operatorDetails-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BaseDemographics }
     *     
     */
    public BaseDemographics getOperatorDetails() {
        return operatorDetails;
    }

    /**
     * Legt den Wert der operatorDetails-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BaseDemographics }
     *     
     */
    public void setOperatorDetails(BaseDemographics value) {
        this.operatorDetails = value;
    }

}
