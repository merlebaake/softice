//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für LocalizedTextWidth.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="LocalizedTextWidth"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="xs"/&gt;
 *     &lt;enumeration value="s"/&gt;
 *     &lt;enumeration value="m"/&gt;
 *     &lt;enumeration value="l"/&gt;
 *     &lt;enumeration value="xl"/&gt;
 *     &lt;enumeration value="xxl"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LocalizedTextWidth")
@XmlEnum
public enum LocalizedTextWidth {


    /**
     * A line has 4 or less fullwidth characters.
     * 
     */
    @XmlEnumValue("xs")
    XS("xs"),

    /**
     * A line has 8 or less fullwidth characters.
     * 
     */
    @XmlEnumValue("s")
    S("s"),

    /**
     * A line has 12 or less fullwidth characters.
     * 
     */
    @XmlEnumValue("m")
    M("m"),

    /**
     * A line has 16 or less fullwidth characters.
     * 
     */
    @XmlEnumValue("l")
    L("l"),

    /**
     * A line has 20 or less fullwidth characters.
     * 
     */
    @XmlEnumValue("xl")
    XL("xl"),

    /**
     * A line has 21 or more fullwidth characters.
     * 
     */
    @XmlEnumValue("xxl")
    XXL("xxl");
    private final String value;

    LocalizedTextWidth(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LocalizedTextWidth fromValue(String v) {
        for (LocalizedTextWidth c: LocalizedTextWidth.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
