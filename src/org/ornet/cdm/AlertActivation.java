//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AlertActivation.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="AlertActivation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="On"/&gt;
 *     &lt;enumeration value="Off"/&gt;
 *     &lt;enumeration value="Psd"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AlertActivation")
@XmlEnum
public enum AlertActivation {


    /**
     * The ALERT SYSTEM ELEMENT is operating.
     * 
     */
    @XmlEnumValue("On")
    ON("On"),

    /**
     * The ALERT SYSTEM ELEMENT is not operating.
     * 
     */
    @XmlEnumValue("Off")
    OFF("Off"),

    /**
     * Psd = Paused. The ALERT SYSTEM ELEMENT is temporarily not operating.
     * 
     */
    @XmlEnumValue("Psd")
    PSD("Psd");
    private final String value;

    AlertActivation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AlertActivation fromValue(String v) {
        for (AlertActivation c: AlertActivation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
