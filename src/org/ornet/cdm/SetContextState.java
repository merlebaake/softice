//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractSet"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProposedContextState" type="{http://p11073-10207/draft10/pm/2017/10/05}AbstractContextState" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "proposedContextState"
})
@XmlRootElement(name = "SetContextState", namespace = "http://p11073-10207/draft10/msg/2017/10/05")
public class SetContextState
    extends AbstractSet
{

    @XmlElement(name = "ProposedContextState", namespace = "http://p11073-10207/draft10/msg/2017/10/05", required = true)
    protected List<AbstractContextState> proposedContextState;

    /**
     * Gets the value of the proposedContextState property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the proposedContextState property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProposedContextState().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractContextState }
     * 
     * 
     */
    public List<AbstractContextState> getProposedContextState() {
        if (proposedContextState == null) {
            proposedContextState = new ArrayList<AbstractContextState>();
        }
        return this.proposedContextState;
    }

}
