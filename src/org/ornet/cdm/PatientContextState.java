//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Observed information about a patient, e.g., demographics.
 * 
 * NOTE—PatientContextState contains information that is typical for a header in an anamnesis questionnaire.
 * 
 * <p>Java-Klasse für PatientContextState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PatientContextState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractContextState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CoreData" type="{http://p11073-10207/draft10/pm/2017/10/05}PatientDemographicsCoreData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PatientContextState", propOrder = {
    "coreData"
})
public class PatientContextState
    extends AbstractContextState
{

    @XmlElement(name = "CoreData")
    protected PatientDemographicsCoreData coreData;

    /**
     * Ruft den Wert der coreData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PatientDemographicsCoreData }
     *     
     */
    public PatientDemographicsCoreData getCoreData() {
        return coreData;
    }

    /**
     * Legt den Wert der coreData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientDemographicsCoreData }
     *     
     */
    public void setCoreData(PatientDemographicsCoreData value) {
        this.coreData = value;
    }

}
