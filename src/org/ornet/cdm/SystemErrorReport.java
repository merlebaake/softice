//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReport"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReportPart" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReportPart"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ErrorCode" type="{http://p11073-10207/draft10/pm/2017/10/05}CodedValue"/&gt;
 *                   &lt;element name="ErrorInfo" type="{http://p11073-10207/draft10/pm/2017/10/05}LocalizedText" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reportPart"
})
@XmlRootElement(name = "SystemErrorReport", namespace = "http://p11073-10207/draft10/msg/2017/10/05")
public class SystemErrorReport
    extends AbstractReport
{

    @XmlElement(name = "ReportPart", namespace = "http://p11073-10207/draft10/msg/2017/10/05", required = true)
    protected List<SystemErrorReport.ReportPart> reportPart;

    /**
     * Gets the value of the reportPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SystemErrorReport.ReportPart }
     * 
     * 
     */
    public List<SystemErrorReport.ReportPart> getReportPart() {
        if (reportPart == null) {
            reportPart = new ArrayList<SystemErrorReport.ReportPart>();
        }
        return this.reportPart;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReportPart"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ErrorCode" type="{http://p11073-10207/draft10/pm/2017/10/05}CodedValue"/&gt;
     *         &lt;element name="ErrorInfo" type="{http://p11073-10207/draft10/pm/2017/10/05}LocalizedText" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "errorCode",
        "errorInfo"
    })
    public static class ReportPart
        extends AbstractReportPart
    {

        @XmlElement(name = "ErrorCode", namespace = "http://p11073-10207/draft10/msg/2017/10/05", required = true)
        protected CodedValue errorCode;
        @XmlElement(name = "ErrorInfo", namespace = "http://p11073-10207/draft10/msg/2017/10/05")
        protected LocalizedText errorInfo;

        /**
         * Ruft den Wert der errorCode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link CodedValue }
         *     
         */
        public CodedValue getErrorCode() {
            return errorCode;
        }

        /**
         * Legt den Wert der errorCode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link CodedValue }
         *     
         */
        public void setErrorCode(CodedValue value) {
            this.errorCode = value;
        }

        /**
         * Ruft den Wert der errorInfo-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link LocalizedText }
         *     
         */
        public LocalizedText getErrorInfo() {
            return errorInfo;
        }

        /**
         * Legt den Wert der errorInfo-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link LocalizedText }
         *     
         */
        public void setErrorInfo(LocalizedText value) {
            this.errorInfo = value;
        }

    }

}
