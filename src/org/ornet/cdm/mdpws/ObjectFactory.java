//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:58 PM CEST 
//


package org.ornet.cdm.mdpws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.ornet.cdm.mdpws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StreamSource_QNAME = new QName("http://standards.ieee.org/downloads/11073/11073-20702-201x/", "StreamSource");
    private final static QName _StreamDescriptions_QNAME = new QName("http://standards.ieee.org/downloads/11073/11073-20702-201x/", "StreamDescriptions");
    private final static QName _SafetyReqAssertion_QNAME = new QName("http://standards.ieee.org/downloads/11073/11073-20702-201x/", "SafetyReqAssertion");
    private final static QName _SafetyReq_QNAME = new QName("http://standards.ieee.org/downloads/11073/11073-20702-201x/", "SafetyReq");
    private final static QName _SafetyInfo_QNAME = new QName("http://standards.ieee.org/downloads/11073/11073-20702-201x/", "SafetyInfo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.ornet.cdm.mdpws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StreamDescriptionsType }
     * 
     */
    public StreamDescriptionsType createStreamDescriptionsType() {
        return new StreamDescriptionsType();
    }

    /**
     * Create an instance of {@link StreamSourceType }
     * 
     */
    public StreamSourceType createStreamSourceType() {
        return new StreamSourceType();
    }

    /**
     * Create an instance of {@link SafetyReqAssertionType }
     * 
     */
    public SafetyReqAssertionType createSafetyReqAssertionType() {
        return new SafetyReqAssertionType();
    }

    /**
     * Create an instance of {@link SafetyReqType }
     * 
     */
    public SafetyReqType createSafetyReqType() {
        return new SafetyReqType();
    }

    /**
     * Create an instance of {@link SafetyInfoType }
     * 
     */
    public SafetyInfoType createSafetyInfoType() {
        return new SafetyInfoType();
    }

    /**
     * Create an instance of {@link StreamTransmissionType }
     * 
     */
    public StreamTransmissionType createStreamTransmissionType() {
        return new StreamTransmissionType();
    }

    /**
     * Create an instance of {@link StreamTypeType }
     * 
     */
    public StreamTypeType createStreamTypeType() {
        return new StreamTypeType();
    }

    /**
     * Create an instance of {@link SelectorType }
     * 
     */
    public SelectorType createSelectorType() {
        return new SelectorType();
    }

    /**
     * Create an instance of {@link DualChannelDefType }
     * 
     */
    public DualChannelDefType createDualChannelDefType() {
        return new DualChannelDefType();
    }

    /**
     * Create an instance of {@link SafetyContextDefType }
     * 
     */
    public SafetyContextDefType createSafetyContextDefType() {
        return new SafetyContextDefType();
    }

    /**
     * Create an instance of {@link DualChannelType }
     * 
     */
    public DualChannelType createDualChannelType() {
        return new DualChannelType();
    }

    /**
     * Create an instance of {@link SafetyContextType }
     * 
     */
    public SafetyContextType createSafetyContextType() {
        return new SafetyContextType();
    }

    /**
     * Create an instance of {@link DcValueType }
     * 
     */
    public DcValueType createDcValueType() {
        return new DcValueType();
    }

    /**
     * Create an instance of {@link CtxtValueType }
     * 
     */
    public CtxtValueType createCtxtValueType() {
        return new CtxtValueType();
    }

    /**
     * Create an instance of {@link StreamDescriptionsType.Types }
     * 
     */
    public StreamDescriptionsType.Types createStreamDescriptionsTypeTypes() {
        return new StreamDescriptionsType.Types();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StreamSourceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://standards.ieee.org/downloads/11073/11073-20702-201x/", name = "StreamSource")
    public JAXBElement<StreamSourceType> createStreamSource(StreamSourceType value) {
        return new JAXBElement<StreamSourceType>(_StreamSource_QNAME, StreamSourceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StreamDescriptionsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://standards.ieee.org/downloads/11073/11073-20702-201x/", name = "StreamDescriptions")
    public JAXBElement<StreamDescriptionsType> createStreamDescriptions(StreamDescriptionsType value) {
        return new JAXBElement<StreamDescriptionsType>(_StreamDescriptions_QNAME, StreamDescriptionsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SafetyReqAssertionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://standards.ieee.org/downloads/11073/11073-20702-201x/", name = "SafetyReqAssertion")
    public JAXBElement<SafetyReqAssertionType> createSafetyReqAssertion(SafetyReqAssertionType value) {
        return new JAXBElement<SafetyReqAssertionType>(_SafetyReqAssertion_QNAME, SafetyReqAssertionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SafetyReqType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://standards.ieee.org/downloads/11073/11073-20702-201x/", name = "SafetyReq")
    public JAXBElement<SafetyReqType> createSafetyReq(SafetyReqType value) {
        return new JAXBElement<SafetyReqType>(_SafetyReq_QNAME, SafetyReqType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SafetyInfoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://standards.ieee.org/downloads/11073/11073-20702-201x/", name = "SafetyInfo")
    public JAXBElement<SafetyInfoType> createSafetyInfo(SafetyInfoType value) {
        return new JAXBElement<SafetyInfoType>(_SafetyInfo_QNAME, SafetyInfoType.class, null, value);
    }

}
