//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * State of an operation that is exposed on the SCO.
 * 
 * <p>Java-Klasse für AbstractOperationState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractOperationState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractState"&gt;
 *       &lt;attribute name="OperatingMode" use="required" type="{http://p11073-10207/draft10/pm/2017/10/05}OperatingMode" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractOperationState")
@XmlSeeAlso({
    SetValueOperationState.class,
    SetStringOperationState.class,
    ActivateOperationState.class,
    SetContextStateOperationState.class,
    SetMetricStateOperationState.class,
    SetComponentStateOperationState.class,
    SetAlertStateOperationState.class
})
public class AbstractOperationState
    extends AbstractState
{

    @XmlAttribute(name = "OperatingMode", required = true)
    protected OperatingMode operatingMode;

    /**
     * Ruft den Wert der operatingMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OperatingMode }
     *     
     */
    public OperatingMode getOperatingMode() {
        return operatingMode;
    }

    /**
     * Legt den Wert der operatingMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OperatingMode }
     *     
     */
    public void setOperatingMode(OperatingMode value) {
        this.operatingMode = value;
    }

}
