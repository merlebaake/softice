//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * LimitAlertConditionDescriptor is a specialization of an ALERT CONDITION that is active if at least one limit for a referenced METRIC has been violated.
 * 
 * <p>Java-Klasse für LimitAlertConditionDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LimitAlertConditionDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AlertConditionDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MaxLimits" type="{http://p11073-10207/draft10/pm/2017/10/05}Range"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="AutoLimitSupported" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LimitAlertConditionDescriptor", propOrder = {
    "maxLimits"
})
public class LimitAlertConditionDescriptor
    extends AlertConditionDescriptor
{

    @XmlElement(name = "MaxLimits", required = true)
    protected Range maxLimits;
    @XmlAttribute(name = "AutoLimitSupported")
    protected Boolean autoLimitSupported;

    /**
     * Ruft den Wert der maxLimits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Range }
     *     
     */
    public Range getMaxLimits() {
        return maxLimits;
    }

    /**
     * Legt den Wert der maxLimits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Range }
     *     
     */
    public void setMaxLimits(Range value) {
        this.maxLimits = value;
    }

    /**
     * Ruft den Wert der autoLimitSupported-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoLimitSupported() {
        return autoLimitSupported;
    }

    /**
     * Legt den Wert der autoLimitSupported-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoLimitSupported(Boolean value) {
        this.autoLimitSupported = value;
    }

}
