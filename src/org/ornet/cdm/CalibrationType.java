//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CalibrationType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="CalibrationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Offset"/&gt;
 *     &lt;enumeration value="Gain"/&gt;
 *     &lt;enumeration value="TP"/&gt;
 *     &lt;enumeration value="Unspec"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CalibrationType")
@XmlEnum
public enum CalibrationType {


    /**
     * Offset calibration.
     * 
     */
    @XmlEnumValue("Offset")
    OFFSET("Offset"),

    /**
     * Gain calibration
     * 
     */
    @XmlEnumValue("Gain")
    GAIN("Gain"),

    /**
     * Two point calibration.
     * 
     */
    TP("TP"),

    /**
     * Unspecified calibration type.
     * 
     */
    @XmlEnumValue("Unspec")
    UNSPEC("Unspec");
    private final String value;

    CalibrationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CalibrationType fromValue(String v) {
        for (CalibrationType c: CalibrationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
