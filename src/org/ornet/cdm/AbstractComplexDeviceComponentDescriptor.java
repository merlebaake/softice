//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * AbstractComplexDeviceComponentDescriptor adds an OPTIONAL pm:AlertSystemDescriptor and pm:ScoDescriptor to pm:AbstractDeviceComponentDescriptor.
 * 
 * <p>Java-Klasse für AbstractComplexDeviceComponentDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractComplexDeviceComponentDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractDeviceComponentDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AlertSystem" type="{http://p11073-10207/draft10/pm/2017/10/05}AlertSystemDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="Sco" type="{http://p11073-10207/draft10/pm/2017/10/05}ScoDescriptor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractComplexDeviceComponentDescriptor", propOrder = {
    "alertSystem",
    "sco"
})
@XmlSeeAlso({
    MdsDescriptor.class,
    VmdDescriptor.class
})
public class AbstractComplexDeviceComponentDescriptor
    extends AbstractDeviceComponentDescriptor
{

    @XmlElement(name = "AlertSystem")
    protected AlertSystemDescriptor alertSystem;
    @XmlElement(name = "Sco")
    protected ScoDescriptor sco;

    /**
     * Ruft den Wert der alertSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertSystemDescriptor }
     *     
     */
    public AlertSystemDescriptor getAlertSystem() {
        return alertSystem;
    }

    /**
     * Legt den Wert der alertSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertSystemDescriptor }
     *     
     */
    public void setAlertSystem(AlertSystemDescriptor value) {
        this.alertSystem = value;
    }

    /**
     * Ruft den Wert der sco-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ScoDescriptor }
     *     
     */
    public ScoDescriptor getSco() {
        return sco;
    }

    /**
     * Legt den Wert der sco-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ScoDescriptor }
     *     
     */
    public void setSco(ScoDescriptor value) {
        this.sco = value;
    }

}
