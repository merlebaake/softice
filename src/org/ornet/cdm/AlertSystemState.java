//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * AlertSystemState contains the dynamic/volatile information of an ALERT SYSTEM. See pm:AlertSystemDescriptor for static information.
 * 
 * <p>Java-Klasse für AlertSystemState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlertSystemState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractAlertState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SystemSignalActivation" type="{http://p11073-10207/draft10/pm/2017/10/05}SystemSignalActivation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="LastSelfCheck" type="{http://p11073-10207/draft10/pm/2017/10/05}Timestamp" /&gt;
 *       &lt;attribute name="SelfCheckCount" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="PresentPhysiologicalAlarmConditions" type="{http://p11073-10207/draft10/pm/2017/10/05}AlertConditionReference" /&gt;
 *       &lt;attribute name="PresentTechnicalAlarmConditions" type="{http://p11073-10207/draft10/pm/2017/10/05}AlertConditionReference" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertSystemState", propOrder = {
    "systemSignalActivation"
})
public class AlertSystemState
    extends AbstractAlertState
{

    @XmlElement(name = "SystemSignalActivation")
    protected List<SystemSignalActivation> systemSignalActivation;
    @XmlAttribute(name = "LastSelfCheck")
    protected BigInteger lastSelfCheck;
    @XmlAttribute(name = "SelfCheckCount")
    protected Long selfCheckCount;
    @XmlAttribute(name = "PresentPhysiologicalAlarmConditions")
    protected List<String> presentPhysiologicalAlarmConditions;
    @XmlAttribute(name = "PresentTechnicalAlarmConditions")
    protected List<String> presentTechnicalAlarmConditions;

    /**
     * Gets the value of the systemSignalActivation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the systemSignalActivation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSystemSignalActivation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SystemSignalActivation }
     * 
     * 
     */
    public List<SystemSignalActivation> getSystemSignalActivation() {
        if (systemSignalActivation == null) {
            systemSignalActivation = new ArrayList<SystemSignalActivation>();
        }
        return this.systemSignalActivation;
    }

    /**
     * Ruft den Wert der lastSelfCheck-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLastSelfCheck() {
        return lastSelfCheck;
    }

    /**
     * Legt den Wert der lastSelfCheck-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLastSelfCheck(BigInteger value) {
        this.lastSelfCheck = value;
    }

    /**
     * Ruft den Wert der selfCheckCount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSelfCheckCount() {
        return selfCheckCount;
    }

    /**
     * Legt den Wert der selfCheckCount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSelfCheckCount(Long value) {
        this.selfCheckCount = value;
    }

    /**
     * Gets the value of the presentPhysiologicalAlarmConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the presentPhysiologicalAlarmConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPresentPhysiologicalAlarmConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPresentPhysiologicalAlarmConditions() {
        if (presentPhysiologicalAlarmConditions == null) {
            presentPhysiologicalAlarmConditions = new ArrayList<String>();
        }
        return this.presentPhysiologicalAlarmConditions;
    }

    /**
     * Gets the value of the presentTechnicalAlarmConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the presentTechnicalAlarmConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPresentTechnicalAlarmConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPresentTechnicalAlarmConditions() {
        if (presentTechnicalAlarmConditions == null) {
            presentTechnicalAlarmConditions = new ArrayList<String>();
        }
        return this.presentTechnicalAlarmConditions;
    }

}
