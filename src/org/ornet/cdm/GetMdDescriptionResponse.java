//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractGetResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MdDescription" type="{http://p11073-10207/draft10/pm/2017/10/05}MdDescription"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mdDescription"
})
@XmlRootElement(name = "GetMdDescriptionResponse", namespace = "http://p11073-10207/draft10/msg/2017/10/05")
public class GetMdDescriptionResponse
    extends AbstractGetResponse
{

    @XmlElement(name = "MdDescription", namespace = "http://p11073-10207/draft10/msg/2017/10/05", required = true)
    protected MdDescription mdDescription;

    /**
     * Ruft den Wert der mdDescription-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MdDescription }
     *     
     */
    public MdDescription getMdDescription() {
        return mdDescription;
    }

    /**
     * Legt den Wert der mdDescription-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MdDescription }
     *     
     */
    public void setMdDescription(MdDescription value) {
        this.mdDescription = value;
    }

}
