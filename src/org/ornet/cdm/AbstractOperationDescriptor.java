//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * Abstract description of an operation that is exposed on the SCO.
 * 
 * <p>Java-Klasse für AbstractOperationDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractOperationDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractDescriptor"&gt;
 *       &lt;attribute name="OperationTarget" use="required" type="{http://p11073-10207/draft10/pm/2017/10/05}HandleRef" /&gt;
 *       &lt;attribute name="MaxTimeToFinish" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *       &lt;attribute name="InvocationEffectiveTimeout" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *       &lt;attribute name="Retriggerable" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *       &lt;attribute name="AccessLevel"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="Usr"/&gt;
 *             &lt;enumeration value="CSUsr"/&gt;
 *             &lt;enumeration value="RO"/&gt;
 *             &lt;enumeration value="SP"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractOperationDescriptor")
@XmlSeeAlso({
    SetValueOperationDescriptor.class,
    SetStringOperationDescriptor.class,
    AbstractSetStateOperationDescriptor.class
})
public class AbstractOperationDescriptor
    extends AbstractDescriptor
{

    @XmlAttribute(name = "OperationTarget", required = true)
    protected String operationTarget;
    @XmlAttribute(name = "MaxTimeToFinish")
    protected Duration maxTimeToFinish;
    @XmlAttribute(name = "InvocationEffectiveTimeout")
    protected Duration invocationEffectiveTimeout;
    @XmlAttribute(name = "Retriggerable")
    protected Duration retriggerable;
    @XmlAttribute(name = "AccessLevel")
    protected String accessLevel;

    /**
     * Ruft den Wert der operationTarget-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationTarget() {
        return operationTarget;
    }

    /**
     * Legt den Wert der operationTarget-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationTarget(String value) {
        this.operationTarget = value;
    }

    /**
     * Ruft den Wert der maxTimeToFinish-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMaxTimeToFinish() {
        return maxTimeToFinish;
    }

    /**
     * Legt den Wert der maxTimeToFinish-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMaxTimeToFinish(Duration value) {
        this.maxTimeToFinish = value;
    }

    /**
     * Ruft den Wert der invocationEffectiveTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getInvocationEffectiveTimeout() {
        return invocationEffectiveTimeout;
    }

    /**
     * Legt den Wert der invocationEffectiveTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setInvocationEffectiveTimeout(Duration value) {
        this.invocationEffectiveTimeout = value;
    }

    /**
     * Ruft den Wert der retriggerable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getRetriggerable() {
        return retriggerable;
    }

    /**
     * Legt den Wert der retriggerable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setRetriggerable(Duration value) {
        this.retriggerable = value;
    }

    /**
     * Ruft den Wert der accessLevel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessLevel() {
        return accessLevel;
    }

    /**
     * Legt den Wert der accessLevel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessLevel(String value) {
        this.accessLevel = value;
    }

}
