//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Base type of a context state. Every context state can be identified as valid by a validator instance. Moreover, a context state's lifecycle is determined by a start and end. AbstractContextState bundles these information.
 * 
 * <p>Java-Klasse für AbstractContextState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractContextState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractMultiState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Validator" type="{http://p11073-10207/draft10/pm/2017/10/05}InstanceIdentifier" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Identification" type="{http://p11073-10207/draft10/pm/2017/10/05}InstanceIdentifier" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ContextAssociation" type="{http://p11073-10207/draft10/pm/2017/10/05}ContextAssociation" /&gt;
 *       &lt;attribute name="BindingMdibVersion" type="{http://p11073-10207/draft10/pm/2017/10/05}ReferencedVersion" /&gt;
 *       &lt;attribute name="UnbindingMdibVersion" type="{http://p11073-10207/draft10/pm/2017/10/05}ReferencedVersion" /&gt;
 *       &lt;attribute name="BindingStartTime" type="{http://p11073-10207/draft10/pm/2017/10/05}Timestamp" /&gt;
 *       &lt;attribute name="BindingEndTime" type="{http://p11073-10207/draft10/pm/2017/10/05}Timestamp" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractContextState", propOrder = {
    "validator",
    "identification"
})
@XmlSeeAlso({
    PatientContextState.class,
    LocationContextState.class,
    WorkflowContextState.class,
    OperatorContextState.class,
    MeansContextState.class,
    EnsembleContextState.class
})
public class AbstractContextState
    extends AbstractMultiState
{

    @XmlElement(name = "Validator")
    protected List<InstanceIdentifier> validator;
    @XmlElement(name = "Identification")
    protected List<InstanceIdentifier> identification;
    @XmlAttribute(name = "ContextAssociation")
    protected ContextAssociation contextAssociation;
    @XmlAttribute(name = "BindingMdibVersion")
    protected BigInteger bindingMdibVersion;
    @XmlAttribute(name = "UnbindingMdibVersion")
    protected BigInteger unbindingMdibVersion;
    @XmlAttribute(name = "BindingStartTime")
    protected BigInteger bindingStartTime;
    @XmlAttribute(name = "BindingEndTime")
    protected BigInteger bindingEndTime;

    /**
     * Gets the value of the validator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstanceIdentifier }
     * 
     * 
     */
    public List<InstanceIdentifier> getValidator() {
        if (validator == null) {
            validator = new ArrayList<InstanceIdentifier>();
        }
        return this.validator;
    }

    /**
     * Gets the value of the identification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstanceIdentifier }
     * 
     * 
     */
    public List<InstanceIdentifier> getIdentification() {
        if (identification == null) {
            identification = new ArrayList<InstanceIdentifier>();
        }
        return this.identification;
    }

    /**
     * Ruft den Wert der contextAssociation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContextAssociation }
     *     
     */
    public ContextAssociation getContextAssociation() {
        return contextAssociation;
    }

    /**
     * Legt den Wert der contextAssociation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextAssociation }
     *     
     */
    public void setContextAssociation(ContextAssociation value) {
        this.contextAssociation = value;
    }

    /**
     * Ruft den Wert der bindingMdibVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBindingMdibVersion() {
        return bindingMdibVersion;
    }

    /**
     * Legt den Wert der bindingMdibVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBindingMdibVersion(BigInteger value) {
        this.bindingMdibVersion = value;
    }

    /**
     * Ruft den Wert der unbindingMdibVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnbindingMdibVersion() {
        return unbindingMdibVersion;
    }

    /**
     * Legt den Wert der unbindingMdibVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnbindingMdibVersion(BigInteger value) {
        this.unbindingMdibVersion = value;
    }

    /**
     * Ruft den Wert der bindingStartTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBindingStartTime() {
        return bindingStartTime;
    }

    /**
     * Legt den Wert der bindingStartTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBindingStartTime(BigInteger value) {
        this.bindingStartTime = value;
    }

    /**
     * Ruft den Wert der bindingEndTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBindingEndTime() {
        return bindingEndTime;
    }

    /**
     * Legt den Wert der bindingEndTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBindingEndTime(BigInteger value) {
        this.bindingEndTime = value;
    }

}
