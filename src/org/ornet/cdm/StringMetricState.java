//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * State of a string METRIC.
 * 
 * <p>Java-Klasse für StringMetricState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="StringMetricState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractMetricState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MetricValue" type="{http://p11073-10207/draft10/pm/2017/10/05}StringMetricValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StringMetricState", propOrder = {
    "metricValue"
})
@XmlSeeAlso({
    EnumStringMetricState.class
})
public class StringMetricState
    extends AbstractMetricState
{

    @XmlElement(name = "MetricValue")
    protected StringMetricValue metricValue;

    /**
     * Ruft den Wert der metricValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link StringMetricValue }
     *     
     */
    public StringMetricValue getMetricValue() {
        return metricValue;
    }

    /**
     * Legt den Wert der metricValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link StringMetricValue }
     *     
     */
    public void setMetricValue(StringMetricValue value) {
        this.metricValue = value;
    }

}
