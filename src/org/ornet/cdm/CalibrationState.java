//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für CalibrationState.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="CalibrationState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="No"/&gt;
 *     &lt;enumeration value="Req"/&gt;
 *     &lt;enumeration value="Cal"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CalibrationState")
@XmlEnum
public enum CalibrationState {


    /**
     * No = Not Calibrated. Defines that a component is not calibrated.
     * 
     */
    @XmlEnumValue("No")
    NO("No"),

    /**
     * Req = Calibration Required. Defines that a component requires a calibration.
     * 
     */
    @XmlEnumValue("Req")
    REQ("Req"),

    /**
     * Cal = Calibrated. Defines that a component is calibrated.
     * 
     */
    @XmlEnumValue("Cal")
    CAL("Cal");
    private final String value;

    CalibrationState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CalibrationState fromValue(String v) {
        for (CalibrationState c: CalibrationState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
