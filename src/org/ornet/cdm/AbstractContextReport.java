//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * AbstractContextReport is a change report that contains updated pm:AbstractContextState instances.
 * 
 * <p>Java-Klasse für AbstractContextReport complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractContextReport"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReport"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReportPart" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReportPart"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ContextState" type="{http://p11073-10207/draft10/pm/2017/10/05}AbstractContextState" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractContextReport", namespace = "http://p11073-10207/draft10/msg/2017/10/05", propOrder = {
    "reportPart"
})
@XmlSeeAlso({
    EpisodicContextReport.class,
    PeriodicContextReport.class
})
public class AbstractContextReport
    extends AbstractReport
{

    @XmlElement(name = "ReportPart")
    protected List<AbstractContextReport.ReportPart> reportPart;

    /**
     * Gets the value of the reportPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractContextReport.ReportPart }
     * 
     * 
     */
    public List<AbstractContextReport.ReportPart> getReportPart() {
        if (reportPart == null) {
            reportPart = new ArrayList<AbstractContextReport.ReportPart>();
        }
        return this.reportPart;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReportPart"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ContextState" type="{http://p11073-10207/draft10/pm/2017/10/05}AbstractContextState" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contextState"
    })
    public static class ReportPart
        extends AbstractReportPart
    {

        @XmlElement(name = "ContextState", namespace = "http://p11073-10207/draft10/msg/2017/10/05")
        protected List<AbstractContextState> contextState;

        /**
         * Gets the value of the contextState property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contextState property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContextState().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AbstractContextState }
         * 
         * 
         */
        public List<AbstractContextState> getContextState() {
            if (contextState == null) {
                contextState = new ArrayList<AbstractContextState>();
            }
            return this.contextState;
        }

    }

}
