/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */

package org.ornet.softice.provider;

import org.ornet.softice.SDCToolbox;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AbstractSet;
import org.ornet.cdm.AbstractSetResponse;
import org.ornet.cdm.Activate;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.AbstractOperationState;
import org.ornet.cdm.InvocationInfo;
import org.ornet.cdm.SetAlertState;
import org.ornet.cdm.SetContextState;
import org.ornet.cdm.SetString;
import org.ornet.cdm.SetValue;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.OperatingMode;
import org.ornet.cdm.StringMetricState;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.service.Operation;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.structures.MaxHashMap;

public class ProviderSetOperationFactory {
    
    private static class SetableStateHandler {
        public SDCProviderMDStateHandler handler;
        public AbstractState state;
    }
    
    private static final AtomicLong transactionId = new AtomicLong();
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    private static final LinkedBlockingQueue<SetNotification> queue = new LinkedBlockingQueue<>();
    private static final Map<String, SetableStateHandler> checkStateCache = Collections.synchronizedMap(new MaxHashMap<>(64));
    
    public static void init() {
        if (executor.isShutdown())
            return;
        executor.execute(new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        SetNotification sn = queue.poll(100, TimeUnit.MILLISECONDS);
                        if (sn == null)
                            continue;
                        if (super.isInterrupted())
                            break;
                        Object request = sn.getRequest();
                        if (request instanceof SetValue) {
                            trySetValue(sn.getProvider(), (SetValue)request, sn.getContext());
                        }
                        else if (request instanceof SetString) {
                            trySetString(sn.getProvider(), (SetString)request, sn.getContext());
                        }                        
                        else if (request instanceof SetContextState) {
                            trySetContext(sn.getProvider(), (SetContextState)request, sn.getContext());
                        }                        
                        else if (request instanceof SetAlertState) {
                            trySetAlert(sn.getProvider(), (SetAlertState)request, sn.getContext());
                        }                        
                        else if (request instanceof Activate) {
                            tryActivate(sn.getProvider(), (Activate)request, sn.getContext());
                        }                        
                    } catch (InterruptedException ie) {
                        break;
                    } 
                    catch (Exception ex) {
                        Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }
    
    protected static void shutdown() {
        executor.shutdownNow();
    }
 
    private static class SetNotification<R> {
        private final R request;
        private final OperationInvocationContext context;
        private final SDCProvider provider;

        public SetNotification(SDCProvider provider, R request, OperationInvocationContext context) {
            this.provider = provider;
            this.request = request;
            this.context = context;
        }

        public R getRequest() {
            return request;
        }

        public OperationInvocationContext getContext() {
            return context;
        }

        public SDCProvider getProvider() {
            return provider;
        }
    }
    
    protected static void updateSetableStateCache(SDCProvider provider, AbstractState state) {
        executor.execute(() -> {
            String oh = SDCToolbox.getFirstOperationHandleForOperationTarget(provider, state.getDescriptorHandle());
            if (oh == null) {
                // State was never intended to be setable due to missing SCO, skip this
                return;
            }
            OperationInvocationContext c = new OperationInvocationContext(oh, state.getDescriptorHandle(), 0);
            getSetableStateHandler(provider, c);
        });
    }
    
    private static SetableStateHandler getSetableStateHandler(SDCProvider provider, OperationInvocationContext context) {
        SetableStateHandler sh = checkStateCache.get(context.getOperationHandle());
        if (sh == null) {
            String targetHandle = SDCToolbox.getOperationTargetForOperationHandle(provider, context.getOperationHandle());
            if (targetHandle == null) {
                provider.notifyOperationInvoked(context, InvocationState.FAIL, "No set-operation descriptor found");
                return null;
            }
            AbstractOperationState os = SDCToolbox.findState(provider, context.getOperationHandle(), AbstractOperationState.class);
            if (os == null || os.getOperatingMode() != OperatingMode.EN) {
                provider.notifyOperationInvoked(context, InvocationState.FAIL, "No operation target state or wrong operational state (must be enabled)");
                return null;
            }
            sh = new SetableStateHandler();
            sh.state = SDCToolbox.findState(provider, targetHandle, AbstractState.class);
            if (sh.state == null) {
                provider.notifyOperationInvoked(context, InvocationState.FAIL, "No state (probably wrong handle)");
                return null;
            }
            if (sh.state instanceof AbstractMetricState) {
                if (!SDCToolbox.isMetricChangeAllowed(provider, (AbstractMetricState) sh.state)) {
                    provider.notifyOperationInvoked(context, InvocationState.FAIL, "Requirements of metric violated");
                    return null;
                }             
            }
            Map<String, SDCProviderHandler> handlers = provider.getStateHandlers();
            for (Entry<String, SDCProviderHandler> nextEntry : handlers.entrySet()) {
               if (nextEntry.getKey().equals(targetHandle) && nextEntry.getValue() instanceof SDCProviderMDStateHandler) {        
                   sh.handler = (SDCProviderMDStateHandler)nextEntry.getValue();
                   break;
               }
            }
            if (sh.handler == null) {
                provider.notifyOperationInvoked(context, InvocationState.FAIL, "No state handler found on provider's side");
                return null;
            }
            // State is setable
            checkStateCache.put(context.getOperationHandle(), sh);            
        }
        return sh;
    }  
    
    private static void trySetState(SDCProviderMDStateHandler handler, AbstractState state, OperationInvocationContext context, SDCProvider provider) {
        InvocationState is = handler.onStateChangeRequest(state, context);
        if (is.equals(InvocationState.FIN)) {
            provider.updateState(state);
            provider.notifyOperationInvoked(context, InvocationState.FIN, null);
        } else {
            provider.notifyOperationInvoked(context, is, null);
        }
    }    
    
    private static void trySetValue(SDCProvider provider, SetValue sv, OperationInvocationContext context) {
        SetableStateHandler sh;
        if ((sh = getSetableStateHandler(provider, context)) == null)
            return;
        provider.notifyOperationInvoked(context, InvocationState.START, null);
        NumericMetricState nms = (NumericMetricState)sh.state;
        if (nms.getMetricValue() == null) {
            provider.notifyOperationInvoked(context, InvocationState.FAIL, "Observed value is null");
            return;
        }
        nms.getMetricValue().setValue(sv.getRequestedNumericValue());
        trySetState(sh.handler, nms, context, provider);
    }
    
    private static void trySetString(SDCProvider provider, SetString ss, OperationInvocationContext context) {
        SetableStateHandler sh;
        if ((sh = getSetableStateHandler(provider, context)) == null)
            return;
        provider.notifyOperationInvoked(context, InvocationState.START, null);
        StringMetricState sms = (StringMetricState)sh.state;
        if (sms.getMetricValue() == null) {
            provider.notifyOperationInvoked(context, InvocationState.FAIL, "Observed value is null");
            return;
        }
        sms.getMetricValue().setValue(ss.getRequestedStringValue());
        trySetState(sh.handler, sms, context, provider);
    }    
    
    private static void trySetContext(SDCProvider provider, SetContextState sc, OperationInvocationContext context) {
        for (AbstractContextState nextCS : sc.getProposedContextState()) {
            SetableStateHandler sh;
            if ((sh = getSetableStateHandler(provider, context)) != null) {
                provider.notifyOperationInvoked(context, InvocationState.START, null);
                trySetState(sh.handler, nextCS, context, provider);           
            }
        }
    }
    
    private static void trySetAlert(SDCProvider provider, SetAlertState sa, OperationInvocationContext context) {
        SetableStateHandler sh;
        if ((sh = getSetableStateHandler(provider, context)) == null)
            return;
        provider.notifyOperationInvoked(context, InvocationState.START, null);
        trySetState(sh.handler, sa.getProposedAlertState(), context, provider);       
    }    
    
    private static void tryActivate(SDCProvider provider, Activate activate, OperationInvocationContext context) {
        SDCProviderActivateOperationHandler handler = null;
        Map<String, SDCProviderHandler> handlers = provider.getStateHandlers();
        for (Entry<String, SDCProviderHandler> nextEntry : handlers.entrySet()) {
           if (nextEntry.getKey().equals(context.getOperationHandle()) && nextEntry.getValue() instanceof SDCProviderActivateOperationHandler) {        
               handler = (SDCProviderActivateOperationHandler)nextEntry.getValue();
               break;
           }
        }
        if (handler == null) {
            provider.notifyOperationInvoked(context, InvocationState.FAIL, "Activate handler is null");
            return;
        }        
        provider.notifyOperationInvoked(context, InvocationState.START, null);
        InvocationState is = handler.onActivateRequest(provider.getMDIB(), context);
        if (is.equals(InvocationState.FIN)) {
            provider.notifyOperationInvoked(context, InvocationState.FIN, null);
        } else {
            provider.notifyOperationInvoked(context, is, null);
        }        
    }    
    
    static <U extends AbstractSet, V extends AbstractSetResponse> InvokeDelegate createOSCPSetOperation(final SDCProvider provider, final Class<U> req, final Class<V> res) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            U request = JAXBUtil.getInstance().createInputParameterValue(arguments, req);
            long currentTid = queueRequest(provider, request);
            
            V response;
            try {
                response = res.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(ProviderSetOperationFactory.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            prepareResponse(response, currentTid);
            response.setMdibVersion(BigInteger.valueOf(provider.getMdibVersion()));
            response.setSequenceId("0");
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }
    
    private static long queueRequest(final SDCProvider provider, AbstractSet request) {
        long currentTid = transactionId.addAndGet(1);
        String opTarget = SDCToolbox.getOperationTargetForOperationHandle(provider, request.getOperationHandleRef());
        OperationInvocationContext oic = new OperationInvocationContext(request.getOperationHandleRef(), opTarget, currentTid);
        provider.notifyOperationInvoked(oic, InvocationState.WAIT, null);
        queue.add(new SetNotification<>(provider, request, oic));
        return currentTid;
    }    
    
    private static void prepareResponse(AbstractSetResponse response, long currentTid) {
        InvocationInfo invInf = new InvocationInfo();
        invInf.setTransactionId(currentTid);
        invInf.setInvocationState(InvocationState.WAIT);
        response.setInvocationInfo(invInf);
    }    
           
    
}
