/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

/**
 *
 * @author besting
 */
public class OperationInvocationContext {
    
    private String operationHandle;
    private String operationTarget;
    private long transactionId;

    public OperationInvocationContext(String operationHandle, String operationTarget, long transactionId) {
        this.operationHandle = operationHandle;
        this.operationTarget = operationTarget;
        this.transactionId = transactionId;
    }

    public void setOperationHandle(String operationHandle) {
        this.operationHandle = operationHandle;
    }

    public String getOperationHandle() {
        return operationHandle;
    }

    public void setOperationTarget(String operationTarget) {
        this.operationTarget = operationTarget;
    }

    public String getOperationTarget() {
        return operationTarget;
    }  

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public long getTransactionId() {
        return transactionId;
    }
    
}
