/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import org.yads.java.service.DefaultEventSource;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.EventSourceStub;
import org.yads.java.service.ServiceSubscription;
import org.yads.java.service.parameter.ParameterValue;

public class SDCEventDelegate implements EventDelegate {

    private EventSourceStub eventSourceStub;

    public EventSourceStub getEventSourceStub() {
        return eventSourceStub;
    }

    public void setEventSourceStub(EventSourceStub eventSourceStub) {
        this.eventSourceStub = eventSourceStub;
    }
    
    @Override
    public void solicitResponseReceived(DefaultEventSource event, ParameterValue paramValue, int eventNumber, ServiceSubscription subscription) {
    }    
   
}
