/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import com.rits.cloning.Cloner;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.AbstractState;

public abstract class SDCProviderMDStateHandler<T extends AbstractState> extends SDCProviderHandler<T> {
    
    public SDCProviderMDStateHandler(String descriptorHandle) {
        super(descriptorHandle);
    }
      
    public InvocationState onStateChangeRequest(T state, OperationInvocationContext oic) {
        return InvocationState.FAIL;
    }    
    
    protected final AbstractState getInitalClonedState() {
        Cloner cloner = new Cloner();
        AbstractState cloned = cloner.deepClone(getInitialState());
        return cloned;
    }
    
    protected abstract T getInitialState();
    
}
