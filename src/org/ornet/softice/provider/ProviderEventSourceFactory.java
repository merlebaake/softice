/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import org.ornet.softice.SDCToolbox;
import java.math.BigInteger;
import org.ornet.cdm.AbstractAlertReport;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextReport;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricReport;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.EpisodicAlertReport;
import org.ornet.cdm.EpisodicContextReport;
import org.ornet.cdm.EpisodicMetricReport;
import org.ornet.cdm.InstanceIdentifier;
import org.ornet.cdm.InvocationError;
import org.ornet.cdm.InvocationInfo;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.OperationInvokedReport;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.parameter.ParameterValue;

public class ProviderEventSourceFactory {
    
    public static SDCEventDelegate episodicMetricReportSource;
    public static SDCEventDelegate episodicContextReportSource;
    public static SDCEventDelegate operationInvokedReportSource;
    public static SDCEventDelegate waveformStreamReportSource;
    public static SDCEventDelegate episodicAlertReportSource;
    
    static EventDelegate createEpisodicMetricReportSource(final SDCProvider provider) {
        episodicMetricReportSource = new SDCEventDelegate();
        return episodicMetricReportSource;
    }  
    
    static EventDelegate createOperationInvokedReportSource(final SDCProvider provider) {
        operationInvokedReportSource = new SDCEventDelegate();
        return operationInvokedReportSource;
    }       
    
    static EventDelegate createEpisodicContextReportSource(final SDCProvider provider) {
        episodicContextReportSource = new SDCEventDelegate();
        return episodicContextReportSource;
    }
    
    static EventDelegate createStreamReportSource(final SDCProvider provider) {
        waveformStreamReportSource = new SDCEventDelegate();
        return waveformStreamReportSource;
    } 
    
    static EventDelegate createEpisodicAlertReportSource(final SDCProvider provider) {
        episodicAlertReportSource = new SDCEventDelegate();
        return episodicAlertReportSource;
    }     

    static synchronized void fireEpisodicMetricEventReport(AbstractMetricState state, BigInteger mdibVersion) {
        AbstractMetricReport.ReportPart mrp = new AbstractMetricReport.ReportPart();
        mrp.getMetricState().add(state);
        EpisodicMetricReport report = new EpisodicMetricReport();
        report.getReportPart().add(mrp);
        report.setMdibVersion(mdibVersion);
        report.setSequenceId("0");
        ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(report);
        episodicMetricReportSource.getEventSourceStub().fire(pv, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);        
    }
    
    static synchronized void fireEpisodicAlertEventReport(AbstractAlertState state, BigInteger mdibVersion) {
        AbstractAlertReport.ReportPart arp = new AbstractAlertReport.ReportPart();
        arp.getAlertState().add(state);
        EpisodicAlertReport report = new EpisodicAlertReport();
        report.getReportPart().add(arp);
        report.setMdibVersion(mdibVersion);
        report.setSequenceId("0");
        ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(report);
        episodicAlertReportSource.getEventSourceStub().fire(pv, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);     
    }    

    static synchronized void fireOperationInvokedReport(SDCProvider provider, OperationInvocationContext oic, InvocationState is, BigInteger mdibVersion, String operationErrorMsg) {
        OperationInvokedReport.ReportPart orp = new OperationInvokedReport.ReportPart();
        InvocationInfo invInf = new InvocationInfo();
        orp.setInvocationInfo(invInf);
        orp.setInvocationSource(new InstanceIdentifier());
        invInf.setTransactionId(oic.getTransactionId());
        invInf.setInvocationState(is);
        orp.setOperationHandleRef(oic.getOperationHandle());
        orp.setOperationTarget(SDCToolbox.getOperationTargetForOperationHandle(provider, oic.getOperationHandle()));
        if (operationErrorMsg != null) {
            invInf.setInvocationError(InvocationError.UNKN);
        }
        OperationInvokedReport oir = new OperationInvokedReport();
        oir.getReportPart().add(orp);
        oir.setMdibVersion(mdibVersion);
        oir.setSequenceId("0");
        ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(oir);
        operationInvokedReportSource.getEventSourceStub().fire(pv, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);            
    }

    static synchronized void fireEpisodicContextChangedReport(AbstractContextState acs, BigInteger mdibVersion) {
        AbstractContextReport.ReportPart crp = new AbstractContextReport.ReportPart();
        crp.getContextState().add(acs);
        EpisodicContextReport ecr = new EpisodicContextReport();
        ecr.getReportPart().add(crp);
        ecr.setMdibVersion(mdibVersion);
        ecr.setSequenceId("0");
        ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(ecr);        
        episodicContextReportSource.getEventSourceStub().fire(pv, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);    
    }

}
