/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting, stegemann
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.validation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

/**
 * Validation handler which allows the registragtion of multiple handlers. 
 * Stores a list of handlers which are executed, if a event occures.
 * @author stegemann
 *
 */
public class CustomValidationHandler implements ValidationEventHandler 
{	
	private List<ValidationEventHandler> handlers;

	public CustomValidationHandler() 
	{
		handlers = new ArrayList<>();
	}
	
	public void addHandler(ValidationEventHandler handler)
	{
		if(handler != null) {
			handlers.add(handler);
		}
	}
	
	public void removeHandler(ValidationEventHandler handler)
	{
		if(handler != null) {
			handlers.remove(handler);
		}
	}
	
	@Override
	public boolean handleEvent(ValidationEvent event) 
	{
		for(ValidationEventHandler handler : handlers)
		{
			handler.handleEvent(event);
		}
		
		return false;
	}

}
