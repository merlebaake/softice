/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting, stegemann
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.validation;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

import org.yads.java.util.Log;

/**
 * Validation handler which writes into a log.
 * @author stegemann
 */
public class LogValidationHandler implements ValidationEventHandler {
    
    @Override
    public boolean handleEvent(ValidationEvent event) {
        StringBuilder sb = new StringBuilder();
        sb.append("XML schema validation alert");
        sb.append("\nSEVERITY:  ").append(event.getSeverity());
        sb.append("\nMESSAGE:  ").append(event.getMessage());
        sb.append("\nLINKED EXCEPTION:  ").append(event.getLinkedException());
        sb.append("\nLOCATOR");
        sb.append("\n    LINE NUMBER:  ").append(event.getLocator().getLineNumber());
        sb.append("\n    COLUMN NUMBER:  ").append(event.getLocator().getColumnNumber());
        sb.append("\n    OFFSET:  ").append(event.getLocator().getOffset());
        sb.append("\n    OBJECT:  ").append(event.getLocator().getObject());
        sb.append("\n    NODE:  ").append(event.getLocator().getNode());
        sb.append("\n    URL:  ").append(event.getLocator().getURL());
        Log.error(sb.toString());
        return false;
    } 
}  
