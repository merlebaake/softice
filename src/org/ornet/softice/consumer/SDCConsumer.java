/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.consumer;

import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractAlertReport;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextReport;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricReport;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AbstractSet;
import org.ornet.cdm.AbstractSetResponse;
import org.ornet.cdm.Activate;
import org.ornet.cdm.ActivateResponse;
import org.ornet.cdm.EpisodicAlertReport;
import org.ornet.cdm.EpisodicContextReport;
import org.ornet.cdm.EpisodicMetricReport;
import org.ornet.cdm.GetContextStates;
import org.ornet.cdm.GetContextStatesResponse;
import org.ornet.cdm.GetMdDescription;
import org.ornet.cdm.GetMdDescriptionResponse;
import org.ornet.cdm.GetMdib;
import org.ornet.cdm.GetMdibResponse;
import org.ornet.cdm.GetMdState;
import org.ornet.cdm.GetMdStateResponse;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.Mdib;
import org.ornet.cdm.MdState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.NumericMetricValue;
import org.ornet.cdm.OperationInvokedReport;
import org.ornet.cdm.PeriodicAlertReport;
import org.ornet.cdm.PeriodicContextReport;
import org.ornet.cdm.PeriodicMetricReport;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.SetAlertState;
import org.ornet.cdm.SetContextState;
import org.ornet.cdm.SetString;
import org.ornet.cdm.SetValue;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.StringMetricState;
import org.ornet.cdm.StringMetricValue;
import org.ornet.cdm.WaveformStream;
import org.ornet.mdpws.MDPWSStreamingManager;
import org.ornet.softice.SoftICE;
import org.ornet.softice.SDCToolbox;
import org.ornet.softice.provider.OperationInvocationContext;
import org.yads.java.client.DefaultClient;
import org.yads.java.communication.CommunicationException;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.IPNetworkDetection;
import org.yads.java.communication.protocol.http.HTTPBinding;
import org.yads.java.eventing.ClientSubscription;
import org.yads.java.eventing.EventSource;
import org.yads.java.message.eventing.SubscriptionEndMessage;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.security.CredentialInfo;
import org.yads.java.security.SecurityKey;
import org.yads.java.service.Operation;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.service.reference.DeviceReference;
import org.yads.java.service.reference.ServiceReference;
import org.yads.java.types.URI;
import org.yads.java.types.UnknownDataContainer;
import org.yads.java.util.Log;
import org.ornet.softice.provider.SDCEndpoint;
import org.yads.java.communication.protocol.http.HTTPSBinding;

public final class SDCConsumer extends DefaultClient implements SDCEndpoint {
    
    protected static final int DURATION_SUBSCRIPTION = 30;
    protected static final int MAX_QUEUED_EVENTS = 256;
    protected static final int CYLCLE_RENEW = 15;
    private static final ExecutorService executor = Executors.newCachedThreadPool();

    private static final String PERIODIC_CONTEXT_CHANGED_REPORT = "PeriodicContextChangedReport";
    private static final String PERIODIC_ALERT_REPORT = "PeriodicAlertReport";
    private static final String PERIODIC_METRIC_REPORT = "PeriodicMetricReport";
    private static final String OPERATION_INVOKED_REPORT = "OperationInvokedReport";
    private static final String EPISODIC_ALERT_REPORT = "EpisodicAlertReport";
    private static final String EPISODIC_CONTEXT_CHANGED_REPORT = "EpisodicContextChangedReport";
    private static final String EPISODIC_METRIC_REPORT = "EpisodicMetricReport";    
    
    private final ScheduledExecutorService renewScheduler = Executors.newScheduledThreadPool(1);
    private SDCConnectionLostHandler connectionLostHandler;
    private BlockingDeque<AbstractState> eventDeque = null;

    private void enqueueEvent(AbstractState next) {
        if (eventDeque == null)
            return;
        eventDeque.add(next);
        if (eventDeque.size() > MAX_QUEUED_EVENTS) {
            eventDeque.removeFirst();
        }
    }

    protected BlockingDeque<AbstractState> getEventDeque() {
        return eventDeque;
    }

    public void setEventDeque(BlockingDeque<AbstractState> eventDeque) {
        MDPWSStreamingManager.getInstance().addStreamListeners(this, streamAddrs);
        this.eventDeque = eventDeque;
    }
    
    interface SubscriptionVisitor {
        boolean visit(ClientSubscription subscription);
    }
    
    class TransactionState {
        
        public TransactionState(long transactionId, InvocationState is) {
            this.transactionId = transactionId;
            this.invocationState = is;
        }

        public synchronized long getTransactionId() {
            return transactionId;
        }

        public synchronized void setTransactionId(long transactionId) {
            this.transactionId = transactionId;
        }

        public synchronized InvocationState getInvocationState() {
            return invocationState;
        }

        public synchronized void setInvocationState(InvocationState invocationState) {
            this.invocationState = invocationState;
        }
        
        private long transactionId;
        private InvocationState invocationState;
        
    }
    
    private final DeviceReference deviceRef;
    private final Mdib mdib = new Mdib();
    private final AtomicBoolean connected = new AtomicBoolean(true);
    private final List<String> streamAddrs = new ArrayList<>();

    private final Map<ClientSubscription, String> subscriptions = new ConcurrentHashMap<>();
    
    private final ConcurrentMap<String, SDCConsumerHandler> eventHandler = new ConcurrentHashMap<>();
    private final ConcurrentLinkedQueue<TransactionState> transactionQueue = new ConcurrentLinkedQueue<>();
    private final ConcurrentMap<Long, FutureInvocationState> fisMap = new ConcurrentHashMap<>();

    public SDCConsumer(DeviceReference deviceRef) {
        this.deviceRef = deviceRef;
        init(deviceRef);
    }

    @Override
    @SuppressWarnings("FinalizeDeclaration")
    public void finalize() throws Throwable {
        try {
            close();
        } finally {
            super.finalize();
        }
    }    

    private void init(DeviceReference deviceRef) {
        if (getMDIB() != null) {
            connected.set(initEventing());
            initMDPWSStreaming();
            if (connected.get()) {
                renewScheduler.scheduleAtFixedRate(createRenewManager(), SDCConsumer.CYLCLE_RENEW / 2, SDCConsumer.CYLCLE_RENEW, TimeUnit.SECONDS);
            }
            else {
                Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, "Fatal error: could not initialize eventing! Device reference: {0}", deviceRef);
            }
        }
        else
            connected.set(false);
    }
    
    private boolean addSubscription(String name, ClientSubscription subscription) {
        if (subscription != null) {
            subscriptions.put(subscription, name);
            return true;
        }
        return false;
    }
    
    private void applyToSubscriptions(SubscriptionVisitor visitor) {
        synchronized(subscriptions) {
            for (Map.Entry<ClientSubscription, String> next : subscriptions.entrySet()) {
                if (!visitor.visit(next.getKey()))
                    break;
            }
        }
    }

    private boolean initEventing() {
        subscriptions.clear();
        if (!addSubscription(EPISODIC_METRIC_REPORT, initEventSink("EventReport", EPISODIC_METRIC_REPORT)))
            return false;
        if (!addSubscription(EPISODIC_CONTEXT_CHANGED_REPORT, initEventSink("ContextService", EPISODIC_CONTEXT_CHANGED_REPORT)))
            return false;
        if (!addSubscription(EPISODIC_ALERT_REPORT, initEventSink("EventReport", EPISODIC_ALERT_REPORT)))
            return false;
        if (!addSubscription(OPERATION_INVOKED_REPORT, initEventSink("EventReport", OPERATION_INVOKED_REPORT)))
            return false;
        if (!addSubscription(PERIODIC_METRIC_REPORT, initEventSink("EventReport", PERIODIC_METRIC_REPORT)))
            return false;
        if (!addSubscription(PERIODIC_ALERT_REPORT, initEventSink("EventReport", PERIODIC_ALERT_REPORT)))
            return false;        
        return addSubscription(PERIODIC_CONTEXT_CHANGED_REPORT, initEventSink("ContextService", PERIODIC_CONTEXT_CHANGED_REPORT));
    }

    private void initMDPWSStreaming() {
        try {
            // Check for waveform streaming service
            final ServiceReference serviceReference = deviceRef.getDevice().getServiceReference(new URI("WaveformEventReport"), SecurityKey.EMPTY_KEY);
            if (serviceReference == null)
                return;
            // Trigger custom metadata fetch from waveform streaming service
            UnknownDataContainer [] udc = serviceReference.getService().getCustomMData(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
            if (udc == null)
                // No custom metadata found, return
                return;
            for (UnknownDataContainer next : udc) {
                List streamingAddresses = next.getUnknownElements(MDPWSStreamingManager.STREAM_DESCRIPTIONS);
                // No streaming addresses found, return
                if (streamingAddresses == null)
                    return;
                for (Object nextAdrList : streamingAddresses) {
                    for (String nextAdr : (List<String>)nextAdrList) {
                        streamAddrs.add(nextAdr);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      
    public void close() {
        try {
            renewScheduler.shutdown();
            renewScheduler.awaitTermination(5, TimeUnit.SECONDS);
            applyToSubscriptions((ClientSubscription subscription) -> {
                try {
                    subscription.unsubscribe();
                } catch (Exception ex) {
                    Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
                }
                return true;
            });
            subscriptions.clear();
            setEventDeque(null);
            MDPWSStreamingManager.getInstance().removeStreamListeners(this);
            connected.set(false);
        } catch (Exception ex) {
            if (Log.isDebug()) {
                Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);            
            }
        }
    }

    private Runnable createRenewManager() {
        return () -> {
            try {
                if (!connected.get()) {
                    // Try to subscribe again
                    connected.set(initEventing());
                    if (connected.get()) {
                        Logger.getLogger(SDCConsumer.class.getName()).log(Level.INFO, "Connection re-established.");
                    }
                }
                applyToSubscriptions((ClientSubscription subscription) -> {
                    try {
                        subscription.renew(SDCConsumer.DURATION_SUBSCRIPTION * 1000);
                        return true;
                    } catch (Exception ex) {
                        if (Log.isDebug()) {
                            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Logger.getLogger(SDCConsumer.class.getName()).log(Level.INFO, "Connection lost. Trying to re-establish in {0} seconds...", CYLCLE_RENEW);
                        connected.set(false);
                        if (connectionLostHandler != null) {
                            try {
                                connectionLostHandler.onConnectionLost(SDCConsumer.this);
                            }
                            catch (Exception e) {
                                Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, e);
                            }
                        }
                        return false;
                    }
                });
            } catch (Exception ex) {
                Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
    }

    public boolean isConnected() {
        return connected.get();
    }
    
    private synchronized Operation getOperation(String serviceId, String operationName) {
        try {
            ServiceReference getServiceRef = deviceRef.getDevice().getServiceReference(new URI(serviceId), SecurityKey.EMPTY_KEY);
            if (getServiceRef == null)
                return null;
            Operation op = getServiceRef.getService().getOperation(null, operationName, null, null);
            return op;
        } catch (CommunicationException ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void subscriptionEndReceived(ClientSubscription subscription, int subscriptionEndType) {  
        if (!connected.get())
            return;
        connected.set(false);
        try {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.WARNING, "Subscription ended! Consumer is now disconnected.");
        } catch (Exception ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void subscriptionTimeoutReceived(ClientSubscription subscription) {
        subscriptionEndReceived(subscription, SubscriptionEndMessage.WSE_STATUS_UNKNOWN);
    }
    
    private synchronized EventSource getEventSource(String serviceId, String sourceName) {
        try {
            ServiceReference getServiceRef = deviceRef.getDevice().getServiceReference(new URI(serviceId), SecurityKey.EMPTY_KEY);
            if (getServiceRef == null)
                return null;
            EventSource es = getServiceRef.getService().getEventSource(null, sourceName, null, null);
            return es;
        } catch (CommunicationException ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }    
      
    @Override
    public Mdib getMDIB() {    
        try {
            Operation op = getOperation("GetService", "GetMdib");
            if (op == null)
                return null;
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(new GetMdib());
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            GetMdibResponse response = JAXBUtil.getInstance().createInputParameterValue(result, GetMdibResponse.class);
            synchronized(mdib) {
                Mdib temp = response.getMdib();
                mdib.setMdDescription(temp.getMdDescription());
                mdib.setMdState(temp.getMdState());
                mdib.setSequenceId(temp.getSequenceId());
                Cloner cloner = new Cloner();
                return cloner.deepClone(mdib);
            }
        } catch (Exception ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }    

    @Override
    public MdDescription getMDDescription() {
        synchronized(mdib) {
            if (mdib.getMdDescription() != null) {
                Cloner cloner = new Cloner();
                return cloner.deepClone(mdib.getMdDescription());
            }
        }
        try {
            Operation op = getOperation("GetService", "GetMdDescription");
            if (op == null)
                return null;
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(new GetMdDescription());
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            GetMdDescriptionResponse response = JAXBUtil.getInstance().createInputParameterValue(result, GetMdDescriptionResponse.class);
            synchronized(mdib) {
                mdib.setMdDescription(response.getMdDescription());
                Cloner cloner = new Cloner();
                return cloner.deepClone(mdib.getMdDescription());
            }
        } catch (Exception ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }

    @Override
    public MdState getMDState() {
        return getMDState(null);
    }

    @Override
    public MdState getMDState(List<String> handles) {
        try {
            Operation op = getOperation("GetService", "GetMdState");
            if (op == null)
                return null;
            GetMdState getState = new GetMdState();
            if (handles != null)
                getState.getHandleRef().addAll(handles);
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(getState);
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            GetMdStateResponse response = JAXBUtil.getInstance().createInputParameterValue(result, GetMdStateResponse.class);
            synchronized(mdib) {
                mdib.setMdState(response.getMdState());
                Cloner cloner = new Cloner();
                return cloner.deepClone(mdib.getMdState());
            }
        } catch (Exception ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public <T extends AbstractState> T requestState(String handle, Class<T> stateType) {
        if (AbstractContextState.class.isAssignableFrom(stateType)) {
            Operation op = getOperation("ContextService", "GetContextStates");
            if (op == null)
                return null;
            try {
                GetContextStates getState = new GetContextStates();
                getState.getHandleRef().add(handle);
                ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(getState);
                ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
                GetContextStatesResponse response = JAXBUtil.getInstance().createInputParameterValue(result, GetContextStatesResponse.class);
                final List<AbstractContextState> contextStates = response.getContextState();
                if (contextStates.isEmpty())
                    return null;
                return stateType.cast(contextStates.get(0));
            } catch (Exception ex) {
                Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
            }        
            return null;
        }
        List<String> handles = new ArrayList<>();
        handles.add(handle);
        MdState mdState = getMDState(handles);
        if (mdState == null)
            return null;
        if (mdState.getState().isEmpty())
            return null;
        return stateType.cast(mdState.getState().get(0));
    }
    
    public InvocationState activate(String handle, FutureInvocationState fis) {
        try {
            Activate activate = new Activate();
            activate.setOperationHandleRef(handle);
            Operation op = getOperation("SetService", "Activate");
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(activate);
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            ActivateResponse response = JAXBUtil.getInstance().createInputParameterValue(result, ActivateResponse.class);
            handleFutureInvocationState(response.getInvocationInfo().getTransactionId(), fis);
            return response.getInvocationInfo().getInvocationState();
        } catch (Exception ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return InvocationState.FAIL;        
    }

    public InvocationState commitValue(String descriptorHandle, NumericMetricValue value, FutureInvocationState fis) {
        NumericMetricState state = new NumericMetricState();
        state.setDescriptorHandle(descriptorHandle);
        state.setMetricValue(value);
        return commitState(state, fis);
    }
     
    public InvocationState commitString(String descriptorHandle, StringMetricValue value, FutureInvocationState fis) {
        StringMetricState state = new StringMetricState();
        state.setDescriptorHandle(descriptorHandle);
        state.setMetricValue(value);
        return commitState(state, fis);
    }    
    
    public InvocationState commitState(AbstractState state, FutureInvocationState fis) {
        String opName = null;
        opName = getSetOperationName(state, opName);
        try {
            if (opName == null)
                return InvocationState.FAIL;
            String serviceId = (state instanceof AbstractContextState? "ContextService" : "SetService");
            Operation op = getOperation(serviceId, opName);
            if (op == null)
                return InvocationState.FAIL;
            AbstractSet reqObj = createSetRequestType(opName, state);
            if (reqObj == null)
                return InvocationState.FAIL;
            reqObj.setOperationHandleRef(SDCToolbox.getFirstOperationHandleForOperationTarget(this, state.getDescriptorHandle()));
            if (reqObj.getOperationHandleRef() == null)
                return InvocationState.FAIL;
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(reqObj);
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            AbstractSetResponse response = JAXBUtil.getInstance().createInputParameterValue(result, AbstractSetResponse.class);
            handleFutureInvocationState(response.getInvocationInfo().getTransactionId(), fis);
            return response.getInvocationInfo().getInvocationState();
        } catch (Exception ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return InvocationState.FAIL;
    }
    
    public String exchangeRaw(String rawData, String serviceId, String operationName) {
        try {
            ParameterValue pv = new ParameterValue();
            pv.setParameterRawData(rawData); 
            Operation op = getOperation(serviceId, operationName);
            if (op == null)
                throw new Exception("Can't resolve operation for service id '" + serviceId + "' and operation name '" + operationName + "'.");
            ParameterValue result = op.invoke(pv, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            return JAXBUtil.getInstance().nodeToString(result.getParameterXmlData());
        } catch (Exception ex) {
            Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;        
    }

    private AbstractSet createSetRequestType(String opName, AbstractState state) {
        AbstractSet reqObj = null;
        switch(opName) {
            case "SetValue": 
                reqObj = new SetValue(); 
                ((SetValue)reqObj).setRequestedNumericValue(((NumericMetricState)state).getMetricValue().getValue());
                break;
            case "SetString": 
                reqObj = new SetString(); 
                ((SetString)reqObj).setRequestedStringValue(((StringMetricState)state).getMetricValue().getValue());
                break;
            case "SetContextState": 
                reqObj = new SetContextState(); 
                ((SetContextState)reqObj).getProposedContextState().add((AbstractContextState) state);
                break;
            case "SetAlertState": 
                reqObj = new SetAlertState(); 
                ((SetAlertState)reqObj).setProposedAlertState((AbstractAlertState) state);
                break;
        }
        return reqObj;
    }

    private String getSetOperationName(AbstractState state, String opName) {
        if (state instanceof NumericMetricState) {
            opName = "SetValue";
        }
        else if (state instanceof StringMetricState) {
            opName = "SetString";
        }
        else if (state instanceof AbstractContextState) {
            opName = "SetContextState";
        }
        else if (state instanceof AbstractAlertState) {
            opName = "SetAlertState";
        }
        return opName;
    }

    private ClientSubscription initEventSink(String serviceId, String eventName) {
        EventSource reportSource = getEventSource(serviceId, eventName);
        if (reportSource == null) {
            return null;
        }
        List<HTTPBinding> bindings = new ArrayList<>();
        Iterator<IPAddress> adrIter = IPNetworkDetection.getInstance().getIPv4Addresses(true);
        int port = SoftICE.getInstance().extractNextPort();
        while (adrIter.hasNext()) {
            IPAddress next = adrIter.next();
            HTTPBinding binding = (SoftICE.getInstance().getServerSSLContext() != null)?
                    new HTTPSBinding(next, port, "/" + eventName + "Sink", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, CredentialInfo.EMPTY_CREDENTIAL_INFO)
                        :
                    new HTTPBinding(next, port, "/" + eventName + "Sink", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
            bindings.add(binding);            
        }
        try {
            return reportSource.subscribe(this, 0, bindings, CredentialInfo.EMPTY_CREDENTIAL_INFO);
        } catch (Exception ex) {
            if (Log.isDebug()) {
                Logger.getLogger(SDCConsumer.class.getName()).log(Level.SEVERE, null, ex);            
            }
            return null;
        }
    }
    
    public void registerEventHandler(SDCConsumerHandler handler) {
        eventHandler.put(handler.getDescriptorHandle(), handler);
        MDPWSStreamingManager.getInstance().addStreamListeners(this, streamAddrs);
    }

    public void unregisterEventHandler(SDCConsumerHandler handler) {
        eventHandler.remove(handler.getDescriptorHandle());
    }
    
    public void unregisterEventHandler(String desriptorHandle) {
        eventHandler.remove(desriptorHandle);
    }

    public void setConnectionLostHandler(SDCConnectionLostHandler connectionLostHandler) {
        this.connectionLostHandler = connectionLostHandler;
    }

    public SDCConnectionLostHandler getConnectionLostHandler() {
        return connectionLostHandler;
    }

    @Override
    public ParameterValue eventReceived(ClientSubscription subscription, URI actionURI, ParameterValue parameterValue) {
        synchronized(subscriptions) {        
            if (subscription == null && actionURI.equals(new URI(MDPWSStreamingManager.ACTION_URI))) {
                streamReceived(parameterValue);
                return null;
            }
            else if (subscription == null || !subscriptions.containsKey(subscription)) {
                throw new RuntimeException("Fatal error: subscription type can't be resolved, actionURI = " + actionURI);
            }
            if (subscriptions.get(subscription).equals(EPISODIC_METRIC_REPORT)) {
                emrEventReceived(parameterValue);
            }
            else if (subscriptions.get(subscription).equals(EPISODIC_ALERT_REPORT)) {
                earEventReceived(parameterValue);
            }
            else if (subscriptions.get(subscription).equals(EPISODIC_CONTEXT_CHANGED_REPORT)) {
                ecrEventReceived(parameterValue);
            }
            else if (subscriptions.get(subscription).equals(OPERATION_INVOKED_REPORT)) {
                oirEventReceived(parameterValue);
            }
            else if (subscriptions.get(subscription).equals(PERIODIC_METRIC_REPORT)) {
                pmrEventReceived(parameterValue);
            }              
            else if (subscriptions.get(subscription).equals(PERIODIC_CONTEXT_CHANGED_REPORT)) {
                pcrEventReceived(parameterValue);
            }    
            else if (subscriptions.get(subscription).equals(PERIODIC_ALERT_REPORT)) {
                parEventReceived(parameterValue);
            }            
        }
        return null;
    }

    private void streamReceived(ParameterValue parameterValue) {
        WaveformStream wfs = JAXBUtil.getInstance().createInputParameterValue(parameterValue, WaveformStream.class);
        if (wfs != null) {
            for (RealTimeSampleArrayMetricState rtsams : wfs.getState()) {
                enqueueEvent(rtsams);
                final SDCConsumerHandler handler = eventHandler.get(rtsams.getDescriptorHandle());
                if (handler != null && handler instanceof ISDCConsumerStateChangedHandler)
                    ((ISDCConsumerStateChangedHandler)handler).onStateChanged(rtsams);
            }
        }
    }
    
    protected void unregisterFutureInvocationstate(FutureInvocationState fis) {
        fisMap.remove(fis.getTransactionId());
    }
    
    private void handleFutureInvocationState(long transactionId, FutureInvocationState fis) {
        if (fisMap.containsValue(fis)) {
            throw new RuntimeException("FutureInvocationState instance already in use!");
        }
        fis.setTransactionId(transactionId);
        fis.setConsumer(this);
        fisMap.put(transactionId, fis);
        // Dequeue possible intermediate events
        while (transactionQueue.size() > 0) {
            TransactionState ts = transactionQueue.poll();
            if (fisMap.containsKey(ts.getTransactionId())) {
                fisMap.get(ts.getTransactionId()).setActual(ts.getInvocationState());
            }
        }
    }
    
    private void pmrEventReceived(ParameterValue pv) {
        PeriodicMetricReport pmr = JAXBUtil.getInstance().createInputParameterValue(pv, PeriodicMetricReport.class);
        for (AbstractMetricReport.ReportPart part : pmr.getReportPart()) {
            handleMetricReportPart((AbstractMetricReport.ReportPart) part);
        }
    }
    
    private void pcrEventReceived(ParameterValue pv) {
        PeriodicContextReport ecr = JAXBUtil.getInstance().createInputParameterValue(pv, PeriodicContextReport.class);
        for (AbstractContextReport.ReportPart part : ecr.getReportPart()) {
            handleContextReportPart((AbstractContextReport.ReportPart) part);
        } 
    }

    private void emrEventReceived(ParameterValue pv) {
        EpisodicMetricReport emr = JAXBUtil.getInstance().createInputParameterValue(pv, EpisodicMetricReport.class);
        for (AbstractMetricReport.ReportPart part : emr.getReportPart()) {
            handleMetricReportPart((AbstractMetricReport.ReportPart) part);
        }
    }

    private void earEventReceived(ParameterValue pv) {
        EpisodicAlertReport ear = JAXBUtil.getInstance().createInputParameterValue(pv, EpisodicAlertReport.class);
        for (AbstractAlertReport.ReportPart part : ear.getReportPart()) {
            handleAlertReportPart((AbstractAlertReport.ReportPart) part);
        }
    }
    
    private void parEventReceived(ParameterValue pv) {
        PeriodicAlertReport par = JAXBUtil.getInstance().createInputParameterValue(pv, PeriodicAlertReport.class);
        for (AbstractAlertReport.ReportPart part : par.getReportPart()) {
            handleAlertReportPart((AbstractAlertReport.ReportPart) part);
        }
    }    
    
    private void handleAlertReportPart(AbstractAlertReport.ReportPart part) {
        for (AbstractAlertState next : part.getAlertState()) {
            enqueueEvent(next);
            final SDCConsumerHandler handler = eventHandler.get(next.getDescriptorHandle());
            if (handler != null && handler instanceof ISDCConsumerStateChangedHandler)
                ((ISDCConsumerStateChangedHandler)handler).onStateChanged(next);
        }
    }    

    private <T> void handleMetricReportPart(AbstractMetricReport.ReportPart part) {
        for (AbstractMetricState next : part.getMetricState()) {
            enqueueEvent(next);
            final SDCConsumerHandler handler = eventHandler.get(next.getDescriptorHandle());
            if (handler != null && handler instanceof ISDCConsumerStateChangedHandler)
                ((ISDCConsumerStateChangedHandler)handler).onStateChanged(next);
        }
    }
    
    private void ecrEventReceived(ParameterValue pv) {
        EpisodicContextReport ecr = JAXBUtil.getInstance().createInputParameterValue(pv, EpisodicContextReport.class);
        for (AbstractContextReport.ReportPart part : ecr.getReportPart()) {
            handleContextReportPart((AbstractContextReport.ReportPart) part);
        }
    }

    private void handleContextReportPart(AbstractContextReport.ReportPart part) {
        executor.execute(() -> {
            for (AbstractContextState next : part.getContextState()) {
                final SDCConsumerHandler handler = eventHandler.get(next.getDescriptorHandle());
                enqueueEvent(next);
                if (handler != null && handler instanceof ISDCConsumerStateChangedHandler) {
                    ((ISDCConsumerStateChangedHandler)handler).onStateChanged(next);                    
                }
            }
        });
    }

    private void oirEventReceived(ParameterValue pv) {
        OperationInvokedReport oir = JAXBUtil.getInstance().createInputParameterValue(pv, OperationInvokedReport.class);
        for (OperationInvokedReport.ReportPart part : oir.getReportPart()) {
            String target = part.getOperationTarget();
            String handlerKey;
            if (target == null)
                target = SDCToolbox.getOperationTargetForOperationHandle(this, part.getOperationHandleRef());
            if (target == null) {
                Logger.getLogger(SDCConsumer.class.getName()).log(Level.WARNING, "Error in operation invoked report, can't resolve target: " + part.getInvocationInfo().getInvocationState());
                return;                    
            }
            if (eventHandler.containsKey(part.getOperationHandleRef())) {
                // Activate operations use operation handle
                handlerKey = part.getOperationHandleRef();
            } else {
                handlerKey = target;
            }
            final long transactionId = part.getInvocationInfo().getTransactionId();
            final InvocationState invocationState = part.getInvocationInfo().getInvocationState();
            // Queue to check intermediate events during commits
            transactionQueue.add(new TransactionState(transactionId, invocationState));
            // Notify about future invocation state events
            if (fisMap.containsKey(transactionId)) {
                fisMap.get(transactionId).setActual(invocationState);
            }
            OperationInvocationContext oic = new OperationInvocationContext(part.getOperationHandleRef(), target, transactionId);
            final SDCConsumerHandler handler = eventHandler.get(handlerKey);
            if (handler != null && handler instanceof ISDCConsumerOperationInvokedHandler) {
                ((ISDCConsumerOperationInvokedHandler)handler).onOperationInvoked(oic, invocationState);
            }
        }    
    }
    
    @Override
    public String getEndpointReference() {
        return deviceRef.getEndpointReference().getAddress().toString();
    } 

    @Override
    public int hashCode() {
        return getEndpointReference().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof SDCConsumer)) {
            return false;
        }
        SDCConsumer other = (SDCConsumer)obj;
        return this.getEndpointReference().equals(other.getEndpointReference());
    }
    
    
    
}
