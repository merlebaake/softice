/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.consumer;

public class SDCConsumerHandler {
    
    private final String descriptorHandle;

    public SDCConsumerHandler(String descriptorHandle) {
        this.descriptorHandle = descriptorHandle;
    }

    public String getDescriptorHandle() {
        return descriptorHandle;
    }

}
