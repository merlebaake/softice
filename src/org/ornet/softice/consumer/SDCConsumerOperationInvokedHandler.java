/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.consumer;

import org.ornet.cdm.InvocationState;
import org.ornet.softice.provider.OperationInvocationContext;

public abstract class SDCConsumerOperationInvokedHandler extends SDCConsumerHandler implements ISDCConsumerOperationInvokedHandler {

    public SDCConsumerOperationInvokedHandler(String descriptorHandle) {
        super(descriptorHandle);
    }

    @Override
    public abstract void onOperationInvoked(OperationInvocationContext oic, InvocationState is);
    
}
