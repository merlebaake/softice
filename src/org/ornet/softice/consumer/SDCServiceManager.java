/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.consumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.yads.java.client.DefaultClient;
import org.yads.java.client.SearchManager;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.service.reference.DeviceReference;
import org.yads.java.types.QName;
import org.yads.java.types.QNameSet;
import org.yads.java.types.SearchParameter;

public class SDCServiceManager {
    
    private static final int DEFAULT_TIMEOUT = 16000;
    private static final int MIN_GAP_TIME = 4000;
    
    private static SDCServiceManager instance;
    
    private SDCServiceManager() {
        
    }
    
    public static SDCServiceManager getInstance() {
        if (instance == null)
            instance = new SDCServiceManager();
        return instance;
    }
    
    public List<SDCConsumer> discoverOSCP() {
        return discoverEPR(null, DEFAULT_TIMEOUT, MIN_GAP_TIME);
    }    
    
    public SDCConsumer discoverEPR(String epr) {
        return SDCServiceManager.this.discoverEPR(epr, DEFAULT_TIMEOUT);
    }      
    
    public SDCConsumer discoverEPR(String epr, int timeout) {
        Set<String> eprSet = new HashSet<>();
        eprSet.add(epr);
        List<SDCConsumer> c = discoverEPR(eprSet, timeout, MIN_GAP_TIME);
        if (c.isEmpty())
            return null;
        return c.get(0);
    }    
    
    public List<SDCConsumer> discoverOSCP(int timeout) {
        return discoverEPR(null, timeout, MIN_GAP_TIME);
    }
    
    /**
     * Find OSCP devices.
     * 
     * @param epr Set of endpoint references (null for all devices)
     * @param timeout Global upper search time limit
     * @param gaptime End search if no new device found in this time
     * 
     * @return List of found consumers
     */
    public List<SDCConsumer> discoverEPR(final Set<String> epr, int timeout, int gaptime) {
        SearchParameter search = new SearchParameter();
        search.setDeviceTypes(new QNameSet(new QName("MedicalDevice", "http://p11073-10207/draft10/msg/2017/10/05")), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
        final List<DeviceReference> devMap = Collections.synchronizedList(new ArrayList<DeviceReference>());
        SearchManager.searchDevice(search, new DefaultClient() {

            @Override
            public void deviceFound(DeviceReference devRef, SearchParameter search, String comManId) {
                devMap.add(devRef);
                synchronized(SDCServiceManager.this) {
                   SDCServiceManager.this.notifyAll();
                }                
            }
            
        }, null);
        int i = 0;
        int lastSize = 0;
        int gap = 0;
        while (i < timeout) {
            List<DeviceReference> toRemove = new ArrayList<>();
            synchronized(devMap) {
                if (epr != null)
                {
                    for (DeviceReference next : devMap) {
                        if (!epr.contains(next.getEndpointReference().getAddress().toString())) {
                            toRemove.add(next);
                        }
                    }
                }
            }                
            try {
                long startTime = System.currentTimeMillis();              
                synchronized(this) {
                   wait(100); 
                }
                long elapsed = System.currentTimeMillis() - startTime;                
                i += elapsed;
                gap += elapsed;
            } catch (InterruptedException ex) {
                Logger.getLogger(SDCServiceManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (lastSize < devMap.size()) {
                lastSize = devMap.size();    
                gap = 0;
            }
            devMap.removeAll(toRemove);
            if (epr != null && allFound(devMap, epr))
                break;
            if (gap > gaptime)
                break;            
        }
        List<SDCConsumer> consumers = new ArrayList<>();
        synchronized(devMap) {
            for (DeviceReference nextRef : devMap) {
                final SDCConsumer oscpConsumer = new SDCConsumer(nextRef);
                if (oscpConsumer.isConnected())
                    consumers.add(oscpConsumer);
            }
        }
        return consumers;
    }

    private boolean allFound(List<DeviceReference> devMap, Set<String> epr) {
        if (devMap.size() != epr.size())
            return false;
        Set<String> current = new HashSet<>();
        for (DeviceReference nextRef : devMap) {
            current.add(nextRef.getEndpointReference().getAddress().toString());
        }
        return current.equals(epr);
    }

    
}
