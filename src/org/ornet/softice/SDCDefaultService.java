/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.util.HashMap;
import org.yads.java.service.InvokeDelegate;

public abstract class SDCDefaultService extends SDCMixedService {

    public SDCDefaultService(String wsdl, HashMap<String, InvokeDelegate> invokeDelegates, int port) {
        super(wsdl, invokeDelegates, null, port);
    }      
}
