/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.util.HashMap;
import org.yads.java.service.EventDelegate;

public abstract class SDCEventService extends SDCMixedService {
    
    public SDCEventService(String wsdl, HashMap<String, EventDelegate> eventDelegates, int port) {
        super(wsdl, null, eventDelegates, port);
    }
 
}
