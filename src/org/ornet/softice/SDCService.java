/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.IPNetworkDetection;
import org.yads.java.communication.protocol.http.HTTPBinding;
import org.yads.java.communication.protocol.http.HTTPSBinding;

import org.yads.java.io.fs.FileResource;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.DefaultService;
import org.yads.java.types.ContentType;
import org.yads.java.types.URI;

public abstract class SDCService extends DefaultService {

    public static final String MESSAGEMODEL_NAMESPACE = "http://p11073-10207/draft10/msg/2017/10/05";

    public static final String EXTENSION_POINTXSD = "/org/ornet/softice/resources/ExtensionPoint.xsd";
    public static final String BICEPS__MESSAGE_MODELXSD = "/org/ornet/softice/resources/BICEPS_MessageModel.xsd";
    public static final String BICEPS__PARTICIPANT_MODELXSD = "/org/ornet/softice/resources/BICEPS_ParticipantModel.xsd";
    public static final String MDPWS_XSD = "/org/ornet/softice/resources/MDPWS.xsd";
    public static final String WSDL_RESOURCE_BASE_PATH = "/org/ornet/softice/resources/";
    
    protected final String wsdl;
    private final int port;
    
    public SDCService(String wsdl, String communicationManagerId, int port) {
        super(communicationManagerId);
        this.wsdl = wsdl;
        this.port = port;
    }
       
    public abstract String getOSCPServiceId();

    @Override
    public synchronized void start() throws IOException {
        setServiceId(new URI(getOSCPServiceId()));
        Iterator<IPAddress> adrIter = IPNetworkDetection.getInstance().getIPv4Addresses(true);
        final String bi = SoftICE.getInstance().getBindInterface();        
        while (adrIter.hasNext()) {
            IPAddress next = adrIter.next();
            if (bi == null || next.getAddressWithoutNicId().startsWith(bi) || bi.equals("0.0.0.0")) {
                HTTPBinding binding = (SoftICE.getInstance().getServerSSLContext() != null)?
                        new HTTPSBinding(next, port, getServiceId().toString(), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, CredentialInfo.EMPTY_CREDENTIAL_INFO)
                            :
                        new HTTPBinding(next, port, getServiceId().toString(), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
                this.addBinding(binding);                
            }
        }        
        super.start();
        try {
            String wsdlPath = WSDL_RESOURCE_BASE_PATH + wsdl;
            final FileResource fileResourceWSDL = new FileResource(wsdlPath, "description.wsdl");
            fileResourceWSDL.setContentType(new ContentType("application", "xml", "file"));
            // No generated WSDL: override an existing WSDL resource with the fixed name "description.wsdl"
            registerFileResource(fileResourceWSDL, MESSAGEMODEL_NAMESPACE);
            final FileResource fileResourceDomainModel = new FileResource(BICEPS__PARTICIPANT_MODELXSD);
            fileResourceDomainModel.setContentType(new ContentType("application", "xml", "file"));
            // No generated schema: add original XSD schema file resources
            registerFileResource(fileResourceDomainModel, MESSAGEMODEL_NAMESPACE);
            final FileResource fileResourceMessageModel = new FileResource(BICEPS__MESSAGE_MODELXSD);
            fileResourceMessageModel.setContentType(new ContentType("application", "xml", "file"));
            registerFileResource(fileResourceMessageModel, MESSAGEMODEL_NAMESPACE);
            final FileResource fileResourceExtensionPoint = new FileResource(EXTENSION_POINTXSD);
            fileResourceExtensionPoint.setContentType(new ContentType("application", "xml", "file"));
            registerFileResource(fileResourceExtensionPoint, MESSAGEMODEL_NAMESPACE);
            final FileResource fileResourceMDPWS = new FileResource(MDPWS_XSD);
            fileResourceMDPWS.setContentType(new ContentType("application", "xml", "file"));
            registerFileResource(fileResourceMDPWS, MESSAGEMODEL_NAMESPACE);
        } catch (Exception ex) {
            Logger.getLogger(SDCService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  

}
