/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.util.HashMap;
import java.util.Iterator;
import org.yads.java.communication.AutoBindingFactory;
import org.yads.java.communication.CommunicationManagerRegistry;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.IPDiscoveryDomain;
import org.yads.java.communication.connection.ip.IPNetworkDetection;
import org.yads.java.communication.protocol.http.HTTPBinding;
import org.yads.java.communication.protocol.http.HTTPSBinding;
import org.yads.java.communication.structures.DiscoveryAutoBinding;
import org.yads.java.communication.structures.IPDiscoveryAutoBinding;
import org.yads.java.communication.structures.IPDiscoveryBinding;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.DefaultDevice;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.types.QName;
import org.yads.java.types.QNameSet;

public final class SDCDevice extends DefaultDevice {

    public static final String EVENTREPORTWSDL = "eventreport.wsdl";
    public static final String WAVEREPORTWSDL = "wavereport.wsdl";
    public static final String CTXSERVICEWSDL = "ctxservice.wsdl";
    public static final String SETSERVICEWSDL = "setservice.wsdl";
    public static final String GETSERVICEWSDL = "getservice.wsdl";
    
    private boolean initialized = false;
    private String manufacturer;
    private String modelName;
    private String friendlyName;
    private SDCEventService streamService;
    private final int port;

    public SDCDevice() {
        super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
        friendlyName = "OR.NET device";
        modelName = "OR.NET model";
        manufacturer = "OR.NET consortium";
        port = SoftICE.getInstance().extractNextPort();
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }
    
    public void init(HashMap<String, HashMap<String, InvokeDelegate>> invokeDelegates, 
            HashMap<String, HashMap<String, EventDelegate>> eventDelegates) 
            throws IllegalArgumentException, UnsupportedOperationException, Exception {
        if (initialized) {
            throw new Exception("SDC device already initilized!");
        }
        initialized = true;
        QNameSet qns = new QNameSet();
        qns.add(new QName("MedicalDevice", "http://p11073-10207/draft10/msg/2017/10/05"));
        setPortTypes(qns);
        this.addFriendlyName("en-US", friendlyName);
        this.addModelName("en-US", modelName);
        this.addManufacturer("en-US", manufacturer);
        Iterator<IPAddress> adrIter = IPNetworkDetection.getInstance().getIPv4Addresses(true);
        final String bi = SoftICE.getInstance().getBindInterface();
        while (adrIter.hasNext()) {
            IPAddress next = adrIter.next();
            if (bi == null || next.getAddressWithoutNicId().startsWith(bi) || bi.equals("0.0.0.0")) {
                HTTPBinding binding = (SoftICE.getInstance().getServerSSLContext() != null)?
                        new HTTPSBinding(next, port, getEndpointReference().getAddress().toString(), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, CredentialInfo.EMPTY_CREDENTIAL_INFO)
                            :
                        new HTTPBinding(next, port, getEndpointReference().getAddress().toString(), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
                this.addBinding(binding);                            
            }
        }		        
        Iterator discDoms = IPNetworkDetection.getInstance().getAllAvailableDiscoveryDomains();
        while (discDoms.hasNext()) {
            final IPDiscoveryBinding ipDiscoveryBinding = new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, (IPDiscoveryDomain) discDoms.next()); 
            if (!ipDiscoveryBinding.getHostIPAddress().isIPv6())
                this.addBinding(ipDiscoveryBinding);
        }    
        AutoBindingFactory abf = CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID).getAutoBindingFactory();
        if (abf != null) {
            final String[] networkInterfaceNames = IPNetworkDetection.getInstance().getNetworkInterfaceNames();
            DiscoveryAutoBinding dab = abf.createDiscoveryMulticastAutoBinding(networkInterfaceNames, new String [] {IPDiscoveryAutoBinding.IPADDRESS_FAMILY_IPV4}, true);
            addOutgoingDiscoveryInfo(dab);
        }                            
        createServices(invokeDelegates, eventDelegates);
    }

    private void createServices(HashMap<String, HashMap<String, InvokeDelegate>> invokeDelegates, HashMap<String, HashMap<String, EventDelegate>> eventDelegates) {
        if (invokeDelegates.containsKey(GETSERVICEWSDL))
            addService(new SDCDefaultService(GETSERVICEWSDL, invokeDelegates.get(GETSERVICEWSDL), SoftICE.getInstance().extractNextPort()) {
                @Override
                public String getOSCPServiceId() {
                    return "GetService";
                }
            });
        if (invokeDelegates.containsKey(SETSERVICEWSDL))
            addService(new SDCDefaultService(SETSERVICEWSDL, invokeDelegates.get(SETSERVICEWSDL), SoftICE.getInstance().extractNextPort()) {
                @Override
                public String getOSCPServiceId() {
                    return "SetService";
                }
            });
        if (invokeDelegates.containsKey(CTXSERVICEWSDL))
            addService(new SDCMixedService(CTXSERVICEWSDL, invokeDelegates.get(CTXSERVICEWSDL), eventDelegates.get(CTXSERVICEWSDL), SoftICE.getInstance().extractNextPort()) {
                @Override
                public String getOSCPServiceId() {
                    return "ContextService";
                }
            });        
        if (eventDelegates.containsKey(EVENTREPORTWSDL))
            addService(new SDCEventService(EVENTREPORTWSDL, eventDelegates.get(EVENTREPORTWSDL), SoftICE.getInstance().extractNextPort()) {
                @Override
                public String getOSCPServiceId() {
                    return "EventReport";
                }
            });         
        streamService = new SDCEventService(WAVEREPORTWSDL, eventDelegates.get(WAVEREPORTWSDL), SoftICE.getInstance().extractNextPort()) {
            @Override
            public String getOSCPServiceId() {
                return "WaveformEventReport";
            }
        };
        if (eventDelegates.containsKey(WAVEREPORTWSDL))
            addService(streamService);         
    }

    public SDCEventService getStreamService() {
        return streamService;
    }
    
}
